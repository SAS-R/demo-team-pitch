/* $Id: signature.js 19479 2014-09-11 02:59:48Z JAMESR $ */
/* $URL: http://sifnzrandd1/Mobility/releases/Touch3.2.1/Touch/Touch/core/public/js/signature.js $ */

/**
 * StayinFront Signature Control
 * Version 1.0 - Last updated: 2011.04.20
 * Created by DC 
 */
var isTouch = ('ontouchstart' in window);
window.ORIENTATIONCHANGE_EVENT = ("onorientationchange" in window) ? 'orientationchange' : 'resize';
(function (globals)
{
	 //Constructor of Signature class
	function signature(el,iHeight,iWidth,bCanRotate,sSignHere,bShowLine,sBackColor)
{
		this.element = el;

		//Override events of touch and changing orientations
		this.element[0].addEventListener('touchstart', this, false);
		//if (isDesktopBrowser || isWindowsTouchShell)
			this.element[0].addEventListener('mousedown', this, false);
		window.addEventListener(globals.ORIENTATIONCHANGE_EVENT, this, false);
		this.lastPenPoint = null;
		this.pen = this.element[0].getContext("2d");
		this.canRotate = bCanRotate;
		this.portrait = null;

		this.backcolor = sBackColor ;
		this.height = iHeight;
		this.width = iWidth;
		this.signHere = sSignHere;
		this.showline = bShowLine;
		this.fontName = "'Droid Sans',Calibri,Helvetica";
		this.arrCommands = new Array();
		this.xLeft = 0;
		this.xTop = 0;
	}

	signature.prototype =
	{
		handleEvent: function(e)
		{
			try
			{
				switch (e.type)
				{
					case 'touchstart':
					case 'mousedown':
						this.onTouchStart(e);
						break;
					case 'touchmove':
					case 'mousemove':
						this.onTouchMove(e);
						break;
					case 'touchend':
					case 'mouseup':
						this.onTouchEnd(e);
						break;
					case globals.ORIENTATIONCHANGE_EVENT:
						this.resizeCanvas(e);
						break;
				}
			}
			catch (e)
			{
				alert('signature error: ' + e);
				throw e;
			}
		},
		
		onTouchStart: function(e)
		{
			
			e.preventDefault();
			
 			var self = this;
			var localPosition;

			var isTouchEvent = window.isTouch && 'touches' in e;
			if (isTouchEvent)
			{
				var touch = e.touches[0];
				localPosition = self.getLocalCoordinates(
					touch.pageX,
					touch.pageY
				);
				
			}
			else {
				localPosition = self.getLocalCoordinates(
					e.pageX,
					e.pageY
				);
			}

			if((localPosition.x >= this.xLeft && localPosition.x <= (this.xLeft + 12)) && (localPosition.y >= (this.xTop - 14) && localPosition.y <= this.xTop))
			{
				$(this).trigger('signaturexbuttontapped.crm');
			}
			
			// record last pen point
			self.lastPenPoint = {
				x: localPosition.x,
				y: localPosition.y
			};
			
			// move the pen to the starting coordinate and begin a new path.
			self.pen.beginPath();
			self.pen.moveTo( self.lastPenPoint.x, self.lastPenPoint.y );
			
			//if it is in portrait mode, transform the coordinate and record the command
			if (this.portrait) {
				var this_point = [self.lastPenPoint.y,self.height-self.lastPenPoint.x];
				self.addArrCommand(true,this_point);
			}
			else {
				var this_point = [self.lastPenPoint.x,self.lastPenPoint.y];
				self.addArrCommand(true,this_point);
			}

			// after touchstart has been triggered, listen to touchmove and touchend events
			if (isTouchEvent)
			{
				self.element[0].addEventListener('touchmove', self, false);
				self.element[0].addEventListener('touchend', self, false);
			}
			else
			{
				self.element[0].addEventListener('mousemove', self, false);
				self.element[0].addEventListener('mouseup', self, false);
			}

			return false;
		},
		
		// a function to record commands
		addArrCommand: function( newStroke, command ){
			if (newStroke) 
			{
				var newArray = new Array();
				newArray.push(command);
				this.arrCommands.push(newArray);
			}
			else
			{
				this.arrCommands[this.arrCommands.length-1].push(command);
			}
		},

		getLocalCoordinates: function( pageX, pageY )
		{
			// get the coordinate within canvas.
			var position = this.element.offset();
			return({
				x: parseInt(pageX - position.left),
				y: parseInt(pageY - position.top)
			});
		},

		onTouchEnd: function(e)
		{
			console.log('onTouchEnd');
			
			e.preventDefault();
			//When touchend, stop listening to touch events
			this.element[0].removeEventListener('touchmove', this, false);
			this.element[0].removeEventListener('mousemove', this, false);
			this.element[0].removeEventListener('touchend', this, false);
			this.element[0].removeEventListener('mouseup', this, false);
			
			return false;
		},

		onTouchMove: function(e)
		{
			console.log('onTouchMove');
			e.preventDefault();

			var self = this;
			
			if (window.isTouch && 'touches' in e) {
				var touch = e.touches[0];
				localPosition = self.getLocalCoordinates(
					touch.pageX,
					touch.pageY
				);
			}
			else {
				localPosition = self.getLocalCoordinates(
					e.pageX,
					e.pageY
				);
			}
			
			var isLineTo = this.insideCanvas(self.lastPenPoint);
			//record last pen point
			self.lastPenPoint = {
				x: localPosition.x,
				y: localPosition.y
			};

			if (this.insideCanvas(self.lastPenPoint) )
			{
				//Check if a new line should be started
				var newStroke;
				if (isLineTo)
				{
					//draw a line from last pen point to current coordinate
					self.pen.lineTo( self.lastPenPoint.x, self.lastPenPoint.y );
					self.pen.stroke();
					newStroke = false;
				}
				else
				{
					// move the pen to the starting coordinate and beign a new path.
					self.pen.beginPath();
					self.pen.moveTo( self.lastPenPoint.x, self.lastPenPoint.y );
					newStroke = true;
				}
				//if it is in portrait mode, transform the coordinate and record the command
				if (this.portrait) {
					var this_point = [self.lastPenPoint.y,self.height-self.lastPenPoint.x];
					self.addArrCommand(newStroke,this_point);
				}
				else {
					var this_point = [self.lastPenPoint.x,self.lastPenPoint.y];
					self.addArrCommand(newStroke,this_point);
				}
			}

			return false;
		},
		
		insideCanvas: function(lastPenPoint)
		{
			var bInside;
			if (this.portrait) {
				var transformedX = this.height - this.lastPenPoint.x;
				bInside = (lastPenPoint.y < 0 || lastPenPoint.y > this.width) || (transformedX < 0 || transformedX > this.height);
			}
			else {
				bInside = (lastPenPoint.x < 0 || lastPenPoint.x > this.width) || (lastPenPoint.y < 0 || lastPenPoint.y > this.height);
			}
			
			
			return !bInside;
		},

		setWidth: function(pageWidth)
		{
			if (this.portrait) {
				this.height = pageWidth;
			}
			else {
				this.width = pageWidth;
			}
			
			return false;
		},

		setHeight: function(pageHeight)
		{
			if (this.portrait) {
				this.width = pageHeight;
			}
			else {
				this.height = pageHeight;
			}
					
			return false;
		},

		resizeCanvas: function(e)
		{
			//this resize event of the canvas would redraw the signature, including the signature and the whole layout
			console.log('resizeCanvas');
			e.preventDefault();
			this.resizingCanvas();
		},

		resizingCanvas: function()
		{
			//if in portrait mode, the width and height would be vice-versa as landscape mode
			this.portrait = (window.innerHeight > window.innerWidth && this.canRotate);
			if (this.portrait) {
				this.element[0].setAttribute('width', this.height + 'px');
				this.element[0].setAttribute('height', this.width + 'px');
			}
			else {
				this.element[0].setAttribute('width', this.width + 'px');
				this.element[0].setAttribute('height', this.height + 'px');
			}

			this.redrawSignature();
		},

		redrawSignature: function()
		{
			this.drawSignatureLine();
			//save previous canvas layer
			this.pen.save();
			//if in portrait mode, rotate the canvas 90 dregee based on the origin of (height, 0)
			if (this.portrait) {
				this.pen.translate(this.height,0); 
				this.pen.rotate(90 * Math.PI / 180);
			}

			//redraw the signature based on the recorded commands
			if (has_value(this.arrCommands)) {
				var location;
				
				for (var i = 0; i < this.arrCommands.length; i++)
				{
					for (var j=0; j<this.arrCommands[i].length; j++)
					{
						if (j==0)
						{
							location = this.arrCommands[i][j];
								this.pen.beginPath();
								this.pen.moveTo( location[0], location[1] );
							}
						else
						{
							location = this.arrCommands[i][j];
							this.pen.lineTo( location[0], location[1] );
							this.pen.stroke();
						}
					}
				}
			}
			//restore previous layer
			this.pen.restore();
		},
		
		drawSignatureLine: function() 
		{
			//this function would draw the baseline and message of the signature control
			var lineX = 10;
			var lineY;
			
			//save previous canvas layer
			this.pen.save();
			//if in portrait mode, rotate the canvas 90 dregee based on the origin of (height, 0)
			//Y coordinate of the line will be different
			if (this.portrait) {
				lineY = this.element.width()-40;
				this.pen.translate(this.height,0);
				this.pen.rotate(90 * Math.PI / 180);
			}
			else {
				lineY = this.element.height()-40;
			}
			
			//fill backcolor
			this.setBackColor();
			
			if (this.showline) {
				this.pen.beginPath();
				this.pen.moveTo( lineX, lineY );
				this.pen.lineTo( lineX+this.width-30, lineY );
				this.pen.stroke();
				this.pen.font = "bold 22px " + this.fontName;
				this.pen.fillText("X", lineX, lineY - 14);
				this.xLeft = lineX;
				this.xTop = lineY - 14;
			}

			this.pen.font = "bold 14px " + this.fontName;
			this.pen.fillText(this.signHere, lineX, lineY + 14);
			this.pen.restore();
		},

		clearsignature: function()
		{
			if (this.portrait) {
				this.pen.clearRect(0, 0, this.element[0].height, this.element[0].width);
			}
			else {
				this.pen.clearRect(0, 0, this.element[0].width, this.element[0].height);
			}
			this.arrCommands = new Array();
			this.resizingCanvas();
		},

		getcommands: function()
		{
			return this.arrCommands;
		},
		
		destroy: function()
		{
			this.element[0].removeEventListener('touchstart', this, false);
			this.element[0].removeEventListener('mousedown', this, false);
			this.element[0].removeEventListener('touchmove', this, false);
			this.element[0].removeEventListener('mousemove', this, false);
			this.element[0].removeEventListener('touchend', this, false);
			this.element[0].removeEventListener('mouseup', this, false);
			this.element[0].removeEventListener(globals.ORIENTATIONCHANGE_EVENT, this, false);

			var $canvas = $(this.element[0]);
			$canvas.siblings('canvas.signature_placeholder').remove();
			$canvas.css("position", "inherit");
		},
		
		setBackColor: function()
		{
			if (has_value(this.backcolor))
			{
				if (this.backcolor != 'window')
				{
					var penColor = this.pen.fillStyle;
					this.pen.fillStyle = this.backcolor;
					this.pen.fillRect(0,0,this.width,this.height);
					this.pen.fillStyle = penColor;
				}
			}
		}
	}
	
	window.signature = signature;
})(this);