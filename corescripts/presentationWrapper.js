var current_presentation_id = null;
var crm_allow_save_data = false;
var crm_current_call_key = '';
var crm_current_presentation_key = '';
var pitch_data;
(function () {
    var testing = ( location.search == '');
    var include = function (file) {
        var tag = '<script src="' + file + '"></script>';
        console.log('writing script tag: ' + tag);
        document.write(tag);
    };

    function testingStartup() {

        var data = {

            Demo_DisplayDist: [{ "ASMID": 86, "Description": "PURE HAIRCARE CLASSIC SHAMPOO 15OZ DISPLAYABLE CASE", "ShipDate": "8/9-8/15", "Qty": 1 }, { "ASMID": 86, "Description": "PURE HAIRCARE CLASSIC CONDITIONER 15OZ DISPLAYABLE CASE", "ShipDate": "8/9-8/15", "Qty": 1 }, { "ASMID": 86, "Description": "PURE HAIRCARE SHINE SPRAY 7OZ PALLET 393 CT", "ShipDate": "9/6-9/12", "Qty": 1 }, { "ASMID": 86, "Description": "HAIRSPRAY 6.2OZ BULLET CLIP STRIP", "ShipDate": "9/6-9/12", "Qty": 1 }, { "ASMID": 86, "Description": "KERATIN SHAMPOO 6OZ DISPLAYABLE CASE", "ShipDate": "9/6-9/12", "Qty": 1 }, { "ASMID": 86, "Description": "KERATIN CONDITIONER 6OZ DISPLAY MODULE", "ShipDate": "9/6-9/12", "Qty": 1 }, { "ASMID": 86, "Description": "ORGANIC COCONUT SHAMPOO 12OZ 4 TIER RAC", "ShipDate": "10/8-10/14", "Qty": 1 }],

            Demo_PAR_HD_All1: [{ "Priority": 1, "MerchandisingOptions": "Zero Cases in the backroom", "PointValue": 5, "MaxPointValue": "5" }, { "Priority": 2, "MerchandisingOptions": "All Facings Stocked", "PointValue": 5, "MaxPointValue": "5" }, { "Priority": 3, "MerchandisingOptions": "In Aisle products set to POG & tagged", "PointValue": 3, "MaxPointValue": "3" }, { "Priority": 4, "MerchandisingOptions": "Hairbands or Headbands stocked in Speedy Checklanes", "PointValue": 3, "MaxPointValue": "3" }, { "Priority": 5, "MerchandisingOptions": "Sample product sizes stocked in Checklanes", "PointValue": 3, "MaxPointValue": "3" }, { "Priority": 6, "MerchandisingOptions": "Shared Sidecap on Action Alley tagged/stocked", "PointValue": 3, "MaxPointValue": "2pts ea/Max6" }, { "Priority": 7, "MerchandisingOptions": "Exclusive Sidekicks on Action Alley tagged/stocked", "PointValue": 2, "MaxPointValue": "2pts ea/Max6" }, { "Priority": 8, "MerchandisingOptions": "New Keratin products on shelf", "PointValue": 2, "MaxPointValue": "3" }, { "Priority": 8, "MerchandisingOptions": "Incremental display up & tagged/stocked in Beauty section", "PointValue": 3, "MaxPointValue": "Max 10" }, { "Priority": 10, "MerchandisingOptions": "Clipstrips up in target aisles", "PointValue": 2, "MaxPointValue": "Max 6" }],
          
            DEMO_LAG_WM1: [{ "key": "4 Tier Haircare Rack", "value": "", "retail": "$202.98", "profit": "$117.64" }, { "key": "Classic/Keratin Pallet", "value": "", "retail": "$1,557.88", "profit": "$863.60" }, { "key": "Pure Sidekick", "value": "", "retail": "$179.64", "profit": "$95.88" }],

            ENR_POSCC_Lowes1_Chart:[{"Col1":"Yr","Col2":"Sept","Col3":"Oct","Col4":"Nov","Col5":"Dec","Col6":"Jan","Col7":"Feb","Col8":"YTD","Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":"1"},{"Col1":"Yr","Col2":"Mar","Col3":"Apr","Col4":"May","Col5":"Jun","Col6":"Jul","Col7":"Aug","Col8":"YTD","Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":"1"},{"Col1":"Yr","Col2":"Sept","Col3":"Oct","Col4":"Nov","Col5":"Dec","Col6":"Jan","Col7":"Feb","Col8":"YTD","Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":"1"},{"Col1":"Yr","Col2":"Mar","Col3":"Apr","Col4":"May","Col5":"Jun","Col6":"Jul","Col7":"Aug","Col8":"YTD","Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":"1"},{"Col1":"Current Yr","Col2":"632","Col3":"616","Col4":"382","Col5":"766","Col6":"433","Col7":"0","Col8":"2829","Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":"2"},{"Col1":"Current Yr","Col2":"0","Col3":"0","Col4":"0","Col5":"0","Col6":"0","Col7":"0","Col8":"0","Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":"2"},{"Col1":"Current Yr","Col2":"4862","Col3":"3299","Col4":"3295","Col5":"3998","Col6":"2375","Col7":"0","Col8":"17829","Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":"2"},{"Col1":"Current Yr","Col2":"0","Col3":"0","Col4":"0","Col5":"0","Col6":"0","Col7":"0","Col8":"0","Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":"2"},{"Col1":"Prior Yr","Col2":"0","Col3":"4274","Col4":"6361","Col5":"7362","Col6":"3894","Col7":"566","Col8":"22457","Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":"3"},{"Col1":"Prior Yr","Col2":"3787","Col3":"3080","Col4":"3617","Col5":"4682","Col6":"3175","Col7":"3646","Col8":"21987","Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":"3"},{"Col1":"Prior Yr","Col2":"0","Col3":"1045","Col4":"860","Col5":"1328","Col6":"648","Col7":"86","Col8":"3967","Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":"3"},{"Col1":"Prior Yr","Col2":"573","Col3":"489","Col4":"620","Col5":"1004","Col6":"645","Col7":"664","Col8":"3995","Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":"3"},{"Col1":"%Chg","Col2":"0.1","Col3":"0.26","Col4":"-0.38","Col5":"-0.24","Col6":"-0.33","Col7":"-1","Col8":"-0.29","Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":"4"},{"Col1":"%Chg","Col2":"0","Col3":"-1","Col4":"-1","Col5":"-1","Col6":"-1","Col7":"-1","Col8":"-1","Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":"4"},{"Col1":"%Chg","Col2":"0.28","Col3":"0.07","Col4":"-0.09","Col5":"-0.15","Col6":"-0.25","Col7":"-1","Col8":"-0.19","Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":"4"},{"Col1":"%Chg","Col2":"0","Col3":"-1","Col4":"-1","Col5":"-1","Col6":"-1","Col7":"-1","Col8":"-1","Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":"4"}],

            ENR_POSCC_Lowes1_TimeFrame_ugh:[{"Value":"(WeekNum == 'Mar-Aug')","Display":"Mar-Aug"},{"Value":"(WeekNum == 'Sept-Feb')","Display":"Sept-Feb"}],

            ENR_POSCC_Lowes1_TimeFrame:[{"Value":"Mar-Aug","Display":"Mar-Aug"},{"Value":"Sept-Feb","Display":"Sept-Feb"}],
		//		data.PWS_Movement_Radio = [{"Value":"(WeekNum>40 && WeekNum<53)","Display":"Last 12 weeks"},{"Value":"(WeekNum>0 && WeekNum<13)","Display":"LY Perf 12 weeks out"}];

        //////////////////////////////

            ENR_POSCC_Lowes1_Category:[{"Value":"Lighting Products","Display":"Lighting Products"},{"Value":"Premium Batteries","Display":"Premium Batteries"}],

            ENR_POSCC_Lowes1:[{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":1,"WeekEnding":"07-Sep","Year1DollarValue":132.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"31"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":1,"WeekEnding":"09-Mar","Year1DollarValue":158.00,"Year1Label":"Prior Year","Year2DollarValue":99.00,"Year2Label":"Current Year","WeekNum":"5"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":1,"WeekEnding":"07-Sep","Year1DollarValue":1052.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"31"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":1,"WeekEnding":"09-Mar","Year1DollarValue":1367.00,"Year1Label":"Prior Year","Year2DollarValue":1083.00,"Year2Label":"Current Year","WeekNum":"5"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":2,"WeekEnding":"16-Mar","Year1DollarValue":61.00,"Year1Label":"Prior Year","Year2DollarValue":97.00,"Year2Label":"Current Year","WeekNum":"6"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":2,"WeekEnding":"14-Sep","Year1DollarValue":157.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"32"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":2,"WeekEnding":"16-Mar","Year1DollarValue":1041.00,"Year1Label":"Prior Year","Year2DollarValue":925.00,"Year2Label":"Current Year","WeekNum":"6"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":2,"WeekEnding":"14-Sep","Year1DollarValue":1099.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"32"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":3,"WeekEnding":"23-Mar","Year1DollarValue":199.00,"Year1Label":"Prior Year","Year2DollarValue":200.00,"Year2Label":"Current Year","WeekNum":"7"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":3,"WeekEnding":"21-Sep","Year1DollarValue":165.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"33"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":3,"WeekEnding":"23-Mar","Year1DollarValue":792.00,"Year1Label":"Prior Year","Year2DollarValue":1071.00,"Year2Label":"Current Year","WeekNum":"7"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":3,"WeekEnding":"21-Sep","Year1DollarValue":1300.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"33"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":4,"WeekEnding":"28-Sep","Year1DollarValue":159.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"34"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":4,"WeekEnding":"30-Mar","Year1DollarValue":62.00,"Year1Label":"Prior Year","Year2DollarValue":231.00,"Year2Label":"Current Year","WeekNum":"8"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":4,"WeekEnding":"28-Sep","Year1DollarValue":1078.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"34"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":4,"WeekEnding":"30-Mar","Year1DollarValue":1179.00,"Year1Label":"Prior Year","Year2DollarValue":889.00,"Year2Label":"Current Year","WeekNum":"8"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":5,"WeekEnding":"06-Apr","Year1DollarValue":154.00,"Year1Label":"Prior Year","Year2DollarValue":93.00,"Year2Label":"Current Year","WeekNum":"9"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":5,"WeekEnding":"05-Oct","Year1DollarValue":224.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"35"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":5,"WeekEnding":"06-Apr","Year1DollarValue":1051.00,"Year1Label":"Prior Year","Year2DollarValue":1430.00,"Year2Label":"Current Year","WeekNum":"9"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":5,"WeekEnding":"05-Oct","Year1DollarValue":1292.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"35"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":6,"WeekEnding":"12-Oct","Year1DollarValue":216.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"36"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":6,"WeekEnding":"13-Apr","Year1DollarValue":109.00,"Year1Label":"Prior Year","Year2DollarValue":226.00,"Year2Label":"Current Year","WeekNum":"10"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":6,"WeekEnding":"12-Oct","Year1DollarValue":1118.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"36"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":6,"WeekEnding":"13-Apr","Year1DollarValue":1190.00,"Year1Label":"Prior Year","Year2DollarValue":1047.00,"Year2Label":"Current Year","WeekNum":"10"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":7,"WeekEnding":"20-Apr","Year1DollarValue":85.00,"Year1Label":"Prior Year","Year2DollarValue":192.00,"Year2Label":"Current Year","WeekNum":"11"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":7,"WeekEnding":"19-Oct","Year1DollarValue":425.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"37"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":7,"WeekEnding":"20-Apr","Year1DollarValue":865.00,"Year1Label":"Prior Year","Year2DollarValue":972.00,"Year2Label":"Current Year","WeekNum":"11"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":7,"WeekEnding":"19-Oct","Year1DollarValue":1243.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"37"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":8,"WeekEnding":"26-Oct","Year1DollarValue":164.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"38"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":8,"WeekEnding":"27-Apr","Year1DollarValue":128.00,"Year1Label":"Prior Year","Year2DollarValue":148.00,"Year2Label":"Current Year","WeekNum":"12"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":8,"WeekEnding":"26-Oct","Year1DollarValue":1174.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"38"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":8,"WeekEnding":"27-Apr","Year1DollarValue":1190.00,"Year1Label":"Prior Year","Year2DollarValue":1136.00,"Year2Label":"Current Year","WeekNum":"12"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":9,"WeekEnding":"04-May","Year1DollarValue":81.00,"Year1Label":"Prior Year","Year2DollarValue":97.00,"Year2Label":"Current Year","WeekNum":"13"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":9,"WeekEnding":"02-Nov","Year1DollarValue":210.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"39"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":9,"WeekEnding":"04-May","Year1DollarValue":1293.00,"Year1Label":"Prior Year","Year2DollarValue":808.00,"Year2Label":"Current Year","WeekNum":"13"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":9,"WeekEnding":"02-Nov","Year1DollarValue":971.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"39"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":10,"WeekEnding":"11-May","Year1DollarValue":164.00,"Year1Label":"Prior Year","Year2DollarValue":158.00,"Year2Label":"Current Year","WeekNum":"14"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":10,"WeekEnding":"09-Nov","Year1DollarValue":212.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"40"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":10,"WeekEnding":"11-May","Year1DollarValue":1224.00,"Year1Label":"Prior Year","Year2DollarValue":925.00,"Year2Label":"Current Year","WeekNum":"14"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":10,"WeekEnding":"09-Nov","Year1DollarValue":969.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"40"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":11,"WeekEnding":"16-Nov","Year1DollarValue":263.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"41"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":11,"WeekEnding":"18-May","Year1DollarValue":194.00,"Year1Label":"Prior Year","Year2DollarValue":152.00,"Year2Label":"Current Year","WeekNum":"15"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":11,"WeekEnding":"16-Nov","Year1DollarValue":974.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"41"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":11,"WeekEnding":"18-May","Year1DollarValue":779.00,"Year1Label":"Prior Year","Year2DollarValue":1106.00,"Year2Label":"Current Year","WeekNum":"15"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":12,"WeekEnding":"25-May","Year1DollarValue":127.00,"Year1Label":"Prior Year","Year2DollarValue":175.00,"Year2Label":"Current Year","WeekNum":"16"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":12,"WeekEnding":"23-Nov","Year1DollarValue":187.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"42"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":12,"WeekEnding":"25-May","Year1DollarValue":1199.00,"Year1Label":"Prior Year","Year2DollarValue":992.00,"Year2Label":"Current Year","WeekNum":"16"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":12,"WeekEnding":"23-Nov","Year1DollarValue":1316.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"42"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":13,"WeekEnding":"30-Nov","Year1DollarValue":133.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"43"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":13,"WeekEnding":"01-Jun","Year1DollarValue":90.00,"Year1Label":"Prior Year","Year2DollarValue":125.00,"Year2Label":"Current Year","WeekNum":"17"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":13,"WeekEnding":"30-Nov","Year1DollarValue":1057.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"43"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":13,"WeekEnding":"01-Jun","Year1DollarValue":1191.00,"Year1Label":"Prior Year","Year2DollarValue":816.00,"Year2Label":"Current Year","WeekNum":"17"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":14,"WeekEnding":"08-Jun","Year1DollarValue":141.00,"Year1Label":"Prior Year","Year2DollarValue":134.00,"Year2Label":"Current Year","WeekNum":"18"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":14,"WeekEnding":"07-Dec","Year1DollarValue":219.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"44"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":14,"WeekEnding":"08-Jun","Year1DollarValue":1064.00,"Year1Label":"Prior Year","Year2DollarValue":909.00,"Year2Label":"Current Year","WeekNum":"18"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":14,"WeekEnding":"07-Dec","Year1DollarValue":1066.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"44"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":15,"WeekEnding":"14-Dec","Year1DollarValue":197.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"45"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":15,"WeekEnding":"15-Jun","Year1DollarValue":131.00,"Year1Label":"Prior Year","Year2DollarValue":119.00,"Year2Label":"Current Year","WeekNum":"19"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":15,"WeekEnding":"14-Dec","Year1DollarValue":1326.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"45"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":15,"WeekEnding":"15-Jun","Year1DollarValue":988.00,"Year1Label":"Prior Year","Year2DollarValue":977.00,"Year2Label":"Current Year","WeekNum":"19"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":16,"WeekEnding":"22-Jun","Year1DollarValue":135.00,"Year1Label":"Prior Year","Year2DollarValue":80.00,"Year2Label":"Current Year","WeekNum":"20"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":16,"WeekEnding":"21-Dec","Year1DollarValue":518.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"46"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":16,"WeekEnding":"22-Jun","Year1DollarValue":1356.00,"Year1Label":"Prior Year","Year2DollarValue":717.00,"Year2Label":"Current Year","WeekNum":"20"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":16,"WeekEnding":"21-Dec","Year1DollarValue":1638.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"46"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":17,"WeekEnding":"28-Dec","Year1DollarValue":279.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"47"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":17,"WeekEnding":"29-Jun","Year1DollarValue":188.00,"Year1Label":"Prior Year","Year2DollarValue":279.00,"Year2Label":"Current Year","WeekNum":"21"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":17,"WeekEnding":"28-Dec","Year1DollarValue":1571.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"47"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":17,"WeekEnding":"29-Jun","Year1DollarValue":1257.00,"Year1Label":"Prior Year","Year2DollarValue":1234.00,"Year2Label":"Current Year","WeekNum":"21"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":18,"WeekEnding":"06-Jul","Year1DollarValue":213.00,"Year1Label":"Prior Year","Year2DollarValue":276.00,"Year2Label":"Current Year","WeekNum":"22"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":18,"WeekEnding":"04-Jan","Year1DollarValue":255.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"48"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":18,"WeekEnding":"06-Jul","Year1DollarValue":1198.00,"Year1Label":"Prior Year","Year2DollarValue":936.00,"Year2Label":"Current Year","WeekNum":"22"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":18,"WeekEnding":"04-Jan","Year1DollarValue":1036.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"48"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":19,"WeekEnding":"11-Jan","Year1DollarValue":286.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"49"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":19,"WeekEnding":"13-Jul","Year1DollarValue":88.00,"Year1Label":"Prior Year","Year2DollarValue":251.00,"Year2Label":"Current Year","WeekNum":"23"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":19,"WeekEnding":"11-Jan","Year1DollarValue":1077.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"49"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":19,"WeekEnding":"13-Jul","Year1DollarValue":1327.00,"Year1Label":"Prior Year","Year2DollarValue":951.00,"Year2Label":"Current Year","WeekNum":"23"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":20,"WeekEnding":"18-Jan","Year1DollarValue":233.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"50"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":20,"WeekEnding":"20-Jul","Year1DollarValue":207.00,"Year1Label":"Prior Year","Year2DollarValue":191.00,"Year2Label":"Current Year","WeekNum":"24"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":20,"WeekEnding":"18-Jan","Year1DollarValue":927.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"50"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":20,"WeekEnding":"20-Jul","Year1DollarValue":1371.00,"Year1Label":"Prior Year","Year2DollarValue":971.00,"Year2Label":"Current Year","WeekNum":"24"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":21,"WeekEnding":"27-Jul","Year1DollarValue":212.00,"Year1Label":"Prior Year","Year2DollarValue":212.00,"Year2Label":"Current Year","WeekNum":"25"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":21,"WeekEnding":"25-Jan","Year1DollarValue":164.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"51"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":21,"WeekEnding":"27-Jul","Year1DollarValue":1077.00,"Year1Label":"Prior Year","Year2DollarValue":1242.00,"Year2Label":"Current Year","WeekNum":"25"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":21,"WeekEnding":"25-Jan","Year1DollarValue":1094.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"51"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":22,"WeekEnding":"01-Feb","Year1DollarValue":179.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"52"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":22,"WeekEnding":"03-Aug","Year1DollarValue":168.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"26"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":22,"WeekEnding":"01-Feb","Year1DollarValue":957.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"52"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":22,"WeekEnding":"03-Aug","Year1DollarValue":1673.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"26"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":23,"WeekEnding":"10-Aug","Year1DollarValue":104.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"27"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":23,"WeekEnding":"08-Feb","Year1DollarValue":67.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"1"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":23,"WeekEnding":"10-Aug","Year1DollarValue":1054.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"27"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":23,"WeekEnding":"08-Feb","Year1DollarValue":1008.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"1"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":24,"WeekEnding":"17-Aug","Year1DollarValue":312.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"28"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":24,"WeekEnding":"17-Aug","Year1DollarValue":1664.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"28"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":25,"WeekEnding":"24-Aug","Year1DollarValue":222.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"29"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":25,"WeekEnding":"24-Aug","Year1DollarValue":958.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"29"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":26,"WeekEnding":"31-Aug","Year1DollarValue":183.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"30"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":26,"WeekEnding":"31-Aug","Year1DollarValue":1048.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"30"}],

            ENR_POS_OverviewLowes1_INFO:[{"Team":"ENR","Source":"Last Updated: 11/25/2013","LastUpdated":"Last Updated: 11/25/2013"}],

            ENR_POS_OverviewLowes1_StoreNum:[{"StoreNumber":"Store 1051"}],

            ENR_POS_OverviewLowes1:[{"Category":"Total Energizer","SalesLast4Wks":14219,"StorePct":14.9,"MktPct":4,"Sequence":1},{"Category":"Batteries","SalesLast4Wks":5289.3,"StorePct":-8.4,"MktPct":10.7,"Sequence":2},{"Category":"Shave","SalesLast4Wks":7426.8,"StorePct":-16,"MktPct":-13.1,"Sequence":3},{"Category":"Suncare","SalesLast4Wks":1503,"StorePct":10,"MktPct":10,"Sequence":4}],

            /*Gap Tracker Data*/
            CHW_GapT_All1: [{"Client":"Big Heart Pet Brands","Gap":"Place Shelf Space Holder and take picture","DueDate":"4/16/2015","DaysPast":"17","WorkDaysLeft":"0","CallMode":"GM"},{"Client":"Reynolds Consumer Products","Gap":"Hefty Kitchen Indoor trash bag IRC's (Goal is 140/store) Total Placed:  35","DueDate":"5/1/2015","DaysPast":"6","WorkDaysLeft":"14","CallMode":"GM"},{"Client":"Big Heart Pet Brands","Gap":"Place Shelf Space Holder and take picture","DueDate":"4/16/2015","DaysPast":"17","WorkDaysLeft":"0","CallMode":"GM"},{"Client":"Marzetti","Gap":"Flatout Display Placement","DueDate":"6/2/2015","DaysPast":"0","WorkDaysLeft":"16","CallMode":"Every"},{"Client":"Paramount Farms","Gap":"8oz or 10oz Roasted No Salt Pistachios","DueDate":"4/2/2015","DaysPast":"27","WorkDaysLeft":"0","CallMode":"Grocery"},{"Client":"47 Hour","Gap":"Checkout Display Feature Set Up","DueDate":"5/1/2015","DaysPast":"6","WorkDaysLeft":"0","CallMode":"Every"}],

            CHW_GapT_All1_Client: [{"Value":"47 Hour","Display":"47 Hour"},{"Value":"Big Heart Pet Brands","Display":"Big Heart Pet Brands"},{"Value":"Keurig","Display":"Keurig"},{"Value":"Marzetti","Display":"Marzetti"},{"Value":"Paramount Farms","Display":"Paramount Farms"},{"Value":"Reynolds Consumer Products","Display":"Reynolds Consumer Products"}],

            CHW_GapT_All1_WorkDaysLeft: [{"Value": "0", "Display": "0"},
                {"Value": "14", "Display": "14"},
                {"Value": "16", "Display": "16"}],

            CHW_GapT_All1_CallMode: [{"Value": "Every", "Display": "Every"},
                {"Value": "GM", "Display": "GM"},
                {"Value": "Grocery", "Display": "Grocery"}],

            /*END Gap Tracker Data*/


                    //////////////////////////////



            /*Lost Sales Data*/
            SCJ_OSA_SelectedFilter: [{"Value":"","Display":"All"},{"Value":"Selected","Display":"Selected Only"}],

            SCJ_OSA_LostSalesCalculator1_INFO: [{"Team":"SCJ","Source":"Source: Retail Link, 2/20/2014","LastUpdated":"2/20/2014"}],

            SCJ_OSA_LostSalesCalculator1: [{"WINDescription":"550460643 - GLD AEROSOL REDHONEY","Category":"Air Care","OHLW":157,"MissedSls3Wks":94.08,"FutureMissedSls":282.24},{"WINDescription":"1336229 - GLD SOLID HAWAIIAN","Category":"Air Care","OHLW":27,"MissedSls3Wks":82.32,"FutureMissedSls":246.96},{"WINDescription":"550415176 - GLD AEROSOL APPLCINN","Category":"Air Care","OHLW":183,"MissedSls3Wks":76.44,"FutureMissedSls":229.32},{"WINDescription":"1311707 - GLD OIL RF2PK SWTPEA","Category":"Air Care","OHLW":7,"MissedSls3Wks":58.56,"FutureMissedSls":175.68},{"WINDescription":"1319611 - GLD LI OIL RF2PK LIN","Category":"Air Care","OHLW":93,"MissedSls3Wks":58.56,"FutureMissedSls":175.68},{"WINDescription":"551605991 - ZPLC TNTED SLDR GAL","Category":"Home Storage","OHLW":9,"MissedSls3Wks":52.56,"FutureMissedSls":157.68},{"WINDescription":"1317913 - PLEDGE MS RAINSHOWER","Category":"Home Cleaning","OHLW":6,"MissedSls3Wks":48.6,"FutureMissedSls":145.8},{"WINDescription":"551945013 - GLD CDL HB/VPF 3.8OZ","Category":"Air Care","OHLW":6,"MissedSls3Wks":44.7,"FutureMissedSls":134.1},{"WINDescription":"550081517 - GLD AUTOSPR RF LAVND","Category":"Air Care","OHLW":8,"MissedSls3Wks":43.92,"FutureMissedSls":131.76},{"WINDescription":"550487668 - GLD S&S RFL CASHMERE","Category":"Air Care","OHLW":10,"MissedSls3Wks":26.82,"FutureMissedSls":80.46}],

            SCJ_OSA_LostSalesCalculator1old: [{"WINDescription":"551945013 - GLD CDL HB/VPF 3.8OZ","Category":"Air Care","OHLW":23,"MissedSls3Wks":23,"FutureMissedSls":69},{"WINDescription":"1110625 - DRANO MAX 42OZ GEL","Category":"Home Cleaning","OHLW":13,"MissedSls3Wks":13,"FutureMissedSls":39},{"WINDescription":"551945539 - GLD CND MW/WS 3.8OZ","Category":"Air Care","OHLW":9,"MissedSls3Wks":9,"FutureMissedSls":27},{"WINDescription":"551945013 - GLD CDL HB/VPF 3.8OZ","Category":"Air Care","OHLW":23,"MissedSls3Wks":23,"FutureMissedSls":69},{"WINDescription":"1110625 - DRANO MAX 42OZ GEL","Category":"Home Cleaning","OHLW":13,"MissedSls3Wks":13,"FutureMissedSls":39},{"WINDescription":"551945539 - GLD CND MW/WS 3.8OZ","Category":"Air Care","OHLW":9,"MissedSls3Wks":9,"FutureMissedSls":27},{"WINDescription":"551945013 - GLD CDL HB/VPF 3.8OZ","Category":"Air Care","OHLW":23,"MissedSls3Wks":23,"FutureMissedSls":69},{"WINDescription":"1110625 - DRANO MAX 42OZ GEL","Category":"Home Cleaning","OHLW":13,"MissedSls3Wks":13,"FutureMissedSls":39},{"WINDescription":"551945539 - GLD CND MW/WS 3.8OZ","Category":"Air Care","OHLW":9,"MissedSls3Wks":9,"FutureMissedSls":27},{"WINDescription":"551945013 - GLD CDL HB/VPF 3.8OZ","Category":"Air Care","OHLW":23,"MissedSls3Wks":23,"FutureMissedSls":69},{"WINDescription":"1110625 - DRANO MAX 42OZ GEL","Category":"Home Cleaning","OHLW":13,"MissedSls3Wks":13,"FutureMissedSls":39},{"WINDescription":"551945539 - GLD CND MW/WS 3.8OZ","Category":"Air Care","OHLW":9,"MissedSls3Wks":9,"FutureMissedSls":27},{"WINDescription":"551598487 - OVAL NEON PINK 45","Category":"Shoe Care","OHLW":6,"MissedSls3Wks":6,"FutureMissedSls":18}],

            ARTS_Client_Activities:
                [{
                    "id": "285",
                    "dept": "Seasonal",
                    "ord": "4",
                    "distribution_folder": {
                        "113213": {
                            "codes": "|336600100099|336600100099|0033660010009| 336600100099||033660010009|336600100099|~|453463|",
                            "PID": "113213",
                            "CM": null,
                            "IS": " ",
                            "wf_r": "",
                            "OS": {
                                "is_positive": false,
                                "id": "1",
                                "pt": "Out-Of-Stock (OOS)"
                            },
                            "PF": "9113",
                            "CL": "1865",
                            "CPT": "Display",
                            "oth_info": " |OOS|6/2/2014~OOS~CF~5/27/2014~OOS~CF~5/23/2014~OOS~CF~~~|01DSP|00|Pure|10009||| |Yes|PIC|2014/07/18 09:27:45~OOS~CF|| |All Sets",
                            "PB": "5850",
                            "url": "https://www.asmconnects.com",
                            "WF": true,
                            "WD": true,
                            "Com": false,
                            "NI": false,
                            "AI": true,
                            "ZS": false,
                            "SC": false,
                            "bad": true,
                            "AT": true,
                            "WF2": false,
                            "invres": null,
                            "aux": {
                                "Fields": {}
                            },
                            "wf_sr": "Auto",
                            "id": "527388198",
                            "pt": "Pure Haircare Floor Display",
                            "AC": {
                                "id": "5",
                                "pt": "Ordered (OR)"
                            }
                        }
                    },
                    "tasks": {
                        "folder": []
                    },
                    "surveys": {
                        "folder": {}
                    },
                    "promotions": {
                        "folder": {}
                    },
                    "pt": "Display"
                }, {
                    "id": "124",
                    "dept": "HBC",
                    "ord": "9996",
                    "distribution_folder": {
                        "113205": {
                            "codes": "|336600100011|336600100011|0033660010001| 336600100011||033660010001|336600100011|~|453454|",
                            "PID": "113205",
                            "CM": null,
                            "IS": "U",
                            "wf_r": "",
                            "OS": {
                                "is_positive": false,
                                "id": "4",
                                "pt": "Distribution Void (DV)"
                            },
                            "PF": "9113",
                            "CL": "1865",
                            "CPT": "Hair - Shampoo/Cond",
                            "oth_info": "U|DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~~~|13.5Z|00|Pure|10001|||U|No|PIC|2014/07/18 09:27:45~DV~CF||U|All Sets",
                            "PB": "5850",
                            "url": "",
                            "WF": true,
                            "WD": true,
                            "Com": false,
                            "NI": true,
                            "AI": true,
                            "ZS": false,
                            "SC": false,
                            "bad": true,
                            "AT": true,
                            "WF2": false,
                            "invres": null,
                            "aux": {
                                "Fields": {}
                            },
                            "wf_sr": "New Unconfirmed",
                            "id": "527388210",
                            "prompt": "Pure Haircare Volume Shampoo 13.5z",
                            "AC": {
                                "id": "5",
                                "pt": "Ordered (OR)"
                            }
                        },
                        "113206": {
                            "codes": "|336600100022|336600100022|0033660010002| 336600100022||033660010002|336600100022|~|453456|",
                            "PID": "113206",
                            "CM": null,
                            "IS": " ",
                            "wf_r": "",
                            "OS": {
                                "is_positive": false,
                                "id": "1",
                                "pt": "Out-Of-Stock (OOS)"
                            },
                            "PF": "9113",
                            "CL": "1865",
                            "CPT": "Hair - Shampoo/Cond",
                            "oth_info": " |DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~~~|13.5Z|00|Pure|10002||| |Yes|PIC|2014/07/18 09:27:45~DV~CF|| |",
                            "PB": "5850",
                            "url": "",
                            "WF": true,
                            "WD": true,
                            "Com": true,
                            "NI": false,
                            "AI": true,
                            "ZS": false,
                            "SC": false,
                            "bad": true,
                            "AT": true,
                            "WF2": false,
                            "invres": null,
                            "aux": {
                                "Fields": {}
                            },
                            "wf_sr": "Auto",
                            "id": "527388207",
                            "prompt": "Pure Haircare Ultra Shine Shampoo 13.5z",
                            "is_modified": true,
                            "last_updated": "2017/08/04 13:36:56",
                            "AC": {
                                "id": "5",
                                "pt": "Ordered (OR)"
                            },
                            "RS": {
                                "is_positive": false,
                                "id": "1",
                                "pt": "Out-Of-Stock (OOS)"
                            }
                        },
                        "113207": {
                            "codes": "|336600100033|336600100033|0033660010003| 336600100033||033660010003|336600100033|~|453459|",
                            "PID": "113207",
                            "CM": "278",
                            "IS": " ",
                            "wf_r": "P",
                            "OS": {
                                "is_positive": false,
                                "id": "4",
                                "pt": "Out-Of-Stock (OOS)"
                            },
                            "PF": "9113",
                            "CL": "1865",
                            "CPT": "Hair - Shampoo/Cond",
                            "oth_info": " |DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~P~P~P|13.5Z|00|Pure|10003||| |Yes|15|2014/07/18 09:27:45~DV~CF|| |12ft",
                            "PB": "5850",
                            "url": "",
                            "WF": true,
                            "WD": true,
                            "Com": true,
                            "NI": false,
                            "AI": true,
                            "ZS": false,
                            "SC": false,
                            "bad": true,
                            "AT": true,
                            "WF2": false,
                            "invres": null,
                            "aux": {
                                "Fields": {}
                            },
                            "wf_sr": "P",
                            "id": "527388199",
                            "prompt": "Pure Haircare Silky Smooth Shampoo 13.5z",
                            "is_modified": true,
                            "last_updated": "2017/08/04 13:36:46",
                            "AC": {
                                "id": "4",
                                "pt": "Ordered (OR)"
                            },
                            "RS": {
                                "is_positive": false,
                                "id": "4",
                                "pt": "Distribution Void (DV)"
                            }
                        },
                        "113208": {
                            "codes": "|336600100044|336600100044|0033660010004| 336600100044||033660010004|336600100044|~|453455|",
                            "PID": "113208",
                            "CM": "278",
                            "IS": " ",
                            "wf_r": "Z",
                            "OS": {
                                "is_positive": true,
                                "id": 12,
                                "pt": "Distribution Void (DV)"
                            },
                            "PF": "9113",
                            "CL": "1865",
                            "CPT": "Hair - Shampoo/Cond",
                            "oth_info": " ||~~~~~~~~~~~|12Z|00|Pure|10004||| |Yes|PIC|~~|| |All Sets",
                            "PB": "5850",
                            "url": "",
                            "WF": true,
                            "WD": true,
                            "Com": false,
                            "NI": false,
                            "AI": true,
                            "ZS": true,
                            "SC": false,
                            "bad": true,
                            "AT": true,
                            "WF2": false,
                            "invres": null,
                            "aux": {
                                "Fields": {}
                            },
                            "wf_sr": "Z",
                            "id": "527388204",
                            "prompt": "Pure Haircare Volume Conditioner 12z",
                            "AC": {
                                "id": "2",
                                "pt": "Ordered (OR)"
                            }
                        },
                        "113210": {
                            "codes": "|336600100066|336600100066|0033660010006| 336600100066||033660010006|336600100066|~|453458|",
                            "PID": "113210",
                            "CM": null,
                            "IS": " ",
                            "wf_r": "",
                            "OS": {
                                "is_positive": false,
                                "id": "4",
                                "pt": "Distribution Void (DV)"
                            },
                            "PF": "9113",
                            "CL": "1865",
                            "CPT": "Hair - Shampoo/Cond",
                            "oth_info": " |DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~~~|12Z|00|Pure|10006||| |Yes|PIC|2014/07/18 09:27:45~DV~CF|| |",
                            "PB": "5850",
                            "url": "",
                            "WF": true,
                            "WD": true,
                            "Com": true,
                            "NI": false,
                            "AI": true,
                            "ZS": false,
                            "SC": false,
                            "bad": true,
                            "AT": true,
                            "WF2": false,
                            "invres": null,
                            "aux": {
                                "Fields": {}
                            },
                            "wf_sr": "Auto",
                            "id": "527388202",
                            "prompt": "Pure Haircare Soft Curls Conditioner 12z",
                            "is_modified": true,
                            "last_updated": "2017/08/04 13:36:51",
                            "AC": {
                                "id": "2",
                                "pt": "Tagged (TG)"
                            },
                            "RS": {
                                "is_positive": false,
                                "id": "1",
                                "pt": "Out-Of-Stock (OOS)"
                            }
                        }
                    },
                    "tasks": {
                        "folder": [{
                            "ttl": "Trial Size Checkout Lanes",
                            "st": "2014/06/26 00:00:00",
                            "en": "2025/12/31 00:00:00",
                            "client": {
                                "id": "1865",
                                "pt": "Pure"
                            },
                            "cat": {
                                "id": "124",
                                "pt": "Hair - Shampoo/Cond"
                            },
                            "prod": null,
                            "typ": {
                                "sig_req": false,
                                "id": "623",
                                "pt": "BDM Generated"
                            },
                            "sts": null,
                            "desc": "Verify trial size shampoo and conditioner are on display in the checkout lanes.  If needed, restock product.",
                            "subby": "Workflow User-ADMIN",
                            "rec": true,
                            "can_delete": false,
                            "can_modify": false,
                            "cm": "278",
                            "ct": "",
                            "atch": "",
                            "URL": "",
                            "reqPH": false,
                            "hqp": false,
                            "id": "3de188fb-fedf-482b-a026-73ebf1cd1bd6"
                        }]
                    },
                    "surveys": {
                        "folder": {
                            "35000": {
                                "pt": "HairCare Survey",
                                "id": "35000",
                                "folder": [{
                                    "values": [{
                                        "id": "6285",
                                        "pt": "2 tier wire rack"
                                    }, {
                                        "id": "6283",
                                        "pt": "4 tier floor stand"
                                    }, {
                                        "id": "6286",
                                        "pt": "Head band clipstrip"
                                    }, {
                                        "id": "6284",
                                        "pt": "HBA Endcap for BOGO"
                                    }],
                                    "id": "96373",
                                    "pt": "Which displays are currently up in this store (mark all that apply)?",
                                    "answer": null,
                                    "group": "35000",
                                    "type": "CHK",
                                    "qtype": "Checklist",
                                    "last_ans": "",
                                    "act": "1",
                                    "sbc": null,
                                    "sbs": null,
                                    "condition": null,
                                    "gq": null,
                                    "iscon": false,
                                    "ASMID": "ASM_96373",
                                    "att1": "",
                                    "att2": "",
                                    "att3": "",
                                    "att4": "",
                                    "att5": "",
                                    "att6": "",
                                    "att7": "",
                                    "att8": "",
                                    "att9": "",
                                    "att10": "",
                                    "cm": "278",
                                    "reqPH": false,
                                    "hqp": false
                                }, {
                                    "values": [{
                                        "id": "109801",
                                        "pt": "Yes"
                                    }, {
                                        "id": "109802",
                                        "pt": "No"
                                    }, {
                                        "id": "109803",
                                        "pt": "There are NO U-scan checkouts in this store"
                                    }],
                                    "id": "96327",
                                    "pt": "Is the U-Scan checkout area in this store merchandised with mulit pack head bands?",
                                    "answer": null,
                                    "group": "35000",
                                    "type": "PICK",
                                    "qtype": "Pick List",
                                    "last_ans": "",
                                    "act": "1",
                                    "sbc": null,
                                    "sbs": null,
                                    "condition": null,
                                    "gq": null,
                                    "iscon": false,
                                    "ASMID": "ASM_96327",
                                    "att1": "",
                                    "att2": "",
                                    "att3": "",
                                    "att4": "",
                                    "att5": "",
                                    "att6": "",
                                    "att7": "",
                                    "att8": "",
                                    "att9": "",
                                    "att10": "",
                                    "cm": "278",
                                    "reqPH": false,
                                    "hqp": false
                                }, {
                                    "min": "0",
                                    "max": "999999",
                                    "decimal": false,
                                    "after_dec": "0",
                                    "id": "121194",
                                    "pt": "How many Pure Haircare displays are currently up in the store?",
                                    "answer": null,
                                    "group": "35000",
                                    "type": "NUMBER",
                                    "qtype": "Numeric",
                                    "last_ans": "",
                                    "act": "1",
                                    "sbc": null,
                                    "sbs": null,
                                    "condition": null,
                                    "gq": null,
                                    "iscon": false,
                                    "ASMID": "ASM_121194",
                                    "att1": "",
                                    "att2": "",
                                    "att3": "",
                                    "att4": "",
                                    "att5": "",
                                    "att6": "",
                                    "att7": "",
                                    "att8": "",
                                    "att9": "",
                                    "att10": "",
                                    "cm": "278",
                                    "reqPH": false,
                                    "hqp": false
                                }, {
                                    "min": "",
                                    "max": "",
                                    "id": "96374",
                                    "pt": "What is the expiration date on the ultra shine shampoo (first date to expire)?",
                                    "answer": null,
                                    "group": "35000",
                                    "type": "DATE",
                                    "qtype": "Date",
                                    "last_ans": "",
                                    "act": "1",
                                    "sbc": null,
                                    "sbs": null,
                                    "condition": null,
                                    "gq": null,
                                    "iscon": false,
                                    "ASMID": "ASM_96374",
                                    "att1": "",
                                    "att2": "",
                                    "att3": "",
                                    "att4": "",
                                    "att5": "",
                                    "att6": "",
                                    "att7": "",
                                    "att8": "",
                                    "att9": "",
                                    "att10": "",
                                    "cm": "278",
                                    "reqPH": false,
                                    "hqp": false
                                }, {
                                    "min": "0",
                                    "max": "50",
                                    "decimal": true,
                                    "after_dec": "2",
                                    "id": "96375",
                                    "pt": "What is the price of 20oz Rose Petals conditioner?",
                                    "answer": null,
                                    "group": "35000",
                                    "type": "NUMBER",
                                    "qtype": "Number with Decimals",
                                    "last_ans": "",
                                    "act": "1",
                                    "sbc": null,
                                    "sbs": null,
                                    "condition": null,
                                    "gq": null,
                                    "iscon": false,
                                    "ASMID": "ASM_96375",
                                    "att1": "",
                                    "att2": "",
                                    "att3": "",
                                    "att4": "",
                                    "att5": "",
                                    "att6": "",
                                    "att7": "",
                                    "att8": "",
                                    "att9": "",
                                    "att10": "",
                                    "cm": "278",
                                    "reqPH": false,
                                    "hqp": false
                                }, {
                                    "min": "0",
                                    "max": "60",
                                    "decimal": false,
                                    "after_dec": "0",
                                    "id": "96331",
                                    "pt": "How many active front-end checkouts are in this store?",
                                    "answer": null,
                                    "group": "35000",
                                    "type": "NUMBER",
                                    "qtype": "Numeric",
                                    "last_ans": "",
                                    "act": "1",
                                    "sbc": null,
                                    "sbs": null,
                                    "condition": null,
                                    "gq": null,
                                    "iscon": false,
                                    "ASMID": "ASM_96331",
                                    "att1": "",
                                    "att2": "",
                                    "att3": "",
                                    "att4": "",
                                    "att5": "",
                                    "att6": "",
                                    "att7": "",
                                    "att8": "",
                                    "att9": "",
                                    "att10": "",
                                    "cm": "278",
                                    "reqPH": false,
                                    "hqp": false
                                }, {
                                    "id": "96376",
                                    "pt": "Who is the main contact for the Health and Beauty Department (first & last name)?",
                                    "answer": null,
                                    "group": "35000",
                                    "type": "TEXT",
                                    "qtype": "Text",
                                    "last_ans": "",
                                    "act": "1",
                                    "sbc": null,
                                    "sbs": null,
                                    "condition": null,
                                    "gq": null,
                                    "iscon": false,
                                    "ASMID": "ASM_96376",
                                    "att1": "",
                                    "att2": "",
                                    "att3": "",
                                    "att4": "",
                                    "att5": "",
                                    "att6": "",
                                    "att7": "",
                                    "att8": "",
                                    "att9": "",
                                    "att10": "",
                                    "cm": "278",
                                    "reqPH": false,
                                    "hqp": false
                                }, {
                                    "values": [{
                                        "id": "0",
                                        "pt": "Yes"
                                    }, {
                                        "id": "1",
                                        "pt": "No"
                                    }, {
                                        "id": "2",
                                        "pt": "NA"
                                    }],
                                    "id": "96377",
                                    "pt": "Is the 4 Tier Pure Haircare display up in this store?",
                                    "answer": null,
                                    "group": "35000",
                                    "type": "PICK",
                                    "qtype": "YNNA",
                                    "last_ans": "",
                                    "act": "1",
                                    "sbc": "0",
                                    "sbs": "",
                                    "condition": null,
                                    "gq": "122383",
                                    "iscon": false,
                                    "ASMID": "ASM_96377",
                                    "att1": "",
                                    "att2": "",
                                    "att3": "",
                                    "att4": "",
                                    "att5": "",
                                    "att6": "",
                                    "att7": "",
                                    "att8": "",
                                    "att9": "",
                                    "att10": "",
                                    "cm": "278",
                                    "reqPH": false,
                                    "hqp": false
                                }],
                                "cm": "278",
                                "ct": null,
                                "iscon": false,
                                "atch": "",
                                "URL": "https://www.asmconnects.com/sites/artsuni/doclock/Demo%20Team/National/In%20Line%20Fixture.pdf"
                            }
                        }
                    },
                    "promotions": {
                        "folder": {
                            "20220": {
                                "id": "20220",
                                "ttl": "HairCare 4 Tier Rack Display Promotion",
                                "cm": "278",
                                "l_note": null,
                                "note": null,
                                "p_st": null,
                                "stdt": "3/27/2017",
                                "endt": "12/31/2025",
                                "st_now": "",
                                "last": "No History Available",
                                "summ": "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n<html>\r\n  <head>\r\n    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\r\n    <title>bullet</title>\r\n  </head>\r\n  <body>\r\n    <div align=\"center\"><a href=\"twopromopic.JPG\"><img alt=\"\"\r\n          src=\"twopromopic.JPG\"></a></div>\r\n  </body>\r\n</html>\r\n",
                                "item_folder": [{
                                    "id": "177568",
                                    "pt": "Pure Haircare Volume Shampoo 13.5z",
                                    "pid": "113205",
                                    "sz": "13.5Z",
                                    "pk": "00",
                                    "ic": "10001",
                                    "cupc": "336600100011",
                                    "upc": "336600100011",
                                    "wupc": "0033660010001",
                                    "client": "Pure",
                                    "scode": "453454",
                                    "on": false
                                }, {
                                    "id": "177569",
                                    "pt": "Pure Haircare Volume Conditioner 12z",
                                    "pid": "113208",
                                    "sz": "12Z",
                                    "pk": "00",
                                    "ic": "10004",
                                    "cupc": "336600100044",
                                    "upc": "336600100044",
                                    "wupc": "0033660010004",
                                    "client": "Pure",
                                    "scode": "453455",
                                    "on": false
                                }, {
                                    "id": "177596",
                                    "pt": "Pure Haircare Ultra Shine Shampoo 13.5z",
                                    "pid": "113206",
                                    "sz": "13.5Z",
                                    "pk": "00",
                                    "ic": "10002",
                                    "cupc": "336600100022",
                                    "upc": "336600100022",
                                    "wupc": "0033660010002",
                                    "client": "Pure",
                                    "scode": "453456",
                                    "on": false
                                }, {
                                    "id": "177597",
                                    "pt": "Pure Haircare Silky Smooth Shampoo 13.5z",
                                    "pid": "113207",
                                    "sz": "13.5Z",
                                    "pk": "00",
                                    "ic": "10003",
                                    "cupc": "336600100033",
                                    "upc": "336600100033",
                                    "wupc": "0033660010003",
                                    "client": "Pure",
                                    "scode": "453459",
                                    "on": false
                                }, {
                                    "id": "177598",
                                    "pt": "Pure Haircare Ultra Shine Conditioner 12z",
                                    "pid": "113209",
                                    "sz": "12Z",
                                    "pk": "00",
                                    "ic": "10005",
                                    "cupc": "336600100055",
                                    "upc": "336600100055",
                                    "wupc": "0033660010005",
                                    "client": "Pure",
                                    "scode": "453457",
                                    "on": false
                                }],
                                "loc_folder": [],
                                "img": [{
                                    "imgsrc": "src=\"twopromopic.JPG\"",
                                    "id": "6411941406153872011",
                                    "full_img": false
                                }],
                                "def_loc": {
                                    "id": "2ef4d7e2-78a1-4f64-b5c5-47e2485553a8",
                                    "pt": "Primary Display Location (PDL)"
                                },
                                "def_uom": {
                                    "id": "490",
                                    "pt": "Display(s)"
                                },
                                "pc": {
                                    "id": "124",
                                    "pt": "Hair - Shampoo/Cond"
                                },
                                "ASMID": "ASM_20220",
                                "atch": "",
                                "URL": "",
                                "reqPH": false,
                                "hqp": false
                            },
                            "20233": {
                                "id": "20233",
                                "ttl": "Hair Care 2 Shelf Side Kick Promotion",
                                "cm": "278",
                                "l_note": "",
                                "note": null,
                                "p_st": null,
                                "stdt": "6/26/2014",
                                "endt": "12/31/2025",
                                "st_now": "",
                                "last": "2014/07/18 09:27:45 - Up",
                                "summ": "<html>\r\n\r\n<head>\r\n<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\r\n<meta name=Generator content=\"Microsoft Word 14 (filtered)\">\r\n<style>\r\n<!--\r\n /* Font Definitions */\r\n @font-face\r\n\t{font-family:Calibri;\r\n\tpanose-1:2 15 5 2 2 2 4 3 2 4;}\r\n@font-face\r\n\t{font-family:Tahoma;\r\n\tpanose-1:2 11 6 4 3 5 4 4 2 4;}\r\n /* Style Definitions */\r\n p.MsoNormal, li.MsoNormal, div.MsoNormal\r\n\t{margin-top:0in;\r\n\tmargin-right:0in;\r\n\tmargin-bottom:10.0pt;\r\n\tmargin-left:0in;\r\n\tline-height:115%;\r\n\tfont-size:11.0pt;\r\n\tfont-family:\"Calibri\",\"sans-serif\";}\r\np.MsoAcetate, li.MsoAcetate, div.MsoAcetate\r\n\t{mso-style-link:\"Balloon Text Char\";\r\n\tmargin:0in;\r\n\tmargin-bottom:.0001pt;\r\n\tfont-size:8.0pt;\r\n\tfont-family:\"Tahoma\",\"sans-serif\";}\r\nspan.BalloonTextChar\r\n\t{mso-style-name:\"Balloon Text Char\";\r\n\tmso-style-link:\"Balloon Text\";\r\n\tfont-family:\"Tahoma\",\"sans-serif\";}\r\n.MsoChpDefault\r\n\t{font-family:\"Calibri\",\"sans-serif\";}\r\n.MsoPapDefault\r\n\t{margin-bottom:10.0pt;\r\n\tline-height:115%;}\r\n@page WordSection1\r\n\t{size:8.5in 11.0in;\r\n\tmargin:1.0in 1.0in 1.0in 1.0in;}\r\ndiv.WordSection1\r\n\t{page:WordSection1;}\r\n-->\r\n</style>\r\n\r\n</head>\r\n\r\n<body lang=EN-US>\r\n\r\n<div class=WordSection1>\r\n\r\n<p class=MsoNormal><img width=624 height=445 id=\"Picture 1\"\r\nsrc=\"two%20tier_files/image001.png\"></p>\r\n\r\n</div>\r\n\r\n</body>\r\n\r\n</html>\r\n",
                                "item_folder": [{
                                    "id": "177573",
                                    "pt": "Pure Haircare Volume Shampoo 13.5z",
                                    "pid": "113205",
                                    "sz": "13.5Z",
                                    "pk": "00",
                                    "ic": "10001",
                                    "cupc": "336600100011",
                                    "upc": "336600100011",
                                    "wupc": "0033660010001",
                                    "client": "Pure",
                                    "scode": "453454",
                                    "on": false
                                }, {
                                    "id": "177574",
                                    "pt": "Pure Haircare Volume Conditioner 12z",
                                    "pid": "113208",
                                    "sz": "12Z",
                                    "pk": "00",
                                    "ic": "10004",
                                    "cupc": "336600100044",
                                    "upc": "336600100044",
                                    "wupc": "0033660010004",
                                    "client": "Pure",
                                    "scode": "453455",
                                    "on": false
                                }, {
                                    "id": "177575",
                                    "pt": "Pure Haircare Ultra Shine Shampoo 13.5z",
                                    "pid": "113206",
                                    "sz": "13.5Z",
                                    "pk": "00",
                                    "ic": "10002",
                                    "cupc": "336600100022",
                                    "upc": "336600100022",
                                    "wupc": "0033660010002",
                                    "client": "Pure",
                                    "scode": "453456",
                                    "on": false
                                }, {
                                    "id": "177576",
                                    "pt": "Pure Haircare Ultra Shine Conditioner 12z",
                                    "pid": "113209",
                                    "sz": "12Z",
                                    "pk": "00",
                                    "ic": "10005",
                                    "cupc": "336600100055",
                                    "upc": "336600100055",
                                    "wupc": "0033660010005",
                                    "client": "Pure",
                                    "scode": "453457",
                                    "on": false
                                }, {
                                    "id": "177577",
                                    "pt": "Pure Haircare Silky Smooth Shampoo 13.5z",
                                    "pid": "113207",
                                    "sz": "13.5Z",
                                    "pk": "00",
                                    "ic": "10003",
                                    "cupc": "336600100033",
                                    "upc": "336600100033",
                                    "wupc": "0033660010003",
                                    "client": "Pure",
                                    "scode": "453459",
                                    "on": false
                                }, {
                                    "id": "177578",
                                    "pt": "Pure Haircare Silky Smooth Conditioner 12z",
                                    "pid": "113211",
                                    "sz": "12Z",
                                    "pk": "00",
                                    "ic": "10007",
                                    "cupc": "336600100077",
                                    "upc": "336600100077",
                                    "wupc": "0033660010007",
                                    "client": "Pure",
                                    "scode": "453460",
                                    "on": false
                                }],
                                "loc_folder": [{
                                    "id": null,
                                    "loc": {
                                        "id": "2ef4d7e2-78a1-4f64-b5c5-47e2485553a8",
                                        "pt": "Primary Display Location (PDL)"
                                    },
                                    "qty": null,
                                    "uom": null,
                                    "l_st": null,
                                    "l_rsn": null,
                                    "can_modify": true
                                }],
                                "img": [{
                                    "imgsrc": "src=\"two%20tier_files/image001.png\"",
                                    "id": "-7199593841249788359",
                                    "full_img": false
                                }],
                                "def_loc": {
                                    "id": "2ef4d7e2-78a1-4f64-b5c5-47e2485553a8",
                                    "pt": "Primary Display Location (PDL)"
                                },
                                "def_uom": {
                                    "id": "490",
                                    "pt": "Display(s)"
                                },
                                "pc": {
                                    "id": "124",
                                    "pt": "Hair - Shampoo/Cond"
                                },
                                "ASMID": "ASM_20233",
                                "atch": "",
                                "URL": "",
                                "reqPH": false,
                                "hqp": false
                            }
                        }
                    },
                    "pt": "Hair - Shampoo/Cond"
                }, {
                    "id": "250",
                    "dept": "HBC",
                    "ord": "9998",
                    "distribution_folder": {
                        "113218": {
                            "codes": "|336600100144|336600100144|0033660010014| 336600100144||033660010014|336600100144|~|453452|",
                            "PID": "113218",
                            "CM": "278",
                            "IS": " ",
                            "wf_r": "",
                            "OS": {
                                "is_positive": false,
                                "id": "4",
                                "pt": "Out-Of-Stock (OOS)"
                            },
                            "PF": "9112",
                            "CL": "1865",
                            "CPT": "Hair Care",
                            "oth_info": " |DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~P~P~P|06Z|00|Pure|10014||| |Yes|33|2014/07/18 09:27:46~DV~CF|| |8ft, 16ft",
                            "PB": "5850",
                            "url": "",
                            "WF": true,
                            "WD": true,
                            "Com": false,
                            "NI": false,
                            "AI": true,
                            "ZS": false,
                            "SC": false,
                            "bad": true,
                            "AT": true,
                            "WF2": false,
                            "invres": null,
                            "aux": {
                                "Fields": {}
                            },
                            "wf_sr": "Auto",
                            "id": "527388201",
                            "pt": "Pure Haircare Split End Spray 6z",
                            "AC": {
                                "id": "2",
                                "pt": "Filled (F)"
                            }
                        }
                    },
                    "tasks": {
                        "folder": []
                    },
                    "surveys": {
                        "folder": {}
                    },
                    "promotions": {
                        "folder": {}
                    },
                    "pt": "Hair Care"
                }, {
                    "id": "510",
                    "dept": "HBC",
                    "ord": "99910",
                    "distribution_folder": {
                        "113219": {
                            "codes": "|336600100155|336600100155|0033660010015| 336600100155||033660010015|336600100155|~|453450|",
                            "PID": "113219",
                            "CM": "278",
                            "IS": " ",
                            "wf_r": "",
                            "OS": {
                                "is_positive": false,
                                "id": "2",
                                "pt": "On Shelf (SH)"
                            },
                            "PF": "9112",
                            "CL": "1865",
                            "CPT": "Health & Beauty",
                            "oth_info": " |NT|6/2/2014~NT~CF~5/27/2014~NT~CF~5/23/2014~NT~CF~P~P~P|01EA|00|Pure|10015||| |Yes|22|2014/07/18 09:27:46~NT~CF|| |All Sets",
                            "PB": "5850",
                            "url": "",
                            "WF": true,
                            "WD": true,
                            "Com": false,
                            "NI": false,
                            "AI": true,
                            "ZS": false,
                            "SC": false,
                            "bad": true,
                            "AT": true,
                            "WF2": false,
                            "invres": null,
                            "aux": {
                                "Fields": {}
                            },
                            "wf_sr": "Auto",
                            "id": "527388203",
                            "pt": "Pure Haircare Curling Iron .75in",
                            "AC": {
                                "id": "2",
                                "pt": "Order Suggested (ORS)"
                            }
                        }
                    },
                    "tasks": {
                        "folder": []
                    },
                    "surveys": {
                        "folder": {}
                    },
                    "promotions": {
                        "folder": {}
                    },
                    "pt": "Health & Beauty"
                }, {
                    "id": "144",
                    "dept": "HBC",
                    "ord": "9994",
                    "distribution_folder": {},
                    "tasks": {
                        "folder": [{
                            "ttl": "Checkout Lanes",
                            "st": "2014/06/26 00:00:00",
                            "en": "2025/12/31 00:00:00",
                            "client": {
                                "id": "1865",
                                "pt": "Pure"
                            },
                            "cat": {
                                "id": "144",
                                "pt": "Hair - Accessories"
                            },
                            "prod": {
                                "id": "113216",
                                "pt": "Pure Haircare multi-pack head bands"
                            },
                            "typ": {
                                "sig_req": false,
                                "id": "623",
                                "pt": "BDM Generated"
                            },
                            "sts": null,
                            "desc": "Verify multi-pack head bands are stocked on all checkout lanes. (photo required)",
                            "subby": "Workflow User-ADMIN",
                            "rec": false,
                            "can_delete": false,
                            "can_modify": false,
                            "cm": "278",
                            "ct": "",
                            "atch": "",
                            "URL": "",
                            "reqPH": false,
                            "hqp": false,
                            "id": "a193124d-a9d0-4f94-ab87-7c71b9c2a80b"
                        }]
                    },
                    "surveys": {
                        "folder": {}
                    },
                    "promotions": {
                        "folder": {}
                    },
                    "pt": "Hair - Accessories"
                }],
                //[{ "id": "285", "dept": "Seasonal", "ord": "4", "distribution_folder": { "113213": { "codes": "|336600100099|336600100099|0033660010009| 336600100099||033660010009|336600100099|~|453463|", "PID": "113213", "CM": null, "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" }, "PF": "9113", "CL": "1865", "CPT": "Display", "oth_info": " |OOS|6/2/2014~OOS~CF~5/27/2014~OOS~CF~5/23/2014~OOS~CF~~~|01DSP|00|Pure|10009||| |Yes|PIC|2014/07/18 09:27:45~OOS~CF|| |All Sets", "PB": "5850", "url": "https://www.asmconnects.com", "WF": true, "WD": true, "Com": false, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "Auto", "id": "527388198", "pt": "Pure Haircare Floor Display", "AC": { "id": "5", "pt": "Ordered (OR)" } } }, "tasks": { "folder": [] }, "surveys": { "folder": {} }, "promotions": { "folder": {} }, "pt": "Display" }, { "id": "124", "dept": "HBC", "ord": "9996", "distribution_folder": { "113205": { "codes": "|336600100011|336600100011|0033660010001| 336600100011||033660010001|336600100011|~|453454|", "PID": "113205", "CM": null, "IS": "U", "wf_r": "", "OS": { "is_positive": false, "id": "4", "pt": "Distribution Void (DV)" }, "PF": "9113", "CL": "1865", "CPT": "Hair - Shampoo/Cond", "oth_info": "U|DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~~~|13.5Z|00|Pure|10001|||U|No|PIC|2014/07/18 09:27:45~DV~CF||U|All Sets", "PB": "5850", "url": "", "WF": true, "WD": true, "Com": false, "NI": true, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "New Unconfirmed", "id": "527388210", "prompt": "Pure Haircare Volume Shampoo 13.5z", "AC": { "id": "5", "pt": "Ordered (OR)" } }, "113206": { "codes": "|336600100022|336600100022|0033660010002| 336600100022||033660010002|336600100022|~|453456|", "PID": "113206", "CM": null, "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" }, "PF": "9113", "CL": "1865", "CPT": "Hair - Shampoo/Cond", "oth_info": " |DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~~~|13.5Z|00|Pure|10002||| |Yes|PIC|2014/07/18 09:27:45~DV~CF|| |", "PB": "5850", "url": "", "WF": true, "WD": true, "Com": true, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "Auto", "id": "527388207", "prompt": "Pure Haircare Ultra Shine Shampoo 13.5z", "is_modified": true, "last_updated": "2017/08/04 13:36:56", "AC": { "id": "5", "pt": "Ordered (OR)" }, "RS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" } }, "113207": { "codes": "|336600100033|336600100033|0033660010003| 336600100033||033660010003|336600100033|~|453459|", "PID": "113207", "CM": "278", "IS": " ", "wf_r": "P", "OS": { "is_positive": false, "id": "4", "pt": "Distribution Void (DV)" }, "PF": "9113", "CL": "1865", "CPT": "Hair - Shampoo/Cond", "oth_info": " |DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~P~P~P|13.5Z|00|Pure|10003||| |Yes|15|2014/07/18 09:27:45~DV~CF|| |12ft", "PB": "5850", "url": "", "WF": true, "WD": true, "Com": true, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "P", "id": "527388199", "prompt": "Pure Haircare Silky Smooth Shampoo 13.5z", "is_modified": true, "last_updated": "2017/08/04 13:36:46", "AC": { "id": "4", "pt": "Confirmed (CF)" }, "RS": { "is_positive": false, "id": "4", "pt": "Distribution Void (DV)" } }, "113208": { "codes": "|336600100044|336600100044|0033660010004| 336600100044||033660010004|336600100044|~|453455|", "PID": "113208", "CM": "278", "IS": " ", "wf_r": "Z", "OS": { "is_positive": true, "id": 12, "pt": "Distribution Void (DV)" }, "PF": "9113", "CL": "1865", "CPT": "Hair - Shampoo/Cond", "oth_info": " ||~~~~~~~~~~~|12Z|00|Pure|10004||| |Yes|PIC|~~|| |All Sets", "PB": "5850", "url": "", "WF": true, "WD": true, "Com": false, "NI": false, "AI": true, "ZS": true, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "Z", "id": "527388204", "prompt": "Pure Haircare Volume Conditioner 12z", "AC": { "id": "2", "pt": "Tagged (TG)" } }, "113210": { "codes": "|336600100066|336600100066|0033660010006| 336600100066||033660010006|336600100066|~|453458|", "PID": "113210", "CM": null, "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "4", "pt": "Distribution Void (DV)" }, "PF": "9113", "CL": "1865", "CPT": "Hair - Shampoo/Cond", "oth_info": " |DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~~~|12Z|00|Pure|10006||| |Yes|PIC|2014/07/18 09:27:45~DV~CF|| |", "PB": "5850", "url": "", "WF": true, "WD": true, "Com": true, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "Auto", "id": "527388202", "prompt": "Pure Haircare Soft Curls Conditioner 12z", "is_modified": true, "last_updated": "2017/08/04 13:36:51", "AC": { "id": "2", "pt": "Tagged (TG)" }, "RS": { "is_positive": false, "id": "1", "pt": "Out-Of-Stock (OOS)" } } }, "tasks": { "folder": [{ "ttl": "Trial Size Checkout Lanes", "st": "2014/06/26 00:00:00", "en": "2025/12/31 00:00:00", "client": { "id": "1865", "pt": "Pure" }, "cat": { "id": "124", "pt": "Hair - Shampoo/Cond" }, "prod": null, "typ": { "sig_req": false, "id": "623", "pt": "BDM Generated" }, "sts": null, "desc": "Verify trial size shampoo and conditioner are on display in the checkout lanes.  If needed, restock product.", "subby": "Workflow User-ADMIN", "rec": true, "can_delete": false, "can_modify": false, "cm": "278", "ct": "", "atch": "", "URL": "", "reqPH": false, "hqp": false, "id": "3de188fb-fedf-482b-a026-73ebf1cd1bd6" }] }, "surveys": { "folder": { "35000": { "pt": "HairCare Survey", "id": "35000", "folder": [{ "values": [{ "id": "6285", "pt": "2 tier wire rack" }, { "id": "6283", "pt": "4 tier floor stand" }, { "id": "6286", "pt": "Head band clipstrip" }, { "id": "6284", "pt": "HBA Endcap for BOGO" }], "id": "96373", "pt": "Which displays are currently up in this store (mark all that apply)?", "answer": null, "group": "35000", "type": "CHK", "qtype": "Checklist", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_96373", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "278", "reqPH": false, "hqp": false }, { "values": [{ "id": "109801", "pt": "Yes" }, { "id": "109802", "pt": "No" }, { "id": "109803", "pt": "There are NO U-scan checkouts in this store" }], "id": "96327", "pt": "Is the U-Scan checkout area in this store merchandised with mulit pack head bands?", "answer": null, "group": "35000", "type": "PICK", "qtype": "Pick List", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_96327", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "278", "reqPH": false, "hqp": false }, { "min": "0", "max": "999999", "decimal": false, "after_dec": "0", "id": "121194", "pt": "How many Pure Haircare displays are currently up in the store?", "answer": null, "group": "35000", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_121194", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "278", "reqPH": false, "hqp": false }, { "min": "", "max": "", "id": "96374", "pt": "What is the expiration date on the ultra shine shampoo (first date to expire)?", "answer": null, "group": "35000", "type": "DATE", "qtype": "Date", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_96374", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "278", "reqPH": false, "hqp": false }, { "min": "0", "max": "50", "decimal": true, "after_dec": "2", "id": "96375", "pt": "What is the price of 20oz Rose Petals conditioner?", "answer": null, "group": "35000", "type": "NUMBER", "qtype": "Number with Decimals", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_96375", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "278", "reqPH": false, "hqp": false }, { "min": "0", "max": "60", "decimal": false, "after_dec": "0", "id": "96331", "pt": "How many active front-end checkouts are in this store?", "answer": null, "group": "35000", "type": "NUMBER", "qtype": "Numeric", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_96331", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "278", "reqPH": false, "hqp": false }, { "id": "96376", "pt": "Who is the main contact for the Health and Beauty Department (first & last name)?", "answer": null, "group": "35000", "type": "TEXT", "qtype": "Text", "last_ans": "", "act": "1", "sbc": null, "sbs": null, "condition": null, "gq": null, "iscon": false, "ASMID": "ASM_96376", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "278", "reqPH": false, "hqp": false }, { "values": [{ "id": "0", "pt": "Yes" }, { "id": "1", "pt": "No" }, { "id": "2", "pt": "NA" }], "id": "96377", "pt": "Is the 4 Tier Pure Haircare display up in this store?", "answer": null, "group": "35000", "type": "PICK", "qtype": "YNNA", "last_ans": "", "act": "1", "sbc": "0", "sbs": "", "condition": null, "gq": "122383", "iscon": false, "ASMID": "ASM_96377", "att1": "", "att2": "", "att3": "", "att4": "", "att5": "", "att6": "", "att7": "", "att8": "", "att9": "", "att10": "", "cm": "278", "reqPH": false, "hqp": false }], "cm": "278", "ct": null, "iscon": false, "atch": "", "URL": "https://www.asmconnects.com/sites/artsuni/doclock/Demo%20Team/National/In%20Line%20Fixture.pdf" } } }, "promotions": { "folder": { "20220": { "id": "20220", "ttl": "HairCare 4 Tier Rack Display Promotion", "cm": "278", "l_note": null, "note": null, "p_st": null, "stdt": "3/27/2017", "endt": "12/31/2025", "st_now": "", "last": "No History Available", "summ": "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n<html>\r\n  <head>\r\n    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\r\n    <title>bullet</title>\r\n  </head>\r\n  <body>\r\n    <div align=\"center\"><a href=\"twopromopic.JPG\"><img alt=\"\"\r\n          src=\"twopromopic.JPG\"></a></div>\r\n  </body>\r\n</html>\r\n", "item_folder": [{ "id": "177568", "pt": "Pure Haircare Volume Shampoo 13.5z", "pid": "113205", "sz": "13.5Z", "pk": "00", "ic": "10001", "cupc": "336600100011", "upc": "336600100011", "wupc": "0033660010001", "client": "Pure", "scode": "453454", "on": false }, { "id": "177569", "pt": "Pure Haircare Volume Conditioner 12z", "pid": "113208", "sz": "12Z", "pk": "00", "ic": "10004", "cupc": "336600100044", "upc": "336600100044", "wupc": "0033660010004", "client": "Pure", "scode": "453455", "on": false }, { "id": "177596", "pt": "Pure Haircare Ultra Shine Shampoo 13.5z", "pid": "113206", "sz": "13.5Z", "pk": "00", "ic": "10002", "cupc": "336600100022", "upc": "336600100022", "wupc": "0033660010002", "client": "Pure", "scode": "453456", "on": false }, { "id": "177597", "pt": "Pure Haircare Silky Smooth Shampoo 13.5z", "pid": "113207", "sz": "13.5Z", "pk": "00", "ic": "10003", "cupc": "336600100033", "upc": "336600100033", "wupc": "0033660010003", "client": "Pure", "scode": "453459", "on": false }, { "id": "177598", "pt": "Pure Haircare Ultra Shine Conditioner 12z", "pid": "113209", "sz": "12Z", "pk": "00", "ic": "10005", "cupc": "336600100055", "upc": "336600100055", "wupc": "0033660010005", "client": "Pure", "scode": "453457", "on": false }], "loc_folder": [], "img": [{ "imgsrc": "src=\"twopromopic.JPG\"", "id": "6411941406153872011", "full_img": false }], "def_loc": { "id": "2ef4d7e2-78a1-4f64-b5c5-47e2485553a8", "pt": "Primary Display Location (PDL)" }, "def_uom": { "id": "490", "pt": "Display(s)" }, "pc": { "id": "124", "pt": "Hair - Shampoo/Cond" }, "ASMID": "ASM_20220", "atch": "", "URL": "", "reqPH": false, "hqp": false }, "20233": { "id": "20233", "ttl": "Hair Care 2 Shelf Side Kick Promotion", "cm": "278", "l_note": "", "note": null, "p_st": null, "stdt": "6/26/2014", "endt": "12/31/2025", "st_now": "", "last": "2014/07/18 09:27:45 - Up", "summ": "<html>\r\n\r\n<head>\r\n<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\r\n<meta name=Generator content=\"Microsoft Word 14 (filtered)\">\r\n<style>\r\n<!--\r\n /* Font Definitions */\r\n @font-face\r\n\t{font-family:Calibri;\r\n\tpanose-1:2 15 5 2 2 2 4 3 2 4;}\r\n@font-face\r\n\t{font-family:Tahoma;\r\n\tpanose-1:2 11 6 4 3 5 4 4 2 4;}\r\n /* Style Definitions */\r\n p.MsoNormal, li.MsoNormal, div.MsoNormal\r\n\t{margin-top:0in;\r\n\tmargin-right:0in;\r\n\tmargin-bottom:10.0pt;\r\n\tmargin-left:0in;\r\n\tline-height:115%;\r\n\tfont-size:11.0pt;\r\n\tfont-family:\"Calibri\",\"sans-serif\";}\r\np.MsoAcetate, li.MsoAcetate, div.MsoAcetate\r\n\t{mso-style-link:\"Balloon Text Char\";\r\n\tmargin:0in;\r\n\tmargin-bottom:.0001pt;\r\n\tfont-size:8.0pt;\r\n\tfont-family:\"Tahoma\",\"sans-serif\";}\r\nspan.BalloonTextChar\r\n\t{mso-style-name:\"Balloon Text Char\";\r\n\tmso-style-link:\"Balloon Text\";\r\n\tfont-family:\"Tahoma\",\"sans-serif\";}\r\n.MsoChpDefault\r\n\t{font-family:\"Calibri\",\"sans-serif\";}\r\n.MsoPapDefault\r\n\t{margin-bottom:10.0pt;\r\n\tline-height:115%;}\r\n@page WordSection1\r\n\t{size:8.5in 11.0in;\r\n\tmargin:1.0in 1.0in 1.0in 1.0in;}\r\ndiv.WordSection1\r\n\t{page:WordSection1;}\r\n-->\r\n</style>\r\n\r\n</head>\r\n\r\n<body lang=EN-US>\r\n\r\n<div class=WordSection1>\r\n\r\n<p class=MsoNormal><img width=624 height=445 id=\"Picture 1\"\r\nsrc=\"two%20tier_files/image001.png\"></p>\r\n\r\n</div>\r\n\r\n</body>\r\n\r\n</html>\r\n", "item_folder": [{ "id": "177573", "pt": "Pure Haircare Volume Shampoo 13.5z", "pid": "113205", "sz": "13.5Z", "pk": "00", "ic": "10001", "cupc": "336600100011", "upc": "336600100011", "wupc": "0033660010001", "client": "Pure", "scode": "453454", "on": false }, { "id": "177574", "pt": "Pure Haircare Volume Conditioner 12z", "pid": "113208", "sz": "12Z", "pk": "00", "ic": "10004", "cupc": "336600100044", "upc": "336600100044", "wupc": "0033660010004", "client": "Pure", "scode": "453455", "on": false }, { "id": "177575", "pt": "Pure Haircare Ultra Shine Shampoo 13.5z", "pid": "113206", "sz": "13.5Z", "pk": "00", "ic": "10002", "cupc": "336600100022", "upc": "336600100022", "wupc": "0033660010002", "client": "Pure", "scode": "453456", "on": false }, { "id": "177576", "pt": "Pure Haircare Ultra Shine Conditioner 12z", "pid": "113209", "sz": "12Z", "pk": "00", "ic": "10005", "cupc": "336600100055", "upc": "336600100055", "wupc": "0033660010005", "client": "Pure", "scode": "453457", "on": false }, { "id": "177577", "pt": "Pure Haircare Silky Smooth Shampoo 13.5z", "pid": "113207", "sz": "13.5Z", "pk": "00", "ic": "10003", "cupc": "336600100033", "upc": "336600100033", "wupc": "0033660010003", "client": "Pure", "scode": "453459", "on": false }, { "id": "177578", "pt": "Pure Haircare Silky Smooth Conditioner 12z", "pid": "113211", "sz": "12Z", "pk": "00", "ic": "10007", "cupc": "336600100077", "upc": "336600100077", "wupc": "0033660010007", "client": "Pure", "scode": "453460", "on": false }], "loc_folder": [{ "id": null, "loc": { "id": "2ef4d7e2-78a1-4f64-b5c5-47e2485553a8", "pt": "Primary Display Location (PDL)" }, "qty": null, "uom": null, "l_st": null, "l_rsn": null, "can_modify": true }], "img": [{ "imgsrc": "src=\"two%20tier_files/image001.png\"", "id": "-7199593841249788359", "full_img": false }], "def_loc": { "id": "2ef4d7e2-78a1-4f64-b5c5-47e2485553a8", "pt": "Primary Display Location (PDL)" }, "def_uom": { "id": "490", "pt": "Display(s)" }, "pc": { "id": "124", "pt": "Hair - Shampoo/Cond" }, "ASMID": "ASM_20233", "atch": "", "URL": "", "reqPH": false, "hqp": false } } }, "pt": "Hair - Shampoo/Cond" }, { "id": "250", "dept": "HBC", "ord": "9998", "distribution_folder": { "113218": { "codes": "|336600100144|336600100144|0033660010014| 336600100144||033660010014|336600100144|~|453452|", "PID": "113218", "CM": "278", "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "4", "pt": "Distribution Void (DV)" }, "PF": "9112", "CL": "1865", "CPT": "Hair Care", "oth_info": " |DV|6/2/2014~DV~CF~5/27/2014~DV~CF~5/23/2014~DV~CF~P~P~P|06Z|00|Pure|10014||| |Yes|33|2014/07/18 09:27:46~DV~CF|| |8ft, 16ft", "PB": "5850", "url": "", "WF": true, "WD": true, "Com": false, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "Auto", "id": "527388201", "pt": "Pure Haircare Split End Spray 6z", "AC": { "id": "2", "pt": "Confirmed (CF)" } } }, "tasks": { "folder": [] }, "surveys": { "folder": {} }, "promotions": { "folder": {} }, "pt": "Hair Care" }, { "id": "510", "dept": "HBC", "ord": "99910", "distribution_folder": { "113219": { "codes": "|336600100155|336600100155|0033660010015| 336600100155||033660010015|336600100155|~|453450|", "PID": "113219", "CM": "278", "IS": " ", "wf_r": "", "OS": { "is_positive": false, "id": "2", "pt": "Distribution Void (DV)" }, "PF": "9112", "CL": "1865", "CPT": "Health & Beauty", "oth_info": " |NT|6/2/2014~NT~CF~5/27/2014~NT~CF~5/23/2014~NT~CF~P~P~P|01EA|00|Pure|10015||| |Yes|22|2014/07/18 09:27:46~NT~CF|| |All Sets", "PB": "5850", "url": "", "WF": true, "WD": true, "Com": false, "NI": false, "AI": true, "ZS": false, "SC": false, "bad": true, "AT": true, "WF2": false, "invres": null, "aux": { "Fields": {} }, "wf_sr": "Auto", "id": "527388203", "pt": "Pure Haircare Curling Iron .75in", "AC": { "id": "2", "pt": "Tagged (TG)" } } }, "tasks": { "folder": [] }, "surveys": { "folder": {} }, "promotions": { "folder": {} }, "pt": "Health & Beauty" }, { "id": "144", "dept": "HBC", "ord": "9994", "distribution_folder": {}, "tasks": { "folder": [{ "ttl": "Checkout Lanes", "st": "2014/06/26 00:00:00", "en": "2025/12/31 00:00:00", "client": { "id": "1865", "pt": "Pure" }, "cat": { "id": "144", "pt": "Hair - Accessories" }, "prod": { "id": "113216", "pt": "Pure Haircare multi-pack head bands" }, "typ": { "sig_req": false, "id": "623", "pt": "BDM Generated" }, "sts": null, "desc": "Verify multi-pack head bands are stocked on all checkout lanes. (photo required)", "subby": "Workflow User-ADMIN", "rec": false, "can_delete": false, "can_modify": false, "cm": "278", "ct": "", "atch": "", "URL": "", "reqPH": false, "hqp": false, "id": "a193124d-a9d0-4f94-ab87-7c71b9c2a80b" }] }, "surveys": { "folder": {} }, "promotions": { "folder": {} }, "pt": "Hair - Accessories" }],

               aw: [
{
    "id": "6",
    "dept": "01 Candy Gondola",
    "ord": "4",
    "distribution_folder": {
        "6438": {
            "codes": "|040000201243|20124|0004000020124| 040000201243||040000201243|004000020124|~|90983|~|04000020124|",
            "PID": "6438",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5355",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|10.5Z|24|Mars Inc.|20124||| |Yes|PIC|2016/02/04 12:28:30~DV~CF|| |",
            "PB": "448",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546914",
            "prompt": "Miniature Mixed Variety 10.5z"
        },
        "6439": {
            "codes": "|040000203193|20319|0004000020319| 040000203193||040000203193|004000020319|~|20319|~|04000020319|",
            "PID": "6439",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5355",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|40Z|06|Mars Inc.|20319||| |Yes|PIC|2016/02/04 12:28:33~DV~CF|| |",
            "PB": "448",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546960",
            "prompt": "Miniature Mixed Variety 40z"
        },
        "6459": {
            "codes": "|040000201328|20132|0004000020132| 040000201328||040000201328|004000020132|~|90951|~|04000020132|",
            "PID": "6459",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5355",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.5Z|24|Mars Inc.|20132||| |Yes|PIC|2016/02/04 12:28:35~DV~CF|| |",
            "PB": "452",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546930",
            "prompt": "Miniature Snickers 11.5z"
        },
        "6465": {
            "codes": "|040000202530|20253|0004000020253| 040000202530||040000202530|004000020253|~|90949|~|04000020253|",
            "PID": "6465",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5355",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.5Z|12|Mars Inc.|20253||| |Yes|PIC|2016/02/04 12:28:37~DV~CF|| |",
            "PB": "454",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546944",
            "prompt": "Miniature Twix Caramel 11.5z"
        },
        "6476": {
            "codes": "|040000443278|10040000443275|0004000044327| 040000443278||040000443278|004000044327|~|01602|~|04000044327|",
            "PID": "6476",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5246",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|12.42Z|24|Mars Inc.|44327||| |Yes|PIC|2016/02/04 12:28:39~DV~CF|| |",
            "PB": "452",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546620",
            "prompt": "Multipack Snickers 6pk"
        },
        "6490": {
            "codes": "|040000151227|15122|0004000015122| 040000151227||040000151227|004000015122|~|96753|~|04000015122|",
            "PID": "6490",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11Z|24|Mars Inc.|15122||| |Yes|PIC|2016/02/04 12:26:58~DV~CF|| |-",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546633",
            "prompt": "Fun Size 3 Musketeers 11z"
        },
        "6492": {
            "codes": "|040000151241|15124|0004000015124| 040000151241||040000151241|004000015124|~|96743|~|04000015124|",
            "PID": "6492",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.24Z|36|Mars Inc.|15124||| |Yes|PIC|2016/02/04 12:27:19~DV~CF|| |-",
            "PB": "447",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546644",
            "prompt": "Fun Size Milky Way 11.24z"
        },
        "6493": {
            "codes": "|040000151401|15140|0004000015140| 040000151401||040000151401|004000015140|~|96750|~|04000015140|",
            "PID": "6493",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.18Z|36|Mars Inc.|15140||| |Yes|PIC|2016/02/04 12:27:22~DV~CF|| |-",
            "PB": "452",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546652",
            "prompt": "Fun Size Snickers 11.8z"
        },
        "6495": {
            "codes": "|040000151784|15178|0004000015178| 040000151784||040000151784|004000015178|~|002859||0195535|~|04000015178|",
            "PID": "6495",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.4Z|24|Mars Inc.|15178||| |Yes|PIC|2016/02/04 12:27:28~DV~CF|| |-",
            "PB": "454",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546659",
            "prompt": "Fun Size Twix Caramel 11.4z"
        },
        "6654": {
            "codes": "|040000017318|01731|0004000001731| 040000017318||040000017318|004000001731|~|66431|~|04000001731|",
            "PID": "6654",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5357",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|05.3Z|12|Mars Inc.|01731||| |Yes|PIC|2016/02/04 12:28:41~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546733",
            "prompt": "Peg M&M Milk Choc 5.3z"
        },
        "6655": {
            "codes": "|040000017448|01744|0004000001744| 040000017448||040000017448|004000001744|~|66424|~|04000001744|",
            "PID": "6655",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5357",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|05.14Z|12|Mars Inc.|01744||| |Yes|PIC|2016/02/04 12:28:44~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546743",
            "prompt": "Peg M&M Milk Choc Peanut Butter 5.14z"
        },
        "6657": {
            "codes": "|040000017325|01732|0004000001732| 040000017325||040000017325|004000001732|~|66432|~|04000001732|",
            "PID": "6657",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5357",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|05.1Z|12|Mars Inc.|01732||| |Yes|PIC|2016/02/04 12:28:42~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546775",
            "prompt": "Peg M&M Milk Choc Peanut 5.1z"
        },
        "6666": {
            "codes": "|040000015024|01502|0004000001502| 040000015024||040000015024|004000001502|~|66433|~|04000001502|",
            "PID": "6666",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5357",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|04.4Z|12|Mars Inc.|01502||| |Yes|PIC|2016/02/04 12:29:01~DV~CF|| |",
            "PB": "452",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546793",
            "prompt": "Peg Snickers Miniatures 4.4z"
        },
        "42232": {
            "codes": "|040000249054|24905|0004000024905| 040000249054||040000249054|004000024905|~|82012|~|040000021285|~|04000024905|",
            "PID": "42232",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5353",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|09.9Z|12|Mars Inc.|24905||| |Yes|PIC|2016/02/04 12:27:48~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546869",
            "prompt": "M&M's Milk Choc Almond Bag 9.9z"
        },
        "42239": {
            "codes": "|040000249290|24929|0004000024929| 040000249290||040000249290|004000024929|~|24929|~|040000121329|~|04000024929|",
            "PID": "42239",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5353",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|19.2Z|12|Mars Inc.|24929||| |Yes|PIC|2016/02/04 12:27:59~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546881",
            "prompt": "M&M's Milk Choc Peanut Bag 19.2z"
        },
        "42243": {
            "codes": "|040000324386|32438|0004000032438| 040000324386||040000324386|004000032438|~|93726|~|040000229056|~|04000032438|",
            "PID": "42243",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5353",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|42Z|06|Mars Inc.|32438||| |Yes|PIC|2016/02/04 12:28:19~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546947",
            "prompt": "M&M's Stand Up Milk Choc 42z"
        },
        "62328": {
            "codes": "|040000395058|39505|0004000039505| 040000395058||040000395058|004000039505|~|1053292||0196712|~|04000039505|",
            "PID": "62328",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.5Z|12|Mars Inc.|39505||| |Yes|PIC|2016/02/04 12:27:25~DV~CF|| |",
            "PB": "452",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547112",
            "prompt": "Fun Size Snickers Peanut Butter Squared 11.5z"
        },
        "76502": {
            "codes": "|040000433057|43305|0004000043305| 040000433057||040000433057|004000043305|~|92749|~|04000043305|",
            "PID": "76502",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.23Z|24|Mars Inc.|43305||| |Yes|PIC|2016/02/04 12:27:12~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547102",
            "prompt": "Fun Size M&M Peanut 11.23z"
        },
        "82832": {
            "codes": "|040000464082|10040000464089|0004000046408| 040000464082||040000464082|004000046408|~|96773|~|04000046408|",
            "PID": "82832",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|3.40Z|24|Mars Inc.|46408||| |Yes|PIC|2016/02/04 12:28:53~DV~CF|| |",
            "PB": "452",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547077",
            "prompt": "Snickers 6pk Fun Size 3.4z"
        },
        "82833": {
            "codes": "|040000464105|10040000464102|0004000046410| 040000464105||040000464105|004000046410|~|96772|~|04000046410|",
            "PID": "82833",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|3.28Z|24|Mars Inc.|46410||| |Yes|PIC|2016/02/04 12:28:50~DV~CF|| |",
            "PB": "454",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547086",
            "prompt": "Twix 6pk Fun Size 3.28z"
        },
        "82834": {
            "codes": "|040000464266|10040000464263|0004000046426| 040000464266||040000464266|004000046426|~|96771|",
            "PID": "82834",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|3.36Z|24|Mars Inc.|46426||| |Yes|PIC|2016/02/04 12:28:24~DV~CF|| |",
            "PB": "447",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547082",
            "prompt": "Milky Way 6pk Fun Size 3.36z"
        },
        "82836": {
            "codes": "|040000453079|10040000464119|0004000046411| 040000464112||040000464112|004000046411|~|96776|",
            "PID": "82836",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|2.93Z|24|Mars Inc.|46411||| |Yes|PIC|2016/02/04 12:26:30~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547098",
            "prompt": "Pure Haircare Ultra Shine Shampoo 13.5 fl. Oz",
            "is_modified": true,
            "AC": {
                "id": "22",
                "pt": "Order Suggested (ORS)"
            },
            "last_updated": "2016/03/21 14:31:23",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "782836": {
            "codes": "|7040000453079|10040000464119|0004000046411| 040000464112||040000464112|004000046411|~|96776|",
            "PID": "82836",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|2.93Z|24|Mars Inc.|46411||| |Yes|PIC|2016/02/04 12:26:30~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547098",
            "prompt": "Pure Haircare Silky Smooth Conditioner 12.0 fl. Oz",
            "is_modified": true,
            "AC": {
                "id": "22",
                "pt": "Order Suggested (ORS)"
            },
            "last_updated": "2016/03/21 14:31:23",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "682836": {
            "codes": "|4040000453079|10040000464119|0004000046411| 040000464112||040000464112|004000046411|~|96776|",
            "PID": "82836",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|2.93Z|24|Mars Inc.|46411||| |Yes|PIC|2016/02/04 12:26:30~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547098",
            "prompt": "Pure Haircare Coconut Conditioner 6oz",
            "is_modified": true,
            "AC": {
                "id": "22",
                "pt": "Order Suggested (ORS)"
            },
            "last_updated": "2016/03/21 14:31:23",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "582836": {
            "codes": "|5040000453079|10040000464119|0004000046411| 040000464112||040000464112|004000046411|~|96776|",
            "PID": "82836",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|2.93Z|24|Mars Inc.|46411||| |Yes|PIC|2016/02/04 12:26:30~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547098",
            "prompt": "Pure Haircare Coconut Hand Sanitizer 12.5 fl. Oz.",
            "is_modified": true,
            "AC": {
                "id": "22",
                "pt": "Order Suggested (ORS)"
            },
            "last_updated": "2016/03/21 14:31:23",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "482836": {
            "codes": "|604000045307|10040000464119|0004000046411| 040000464112||040000464112|004000046411|~|96776|",
            "PID": "82836",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|2.93Z|24|Mars Inc.|46411||| |Yes|PIC|2016/02/04 12:26:30~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547098",
            "prompt": "Pure Haircare Volume Shampoo 13.5 fl oz.",
            "is_modified": true,
            "AC": {
                "id": "22",
                "pt": "Order Suggested (ORS)"
            },
            "last_updated": "2016/03/21 14:31:23",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "382836": {
            "codes": "|3040000453079|10040000464119|0004000046411| 040000464112||040000464112|004000046411|~|96776|",
            "PID": "82836",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|2.93Z|24|Mars Inc.|46411||| |Yes|PIC|2016/02/04 12:26:30~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547098",
            "prompt": "Pure Haircare Coconut Nourishing Masque 2oz",
            "is_modified": true,
            "AC": {
                "id": "22",
                "pt": "Order Suggested (ORS)"
            },
            "last_updated": "2016/03/21 14:31:23",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "282836": {
            "codes": "|2040000453079|10040000464119|0004000046411| 040000464112||040000464112|004000046411|~|96776|",
            "PID": "82836",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|2.93Z|24|Mars Inc.|46411||| |Yes|PIC|2016/02/04 12:26:30~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547098",
            "prompt": "Pure Haircare Coconut Straightening Therapy 6oz",
            "is_modified": true,
            "AC": {
                "id": "22",
                "pt": "Order Suggested (ORS)"
            },
            "last_updated": "2016/03/21 14:31:23",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "182836": {
            "codes": "|1040000453079|10040000464119|0004000046411| 040000464112||040000464112|004000046411|~|96776|",
            "PID": "82836",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|2.93Z|24|Mars Inc.|46411||| |Yes|PIC|2016/02/04 12:26:30~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547098",
            "prompt": "Pure Haircare Coconut Shampoo 6oz",
            "is_modified": true,
            "AC": {
                "id": "22",
                "pt": "Order Suggested (ORS)"
            },
            "last_updated": "2016/03/21 14:31:23",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "83406": {
            "codes": "|040000461081|10040000461088|0004000046108| 040000461081||040000461081|004000046108|~|93523|",
            "PID": "83406",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5350",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|35Z|06|Mars Inc.|46108||| |Yes|PIC|2016/02/04 12:27:16~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547525",
            "prompt": "Fun Size M&M Variety Pack 35z"
        },
        "83750": {
            "codes": "|040000457459|10040000457456|0004000045745| 040000457459||040000457459|004000045745|~|98185|~|405503942048|~|04000045745|",
            "PID": "83750",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5358",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|08Z|12|Mars Inc.|45745||| |Yes|PIC|2016/02/04 12:28:59~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547065",
            "prompt": "Snack Mix M&M Milk Chocolate 8z"
        },
        "83751": {
            "codes": "|040000457640|10040000457647|0004000045764| 040000457640||040000457640|004000045764|~|98133|~|04000045764|",
            "PID": "83751",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5358",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|08Z|12|Mars Inc.|45764||| |Yes|PIC|2016/02/04 12:28:55~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547073",
            "prompt": "Snack Mix M&M Peanut 8z"
        },
        "83752": {
            "codes": "|040000457572|10040000457579|0004000045757| 040000457572||040000457572|004000045757|~|98138|~|405503942038|~|04000045757|",
            "PID": "83752",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5358",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|08Z|12|Mars Inc.|45757||| |Yes|PIC|2016/02/04 12:29:04~DV~|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547067",
            "prompt": "Snack Mix M&M Dark Chocolate 8z"
        },
        "97252": {
            "codes": "|040000422518|10040000422515|0004000042251| 040000422518||040000422518|004000042251|~|94570|~|04000042251|",
            "PID": "97252",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9768",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|07Z|12|Mars Inc.|42251||| |Yes|PIC|2016/02/04 12:28:28~DV~CF|| |",
            "PB": "447",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547517",
            "prompt": "Milky Way Unwrapped Bites SUP 7z"
        },
        "97256": {
            "codes": "|040000422549|10040000422546|0004000042254| 040000422549||040000422549|004000042254|~|94569|~|04000042254|",
            "PID": "97256",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9768",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|08Z|12|Mars Inc.|42254||| |Yes|PIC|2016/02/04 12:28:52~DV~CF|| |",
            "PB": "452",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547521",
            "prompt": "Snickers Unwrapped Bites SUP 8z"
        },
        "108705": {
            "codes": "|040000485568|10040000485565|0004000048556| 040000485568||040000485568|004000048556|~|0194575|~|405520655078|",
            "PID": "108705",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            },
            "PF": "9768",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|06Z|08|Mars Inc.|48556||| |Yes|PIC|2016/02/04 12:26:33~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": false,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547555",
            "prompt": "3 Musketeers Bites Stand up Pouch 6z",
            "is_modified": true,
            "AC": {
                "id": "4",
                "pt": "Confirmed (CF)"
            },
            "last_updated": "2016/03/21 14:31:31",
            "RS": {
                "is_positive": true,
                "id": "3",
                "pt": "On Shelf (SH)"
            }
        },
        "108718": {
            "codes": "|040000485834|10040000485831|0004000048583| 040000485834||040000485834|004000048583|~| 94573||1052650||0194573|~|405520655108|",
            "PID": "108718",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9768",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|07Z|08|Mars Inc.|48583||| |Yes|PIC|2016/02/04 12:28:26~DV~CF|| |",
            "PB": "447",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547560",
            "prompt": "Milky Way Simply Caramel Bites Stand up Pouch 7z"
        },
        "112257": {
            "codes": "|040000484325|10040000484322|0004000048432| 040000484325||040000484325|004000048432|~|1051504||0196343|",
            "PID": "112257",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9768",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|07Z|08|Mars Inc.|38432||| |Yes|PIC|2016/02/04 12:28:48~DV~CF|| |",
            "PB": "454",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547587",
            "prompt": "Twix Bites SUP 7z 8cs"
        },
        "124439": {
            "codes": "|040000496045|040000496045|0004000049604| 040000496045||040000496045|004000049604|~~|040000250104|",
            "PID": "124439",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5353",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|10.2z|00|Mars Inc.|49604||| |Yes|PIC|2016/02/04 12:28:17~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672752506",
            "prompt": "M&M's Peanut Butter 10.2z"
        },
        "124441": {
            "codes": "|040000496021|040000496021|0004000049602| 040000496021||040000496021|004000049602|~~|040000248873|",
            "PID": "124441",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5353",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.4z|00|Mars Inc.|49602||| |Yes|PIC|2016/02/04 12:28:14~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672753812",
            "prompt": "M&M's Peanut 11.4z"
        },
        "124444": {
            "codes": "|040000495949|040000495949|0004000049594| 040000495949||040000495949|004000049594|~~|040000236153|",
            "PID": "124444",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5353",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|10.8z.|00|Mars Inc.|49594||| |Yes|PIC|2016/02/04 12:27:44~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672754274",
            "prompt": "M&M's Dark Chocolate Peanut 10.8z"
        },
        "124445": {
            "codes": "|040000495963|040000495963|0004000049596| 040000495963||040000495963|004000049596|~~|040000249061|",
            "PID": "124445",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5353",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|11.4z|00|Mars Inc.|49596||| |Yes|PIC|2016/02/04 12:28:10~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672751359",
            "prompt": "M&M's Milk Chocolate 11.4z"
        },
        "124446": {
            "codes": "|040000495925|040000495925|0004000049592| 040000495925||040000495925|004000049592|~~|040000249160|",
            "PID": "124446",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "5353",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|10.8z|00|Mars Inc.|49592||| |Yes|PIC|2016/02/04 12:27:40~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672754320",
            "prompt": "M&M's Dark Chocolate 10.8z"
        },
        "129452": {
            "codes": "|040000498582|553771588|0004000049858| 040000498582||040000498582|004000049858|~~|040000206408|",
            "PID": "129452",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "11012",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|08.87Z|12|Mars Inc.|49858||| |Yes|PIC|2016/02/04 12:26:41~DV~CF|| |",
            "PB": "455",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672755227",
            "prompt": "Dove Dark Chocolate Promises 8.87z 12cs",
            "is_modified": true,
            "AC": {
                "id": "4",
                "pt": "Confirmed (CF)"
            },
            "last_updated": "2016/03/21 14:31:42",
            "RS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            }
        },
        "129453": {
            "codes": "|040000498643|553771590|0004000049864| 040000498643||040000498643|004000049864|~~|040000265252|",
            "PID": "129453",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|07.94Z|12|Mars Inc.|49864||| |Yes|PIC|2016/02/04 12:26:36~DV~CF|| |",
            "PB": "455",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672756813",
            "prompt": "Dove Dark Chocolate Almond Promises 7.94z 12cs",
            "is_modified": true,
            "AC": {
                "id": "4",
                "pt": "Confirmed (CF)"
            },
            "last_updated": "2016/03/21 14:31:35",
            "RS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            }
        },
        "129456": {
            "codes": "|040000498605|553771586|0004000049860| 040000498605||040000498605|004000049860|~~|040000206385|",
            "PID": "129456",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "1",
                "pt": "Out-Of-Stock (OOS)"
            },
            "PF": "11012",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|08.87Z|12|Mars Inc.|49860||| |Yes|PIC|2016/02/04 12:26:48~DV~CF|| |",
            "PB": "455",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672755955",
            "prompt": "Dove Milk Chocolate Promises 8.87z 12cs",
            "is_modified": true,
            "AC": {
                "id": "4",
                "pt": "Ordered (OR)"
            },
            "last_updated": "2016/03/21 14:32:01",
            "RS": {
                "is_positive": false,
                "id": "1",
                "pt": "Changed" //"Out-Of-Stock (OOS)"
            }
        },
        "129457": {
            "codes": "|040000498629|553771587|0004000049862| 040000498629||040000498629|004000049862|~~|040000206378|",
            "PID": "129457",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "1",
                "pt": "Out-Of-Stock (OOS)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|07.94Z|12|Mars Inc.|49862||| |Yes|PIC|2016/02/04 12:26:45~DV~CF|| |",
            "PB": "455",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672756400",
            "prompt": "Dove Milk Chocolate Caramel Promises 7.94z 12cs",
            "is_modified": true,
            "AC": {
                "id": "4",
                "pt": "Confirmed (CF)"
            },
            "last_updated": "2016/03/21 14:31:53",
            "RS": {
                "is_positive": false,
                "id": "1",
                "pt": "Changed" //"Out-Of-Stock (OOS)"
            }
        },
        "12129457": {
            "codes": "|040000004318|553771587|0004000049862| 040000498629||040000498629|004000049862|~~|040000206378|",
            "PID": "129457",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "1",
                "pt": "Out-Of-Stock (OOS)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|07.94Z|12|Mars Inc.|49862||| |Yes|PIC|2016/02/04 12:26:45~DV~CF|| |",
            "PB": "455",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672756400",
            "prompt": "Dove Milk Chocolate Caramel Promises 7.94z 12cs",
            "is_modified": true,
            "AC": {
                "id": "4",
                "pt": "Confirmed (CF)"
            },
            "last_updated": "2016/03/21 14:31:53",
            "RS": {
                "is_positive": false,
                "id": "1",
                "pt": "Changed" //"Out-Of-Stock (OOS)"
            }
        },
        "1294457": {
            "codes": "|040000164685|553771587|0004000049862| 040000498629||040000498629|004000049862|~~|040000206378|",
            "PID": "129457",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "1",
                "pt": "Out-Of-Stock (OOS)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|07.94Z|12|Mars Inc.|49862||| |Yes|PIC|2016/02/04 12:26:45~DV~CF|| |",
            "PB": "455",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672756400",
            "prompt": "Dove Milk Chocolate Caramel Promises 7.94z 12cs",
            "is_modified": true,
            "AC": {
                "id": "4",
                "pt": "Confirmed (CF)"
            },
            "last_updated": "2016/03/21 14:31:53",
            "RS": {
                "is_positive": false,
                "id": "1",
                "pt": "Changed" //"Out-Of-Stock (OOS)"
            }
        },
        "1294257": {
            "codes": "|041419420065|553771587|0004000049862| 040000498629||040000498629|004000049862|~~|040000206378|",
            "PID": "129457",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "1",
                "pt": "Out-Of-Stock (OOS)"
            },
            "PF": "11013",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|07.94Z|12|Mars Inc.|49862||| |Yes|PIC|2016/02/04 12:26:45~DV~CF|| |",
            "PB": "455",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": true,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672756400",
            "prompt": "Dove Milk Chocolate Caramel Promises 7.94z 12cs",
            "is_modified": true,
            "AC": {
                "id": "4",
                "pt": "Confirmed (CF)"
            },
            "last_updated": "2016/03/21 14:31:53",
            "RS": {
                "is_positive": false,
                "id": "1",
                "pt": "Changed" //"Out-Of-Stock (OOS)"
            }
        },
        "131338": {
            "codes": "|040000496663|040000496663|0004000049666| 040000496663||040000496663|004000049666|~~|040000461081|",
            "PID": "131338",
            "CM": null,
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "598",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": " |DV|~~~~~~~~~~~|32.9Z|06|Mars Inc.|49666||| |Yes|PIC|2016/02/04 12:28:21~DV~CF|| |",
            "PB": "448",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "672750415",
            "prompt": "M&Ms Mixed Funsize 32.9z"
        },
        "131789": {
            "codes": "|040000497172|040000497172|0004000049717| 040000497172||040000497172|004000049717|~|03-72569|",
            "PID": "131789",
            "CM": null,
            "IS": "U",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": null,
                "pt": null
            },
            "PF": "5348",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": "U||~~~~~~~~~~~|01.2Z|04|Mars Inc.|49717|||U|No|PIC|~~||U|",
            "PB": "5830",
            "url": "",
            "WF": true,
            "WD": false,
            "Com": false,
            "NI": true,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "New Unconfirmed",
            "id": "676907870",
            "prompt": "Singles Goodness Knows Peachy Cherry 1.2z"
        },
        "131791": {
            "codes": "|040000497202|040000497202|0004000049720| 040000497202||040000497202|004000049720|~|03-72554|",
            "PID": "131791",
            "CM": null,
            "IS": "U",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": null,
                "pt": null
            },
            "PF": "5348",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": "U||~~~~~~~~~~~|01.2Z|04|Mars Inc.|49720|||U|No|PIC|~~||U|",
            "PB": "5830",
            "url": "",
            "WF": true,
            "WD": false,
            "Com": false,
            "NI": true,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "New Unconfirmed",
            "id": "676910679",
            "prompt": "Singles Goodness Knows Nutty Apple 1.2z"
        },
        "131793": {
            "codes": "|040000497233|040000497233|0004000049723| 040000497233||040000497233|004000049723|~|03-72534|",
            "PID": "131793",
            "CM": null,
            "IS": "U",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": null,
                "pt": null
            },
            "PF": "5348",
            "CL": "133",
            "CPT": "Candy - Gondola",
            "oth_info": "U||~~~~~~~~~~~|01.2Z|04|Mars Inc.|49723|||U|No|PIC|~~||U|",
            "PB": "5830",
            "url": "",
            "WF": true,
            "WD": false,
            "Com": false,
            "NI": true,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "New Unconfirmed",
            "id": "676046405",
            "prompt": "Singles Goodness Knows Very Cranberry 1.2z"
        }
    },
    "tasks": {
        "folder": [

        ]
    },
    "surveys": {
        "folder": {

        }
    },
    "promotions": {
        "folder": {

        }
    },
    "pt": "Candy - Gondola"
},
{
    "id": "7",
    "dept": "82 Impulse / Checklane",
    "ord": "3",
    "distribution_folder": {
        "6468": {
            "codes": "|040000004356|35391|0004000000435| 04043506||04043506|004000000435|~|96715|~|04043506|~|040000000235|~|4002305|~|1586064|~|4043506|~|4043506|~|4043506|~|4043506|~|04000000435|",
            "PID": "6468",
            "CM": "24",
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9760",
            "CL": "133",
            "CPT": "Candy - Checkstand",
            "oth_info": " |DV|~~~~~~~~~~~|02Z|36|Mars Inc.|35391||| |Yes|PIC|2016/02/04 12:26:18~DV~CF|| |",
            "PB": "454",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546588",
            "pt": "Singles Twix Caramel Cookie 1.79z"
        },
        "6469": {
            "codes": "|040000000310|01231|0004000000031| 04003100||04003100|004000000031|~|96460|~|04003100|~|1526052|~|4003100|~|04000000031|",
            "PID": "6469",
            "CM": "24",
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9760",
            "CL": "133",
            "CPT": "Candy - Checkstand",
            "oth_info": " |DV|~~~~~~~~~~~|01.69Z|48|Mars Inc.|01231||| |Yes|PIC|2016/02/04 12:26:09~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670546995",
            "pt": "Singles M&M Milk Choc 1.69z"
        },
        "6470": {
            "codes": "|040000000327|01232|0004000000032| 04003207||04003207|004000000032|~|96474|~|04003207|~|1526003|~|4003207|~|04000000032|",
            "PID": "6470",
            "CM": "24",
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9760",
            "CL": "133",
            "CPT": "Candy - Checkstand",
            "oth_info": " |DV|~~~~~~~~~~~|01.74Z|48|Mars Inc.|01232||| |Yes|PIC|2016/02/04 12:26:13~DV~CF|| |",
            "PB": "450",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547001",
            "pt": "Singles M&M Peanut 1.74z"
        },
        "6628": {
            "codes": "|040000006039|24603|0004000000603| 04060309||04060309|004000000603|~|96714|~|04060309|~|1528249|~|4060309|~|4060309|~|04000000603|",
            "PID": "6628",
            "CM": "24",
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9760",
            "CL": "133",
            "CPT": "Candy - Checkstand",
            "oth_info": " |DV|~~~~~~~~~~~|03.28Z|24|Mars Inc.|24603||| |Yes|PIC|2016/02/04 12:25:47~DV~CF|| |",
            "PB": "442",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547106",
            "pt": "Sharing Size 3 Musketeers 2 to Go 3.28z"
        },
        "6638": {
            "codes": "|040000004011|04401|0004000000401| 04040101||04040101|004000000401|~|96705|~|04040101|~|1527126|~|4040101|~|04000000401|",
            "PID": "6638",
            "CM": "24",
            "IS": " ",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9760",
            "CL": "133",
            "CPT": "Candy - Checkstand",
            "oth_info": " |DV|~~~~~~~~~~~|03.63Z|24|Mars Inc.|04401||| |Yes|PIC|2016/02/04 12:26:03~DV~CF|| |",
            "PB": "447",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": false,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "Auto",
            "id": "670547092",
            "pt": "Sharing Size Milky Way 2 to Go 3.63z"
        },
        "133157": {
            "codes": "|040000503156|040000503156|0004000050315| 040000503156||040000503156|004000050315|~|93738|",
            "PID": "133157",
            "CM": null,
            "IS": "N",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": "4",
                "pt": "Distribution Void (DV)"
            },
            "PF": "9766",
            "CL": "133",
            "CPT": "Candy - Checkstand",
            "oth_info": "N|DV|~~~~~~~~~~~|01.41Z|18|Mars Inc.|50315|||N|Yes|PIC|2016/02/04 12:24:22~DV~CF||N|",
            "PB": "452",
            "url": "",
            "WF": true,
            "WD": true,
            "Com": false,
            "NI": true,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "New Confirmed",
            "id": "676585937",
            "pt": "Singles Snickers Crisper 1.41z"
        }
    },
    "tasks": {
        "folder": [

        ]
    },
    "surveys": {
        "folder": {
            "63842": {
                "pt": "2016 Perfect Store IC Availability Survey",
                "id": "63842",
                "folder": [
                   {
                       "values": [
                          {
                              "id": "205467",
                              "pt": "Yes"
                          },
                          {
                              "id": "205468",
                              "pt": "No"
                          }
                       ],
                       "id": "168489",
                       "pt": "Is Snickers Original Singles in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:35)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168489",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205489",
                              "pt": "Yes"
                          },
                          {
                              "id": "205490",
                              "pt": "No"
                          }
                       ],
                       "id": "168500",
                       "pt": "Is Snickers Original Sharing Size in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:34)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168500",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205485",
                              "pt": "Yes"
                          },
                          {
                              "id": "205486",
                              "pt": "No"
                          }
                       ],
                       "id": "168498",
                       "pt": "Is M&M's Peanut Singles in stock and on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:33)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168498",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205471",
                              "pt": "Yes"
                          },
                          {
                              "id": "205472",
                              "pt": "No"
                          }
                       ],
                       "id": "168491",
                       "pt": "Is M&M's Peanut Sharing Size in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:31)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168491",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205465",
                              "pt": "Yes"
                          },
                          {
                              "id": "205466",
                              "pt": "No"
                          }
                       ],
                       "id": "168488",
                       "pt": "Is M&M Milk Singles in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:30)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168488",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205475",
                              "pt": "Yes"
                          },
                          {
                              "id": "205476",
                              "pt": "No"
                          }
                       ],
                       "id": "168493",
                       "pt": "Is M&M Milk Sharing Size in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:27)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168493",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205469",
                              "pt": "Yes"
                          },
                          {
                              "id": "205470",
                              "pt": "No"
                          }
                       ],
                       "id": "168490",
                       "pt": "Is 3 Musketeers Singles in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:26)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168490",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205479",
                              "pt": "Yes"
                          },
                          {
                              "id": "205480",
                              "pt": "No"
                          }
                       ],
                       "id": "168495",
                       "pt": "Is 3 Musketeers Sharing Size in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:24)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168495",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205473",
                              "pt": "Yes"
                          },
                          {
                              "id": "205474",
                              "pt": "No"
                          }
                       ],
                       "id": "168492",
                       "pt": "Is Snickers Bites Sharing Size in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:23)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168492",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205487",
                              "pt": "Yes"
                          },
                          {
                              "id": "205488",
                              "pt": "No"
                          }
                       ],
                       "id": "168499",
                       "pt": "Is Snickers Almond Singles in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:21)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168499",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205481",
                              "pt": "Yes"
                          },
                          {
                              "id": "205482",
                              "pt": "No"
                          }
                       ],
                       "id": "168496",
                       "pt": "Is Milky Way Sharing Size in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:20)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168496",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205491",
                              "pt": "Yes"
                          },
                          {
                              "id": "205492",
                              "pt": "No"
                          }
                       ],
                       "id": "168501",
                       "pt": "Is Milky Way Singles in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:19)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168501",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205477",
                              "pt": "Yes"
                          },
                          {
                              "id": "205478",
                              "pt": "No"
                          }
                       ],
                       "id": "168494",
                       "pt": "Is Twix Sharing Size in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:16)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168494",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205483",
                              "pt": "Yes"
                          },
                          {
                              "id": "205484",
                              "pt": "No"
                          }
                       ],
                       "id": "168497",
                       "pt": "Is Twix Original Singles in stock and on shelf on every standard or regular checkout?",
                       "answer": null,
                       "group": "63842",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:15)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168497",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": null,
                "iscon": false,
                "atch": "",
                "URL": ""
            }
        }
    },
    "promotions": {
        "folder": {

        }
    },
    "pt": "Candy - Checkstand"
},
{
    "id": "203",
    "dept": "Grocery-Edible",
    "ord": "9",
    "distribution_folder": {
        "131788": {
            "codes": "|040000497165|10040000497155|0004000049716| 040000497165||040000497165|004000049716|~|72569|",
            "PID": "131788",
            "CM": null,
            "IS": "U",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": null,
                "pt": null
            },
            "PF": "5329",
            "CL": "133",
            "CPT": "Snacks - Granola Bars",
            "oth_info": "U||~~~~~~~~~~~|12/01.2Z|04|Mars Inc.|49716|||U|No|PIC|~~||U|",
            "PB": "5830",
            "url": "",
            "WF": true,
            "WD": false,
            "Com": false,
            "NI": true,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "New Unconfirmed",
            "id": "676050438",
            "pt": "Goodness Knows Peachy Cherry 1.2z 12ct"
        },
        "131790": {
            "codes": "|040000497196|10040000497186|0004000049719| 040000497196||040000497196|004000049719|~|72554|",
            "PID": "131790",
            "CM": null,
            "IS": "U",
            "wf_r": "",
            "OS": {
                "is_positive": false,
                "id": null,
                "pt": null
            },
            "PF": "5329",
            "CL": "133",
            "CPT": "Snacks - Granola Bars",
            "oth_info": "U||~~~~~~~~~~~|12/01.2Z|04|Mars Inc.|49719|||U|No|PIC|~~||U|",
            "PB": "5830",
            "url": "",
            "WF": true,
            "WD": false,
            "Com": false,
            "NI": true,
            "AI": true,
            "ZS": false,
            "SC": false,
            "bad": true,
            "AT": true,
            "WF2": false,
            "invres": null,
            "aux": {
                "Fields": {

                }
            },
            "wf_sr": "New Unconfirmed",
            "id": "676051010",
            "pt": "Goodness Knows Nutty Apple 1.2z 12ct"
        }
    },
    "tasks": {
        "folder": [

        ]
    },
    "surveys": {
        "folder": {

        }
    },
    "promotions": {
        "folder": {

        }
    },
    "pt": "Snacks - Granola Bars"
},
{
    "id": "285",
    "dept": "Displays",
    "ord": "2",
    "distribution_folder": {

    },
    "tasks": {
        "folder": [

        ]
    },
    "surveys": {
        "folder": {
            "63808": {
                "pt": "2016 Perfect Store Secondary Display Survey",
                "id": "63808",
                "folder": [
                   {
                       "values": [
                          {
                              "id": "205437",
                              "pt": "Yes"
                          },
                          {
                              "id": "205438",
                              "pt": "No"
                          }
                       ],
                       "id": "168428",
                       "pt": "Is there at least one Entrance Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:48)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168428",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205449",
                              "pt": "Yes"
                          },
                          {
                              "id": "205450",
                              "pt": "No"
                          }
                       ],
                       "id": "168433",
                       "pt": "Is there at least one Front End Alley Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:50)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168433",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205439",
                              "pt": "Yes"
                          },
                          {
                              "id": "205440",
                              "pt": "No"
                          }
                       ],
                       "id": "168429",
                       "pt": "Is there at least one Dairy area Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:51)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168429",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205453",
                              "pt": "Yes"
                          },
                          {
                              "id": "205454",
                              "pt": "No"
                          }
                       ],
                       "id": "168435",
                       "pt": "Is there at least one Chocolate End Cap Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:53)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168435",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205447",
                              "pt": "Yes"
                          },
                          {
                              "id": "205448",
                              "pt": "No"
                          }
                       ],
                       "id": "168432",
                       "pt": "Is there at least one Salty Snacks Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:55)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168432",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205444",
                              "pt": "Yes"
                          },
                          {
                              "id": "205445",
                              "pt": "No"
                          },
                          {
                              "id": "205446",
                              "pt": "Store does not have Fresh Department"
                          }
                       ],
                       "id": "168431",
                       "pt": "Is there at least one Fresh Meal Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:59)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168431",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205451",
                              "pt": "Yes"
                          },
                          {
                              "id": "205452",
                              "pt": "No"
                          }
                       ],
                       "id": "168434",
                       "pt": "Is there at least one Soda Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:09)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168434",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205435",
                              "pt": "Yes"
                          },
                          {
                              "id": "205436",
                              "pt": "No"
                          }
                       ],
                       "id": "168427",
                       "pt": "Is there at least one Ice Cream Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:56:07)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168427",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205441",
                              "pt": "Yes"
                          },
                          {
                              "id": "205442",
                              "pt": "No"
                          },
                          {
                              "id": "205443",
                              "pt": "Store does not have Floral Department"
                          }
                       ],
                       "id": "168430",
                       "pt": "Is there at least one Floral Secondary Location Rack Display in correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "Store does not have Floral Department (02/04/2016 11:56:05)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168430",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "0",
                              "pt": "Yes"
                          },
                          {
                              "id": "1",
                              "pt": "No"
                          },
                          {
                              "id": "2",
                              "pt": "NA"
                          }
                       ],
                       "id": "174847",
                       "pt": "Is there at least one Goodnessknows Produce Display in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63808",
                       "type": "PICK",
                       "qtype": "YNNA",
                       "last_ans": "N/A (02/04/2016 11:56:03)",
                       "act": "1",
                       "sbc": "0",
                       "sbs": "<null>",
                       "condition": null,
                       "gq": "202585",
                       "iscon": false,
                       "ASMID": "ASM_174847",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": null,
                "iscon": false,
                "atch": "",
                "URL": ""
            },
            "63896": {
                "pt": "2016 Display Piece Tracking Survey",
                "id": "63896",
                "folder": [
                   {
                       "values": [
                          {
                              "id": "205602",
                              "pt": "Yes"
                          },
                          {
                              "id": "205603",
                              "pt": "No"
                          }
                       ],
                       "id": "168691",
                       "pt": "Is the (Option B) Entrance Basket/store Circular Display in correct location, adhering to PS standard?",
                       "answer": null,
                       "group": "63896",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:47)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168691",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": null,
                "iscon": false,
                "atch": "",
                "URL": ""
            }
        }
    },
    "promotions": {
        "folder": {

        }
    },
    "pt": "Display"
},
{
    "id": "149",
    "dept": "Seasonal",
    "ord": "1",
    "distribution_folder": {

    },
    "tasks": {
        "folder": [

        ]
    },
    "surveys": {
        "folder": {
            "65890": {
                "pt": "2016 Easter Displays Survey",
                "id": "65890",
                "folder": [
                   {
                       "min": "0",
                       "max": "999",
                       "decimal": false,
                       "after_dec": "0",
                       "id": "174166",
                       "pt": "How many Easter cases did you work out of the backroom",
                       "answer": null,
                       "group": "65890",
                       "type": "NUMBER",
                       "qtype": "Numeric",
                       "last_ans": "0 (02/04/2016 11:54:52)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_174166",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "min": "0",
                       "max": "999",
                       "decimal": false,
                       "after_dec": "0",
                       "id": "174170",
                       "pt": "How many Easter cases are remaining in the backroom at the end of this call?",
                       "answer": null,
                       "group": "65890",
                       "type": "NUMBER",
                       "qtype": "Numeric",
                       "last_ans": "0 (02/04/2016 11:55:08)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_174170",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "min": "0",
                       "max": "999",
                       "decimal": false,
                       "after_dec": "0",
                       "id": "174168",
                       "pt": "How many Easter basket toppers did you place on this store call?",
                       "answer": null,
                       "group": "65890",
                       "type": "NUMBER",
                       "qtype": "Numeric",
                       "last_ans": "0 (02/04/2016 11:55:10)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_174168",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "min": "0",
                       "max": "999",
                       "decimal": false,
                       "after_dec": "0",
                       "id": "174167",
                       "pt": "how many easter pallet displays did you place on this store call?",
                       "answer": null,
                       "group": "65890",
                       "type": "NUMBER",
                       "qtype": "Numeric",
                       "last_ans": "0 (02/04/2016 11:55:12)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_174167",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "211648",
                              "pt": "No issues"
                          },
                          {
                              "id": "211649",
                              "pt": "Yes- Damaged"
                          },
                          {
                              "id": "211650",
                              "pt": "Yes- Discarded by store"
                          },
                          {
                              "id": "211651",
                              "pt": "Yes- Missing or lost"
                          },
                          {
                              "id": "211652",
                              "pt": "Yes- Manager Refused"
                          },
                          {
                              "id": "211653",
                              "pt": "This store is not receiving a focal point"
                          }
                       ],
                       "id": "174169",
                       "pt": "If applicable, were there any issues with the easter focal points?",
                       "answer": null,
                       "group": "65890",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "This store is not receiving a focal point (02/04/2016 11:55:15)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_174169",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": null,
                "iscon": false,
                "atch": "",
                "URL": ""
            }
        }
    },
    "promotions": {
        "folder": {

        }
    },
    "pt": "Seasonal"
},
{
    "id": "39",
    "dept": "Displays",
    "ord": "9995",
    "distribution_folder": {

    },
    "tasks": {
        "folder": [

        ]
    },
    "surveys": {
        "folder": {
            "62157": {
                "pt": "Supervisor Ride with Evaluation Survey",
                "id": "62157",
                "folder": [
                   {
                       "values": [
                          {
                              "id": "199970",
                              "pt": "Announced"
                          },
                          {
                              "id": "199971",
                              "pt": "Unannounced"
                          }
                       ],
                       "id": "164104",
                       "pt": "Was this an announced or unannounced visit? ",
                       "answer": null,
                       "group": "62157",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_164104",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "199966",
                              "pt": "New Hire"
                          },
                          {
                              "id": "199967",
                              "pt": "30/60/90 day review"
                          },
                          {
                              "id": "199968",
                              "pt": "Normal Development Day"
                          },
                          {
                              "id": "199969",
                              "pt": "Seasonal/Event Support"
                          }
                       ],
                       "id": "164103",
                       "pt": "What was the purpose of this call? ",
                       "answer": null,
                       "group": "62157",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_164103",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "199976",
                              "pt": "Yes"
                          },
                          {
                              "id": "199977",
                              "pt": "No"
                          }
                       ],
                       "id": "164107",
                       "pt": "Was the Rep prepared for this call? ",
                       "answer": null,
                       "group": "62157",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_164107",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "199978",
                              "pt": "Yes"
                          },
                          {
                              "id": "199979",
                              "pt": "No"
                          }
                       ],
                       "id": "164108",
                       "pt": "Did the Rep follow the store call process? ",
                       "answer": null,
                       "group": "62157",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_164108",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "199982",
                              "pt": "Yes"
                          },
                          {
                              "id": "199983",
                              "pt": "No"
                          }
                       ],
                       "id": "164110",
                       "pt": "Did the manager/GM know the rep? ",
                       "answer": null,
                       "group": "62157",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_164110",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "199980",
                              "pt": "Yes"
                          },
                          {
                              "id": "199981",
                              "pt": "No"
                          }
                       ],
                       "id": "164109",
                       "pt": "Did the Rep leave a suggested order with KDM? ",
                       "answer": null,
                       "group": "62157",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_164109",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "199972",
                              "pt": "Yes"
                          },
                          {
                              "id": "199973",
                              "pt": "No"
                          }
                       ],
                       "id": "164105",
                       "pt": "Did the Rep show understanding of Perfect Store? ",
                       "answer": null,
                       "group": "62157",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_164105",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "199974",
                              "pt": "Yes"
                          },
                          {
                              "id": "199975",
                              "pt": "No"
                          }
                       ],
                       "id": "164106",
                       "pt": "Was the Rep prepared and attempted to sell in display on this call?",
                       "answer": null,
                       "group": "62157",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_164106",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": "748",
                "iscon": false,
                "atch": "",
                "URL": ""
            },
            "63904": {
                "pt": "2016 IC Activation Survey  ",
                "id": "63904",
                "folder": [
                   {
                       "min": "0",
                       "max": "50",
                       "decimal": false,
                       "after_dec": "0",
                       "id": "168716",
                       "pt": "How many total active registers are in this store?",
                       "answer": null,
                       "group": "63904",
                       "type": "NUMBER",
                       "qtype": "Numeric",
                       "last_ans": "2 (02/04/2016 12:29:36)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168716",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "min": "0",
                       "max": "50",
                       "decimal": false,
                       "after_dec": "0",
                       "id": "168717",
                       "pt": "How many active registers are currently merchandised with Mars with IC Chocolate?",
                       "answer": null,
                       "group": "63904",
                       "type": "NUMBER",
                       "qtype": "Numeric",
                       "last_ans": "1 (02/04/2016 12:29:40)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168717",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": null,
                "iscon": false,
                "atch": "",
                "URL": ""
            },
            "63938": {
                "pt": "2016 POS Material Perfect Store Survey",
                "id": "63938",
                "folder": [
                   {
                       "values": [
                          {
                              "id": "205637",
                              "pt": "Yes"
                          },
                          {
                              "id": "205638",
                              "pt": "No"
                          }
                       ],
                       "id": "168728",
                       "pt": "Are the M&M Shelf organizers installed on shelf?",
                       "answer": null,
                       "group": "63938",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:40)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168728",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205635",
                              "pt": "Yes"
                          },
                          {
                              "id": "205636",
                              "pt": "No"
                          }
                       ],
                       "id": "168727",
                       "pt": "Are the M&M shelf strips installed on shelf?",
                       "answer": null,
                       "group": "63938",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:39)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168727",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205631",
                              "pt": "Yes"
                          },
                          {
                              "id": "205632",
                              "pt": "No"
                          }
                       ],
                       "id": "168725",
                       "pt": "Are the Dove Strips on shelf in the correct location?",
                       "answer": null,
                       "group": "63938",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:38)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168725",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205633",
                              "pt": "Yes"
                          },
                          {
                              "id": "205634",
                              "pt": "No"
                          }
                       ],
                       "id": "168726",
                       "pt": "Are the Snickers Strips on shelf in the correct location?",
                       "answer": null,
                       "group": "63938",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:36)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168726",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": null,
                "iscon": false,
                "atch": "",
                "URL": ""
            },
            "63955": {
                "pt": "2016 Uscan Survey",
                "id": "63955",
                "folder": [
                   {
                       "values": [
                          {
                              "id": "205643",
                              "pt": "Yes"
                          },
                          {
                              "id": "205644",
                              "pt": "No"
                          }
                       ],
                       "id": "168731",
                       "pt": "Does the store have Uscans?",
                       "answer": null,
                       "group": "63955",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "No (02/04/2016 11:55:34)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168731",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205645",
                              "pt": "Yes"
                          },
                          {
                              "id": "205646",
                              "pt": "No"
                          },
                          {
                              "id": "209782",
                              "pt": "NA-No Uscan in Store"
                          }
                       ],
                       "id": "168732",
                       "pt": "If yes, is it covered with Mars IC Chocolate?",
                       "answer": null,
                       "group": "63955",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "NA-No Uscan in Store (02/04/2016 11:55:32)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168732",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205641",
                              "pt": "Yes"
                          },
                          {
                              "id": "205642",
                              "pt": "No"
                          },
                          {
                              "id": "209783",
                              "pt": "NA-No Uscan in Store"
                          }
                       ],
                       "id": "168730",
                       "pt": "Is there at least one Option 1, 2, or 3 display at Self Checkout in the correct location adhering to PS standard?",
                       "answer": null,
                       "group": "63955",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "NA-No Uscan in Store (02/04/2016 11:55:29)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168730",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   },
                   {
                       "values": [
                          {
                              "id": "205639",
                              "pt": "Yes"
                          },
                          {
                              "id": "205640",
                              "pt": "No"
                          },
                          {
                              "id": "209784",
                              "pt": "NA-No Uscan in Store"
                          }
                       ],
                       "id": "168729",
                       "pt": "Is there at least one Entrance Display at Self Check Out in the correct location adhering to perfect store standard?",
                       "answer": null,
                       "group": "63955",
                       "type": "PICK",
                       "qtype": "Pick List",
                       "last_ans": "NA-No Uscan in Store (02/04/2016 11:55:26)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_168729",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": null,
                "iscon": false,
                "atch": "",
                "URL": ""
            },
            "65744": {
                "pt": "2016 KDM Survey",
                "id": "65744",
                "folder": [
                   {
                       "id": "173750",
                       "pt": "What was the name of the KDM that you spoke with on this call",
                       "answer": null,
                       "group": "65744",
                       "type": "TEXT",
                       "qtype": "Text",
                       "last_ans": "Sabers sm (02/04/2016 12:20:01)",
                       "act": "1",
                       "sbc": null,
                       "sbs": null,
                       "condition": null,
                       "gq": null,
                       "iscon": false,
                       "ASMID": "ASM_173750",
                       "att1": "<null>",
                       "att2": "<null>",
                       "att3": "<null>",
                       "att4": "<null>",
                       "att5": "<null>"
                   }
                ],
                "cm": "24",
                "ct": null,
                "iscon": false,
                "atch": "",
                "URL": ""
            }
        }
    },
    "promotions": {
        "folder": {

        }
    },
    "pt": "Miscellaneous"
}
            ],
            /*END Lost Sales Data*/


        }
        data.STORENUMBER = "2081";//set the key to storenumber 2081 which matches the PWS_Opp_BusinessReview2 match value.
        data.PWS_Movement_Radio = [{
            "Value": "(WeekNum>40 && WeekNum<53)",
            "Display": "Last 12 weeks"
        }, {"Value": "(WeekNum>0 && WeekNum<13)", "Display": "LY Perf 12 weeks out"}];
        append_pitch_static_data(data); //call to/from the ASMCustomData.js
        display_presentation_data(data);
        setup_presentation();
    }

    if (!testing)
        include('corescripts/SFPresentations.js');


    function display_presentation_data(data_hash_table) {
        data_hash_table['UBF_MMCalc_Selected'] = [{Value: '', Display: 'All'}, {
            Value: 'Selected',
            Display: 'Selected Only'
        }];
        crm_allow_save_data = data_hash_table['crm_allow_save_data'];

        crm_current_call_key = data_hash_table['crm_current_call_key'];
        crm_current_presentation_key = data_hash_table['crm_current_presentation_key'];
        check_storage_clean_up(crm_current_call_key, crm_current_presentation_key);
        if (typeof DynamicPitch_SetStringAndWrite == 'function') {
            //alert($table[0].id);
            pitch_data = DynamicPitch_SetStringAndWrite(data_hash_table);
        }
        else {
            pitch_data = data_hash_table;
        }
        ;
        core_setup_presentation();
        populate_ui_data($('body'));
        loadPitchDataFromLocalStorage();
    }

    function get_filter_key($filter_element) {
        if ($filter_element.data('filter_key') == null)
            $filter_element.data('filter_key', new_guid());
        return $filter_element.data('filter_key');
    }

    function display_presentation_data_error(message) {
        alert(message);
    }

    function setup_presentation(presentationID) {
        current_presentation_id = presentationID;
        var $end = $('.EndPresentation');
        $end.click(function () {
            var $video = $('video');
            if ($video.length > 0) {
                $video.get(0).pause();
            }


            core.record_breadcrumb('Presentation Ended', function () {
                SFPresentationAPI.end_presentation(presentationID, function () {
                    console.log('presentation ended');
                }, function (message) {
                    alert('Presentation end error: ' + message);
                });
            });
        });
        var $video = $('video');
        $('video').bind('play', function () {
            core.record_breadcrumb('Play Video: ' + $(this).get(0).currentSrc);
        });
        $('video').bind('pause', function () {
            core.record_breadcrumb('Pause Video: ' + $(this).get(0).currentSrc);
        });
        $('video').bind('ended', function () {
            core.record_breadcrumb('End Video: ' + $(this).get(0).currentSrc);
        });
    }

    function wait_for_presentations() {
        if ('SFPresentationAPI' in window) {
            console.log('setting up sfpresentation api');
            SFPresentationAPI.ready(function (presentationID) {
                SFTouch.log('presentation API ready');
                var data_success = function (data) {
                    append_pitch_static_data(data); //call to/from the ASMCustomData.js
                    current_presentation_id = presentationID;
                    display_presentation_data(data);
                    setup_presentation(presentationID);
                }
                SFPresentationAPI.get_presentation_data(presentationID, data_success, display_presentation_data_error);
            });
        }
        else {
            console.log('waiting for SFPresentationAPI');
            setTimeout(wait_for_presentations, 1000);
        }
    }

    if (testing)
        $(testingStartup);
    else
        wait_for_presentations();
})();