﻿// Run functions when a slide loads
function runSlideFunctions($target) {
    var Tid = $target.attr('id'),
        targetTable, tblCalcFunction;
    $("#" + Tid + " " + 'table[tablecalcfunction]').each(function (index, el) {
        targetTable = $(this);
        tblCalcFunction = window[targetTable.attr('tablecalcfunction')];
    });

    if (tblCalcFunction !== undefined) {
        setTimeout(function () {
            tblCalcFunction(targetTable);
        }, 500);
    }
    ////Run CustomPhoto Function that loads photos osaLostCalculator
    if (document.getElementById($target.attr('id')).hasAttribute("loadItems")) {
        BuildHTMLPDFTable();
    }

    if (document.getElementById($target.attr('id')).hasAttribute("osaLostCalculator")) {
        console.log($("#" + Tid + " " + 'table[calculateLostSales]').attr('id'));
        CumulativeAddRecalculateTable($("#" + Tid + " " + 'table[calculateLostSales]').attr('id'));
    }


    if (document.getElementById($target.attr('id')).hasAttribute("loadWMLAGValues")) {
        loadWMLAGValues($target.attr('id'));
    }

}


//For PDF Button
function create_pdf() {

    //console.log("creating pdf");
    //remove the pitch border background from the pdf
    $("html").css({ 'background-color': 'white' });

    //hide the PDF button 
    $('.creatPDF').css({ 'display': 'none' });

    //generate the pdf
    ASMPDFHelper.pitch_pdf($('div.current'));

    //temp fix to put back the border around the pitch after pdf generates
    window.setTimeout(function () {
        $("html").css({ 'background-color': ' #F2F2F2' });
        $('.creatPDF').css({ 'display': 'block' });

    }, 1000);
    //$('.creatPDF').css({ 'display': 'block' });
}


function BuildHTMLPDFTable() {
    console.log('BuildHTMLPDFTable');

    var data = buildPDFData();

    //Get the inner divs of the html template 
    var HTMLTemplate = $("#containerPDF").children();
    //Clear the table
    //Using text('') or html('') will cause some string parsing to take place, 
    //which generally is a bad idea when working with the DOM. Try and use DOM 
    //manipulation methods that do not involve string representations of DOM objects wherever possible.
    $('#containerPDF div').empty();

    if (data.length <= 20) {
        for (var i = 0; i < data.length; i++) {
            for (var x = 0; x < data.length; x++) {
                HTMLTemplate[x].innerHTML =
                    "<div class='imageWrapper'>" +
                    "<div id='cellProductImage" + x + "'>" +
                    "</div>" +
                    "</div>" +
                    "<div class='cellProductName'>" + data[x].product + "</div>" +
                    "<div class='cellProductQTY'>" + data[x].qty + "</div>" +
                    "<img class='cellBarcode" + x + "'>"
                makePDfBarCode(x, data[x].UPC);
                buildPDFImage(data[x].UPC, x);
            }
        }
    }
    data = [];
}

function buildPDFData() {
    var targtTable = $("[buildPDFData='true']"), tableId = targtTable.prop('id'), selectedProducts = [],

        TableRows = document.getElementById(tableId).getElementsByTagName('tbody')[0].rows.length,
        thisRow = 0;

    while (thisRow < TableRows) {
        var elmnt = document.getElementById(tableId).getElementsByTagName('tbody')[0].children[thisRow];

        if (!$(elmnt).hasClass('hidden') && $(elmnt).find('input:checkbox').is(':checked')) {

            // Adjust accordingly/tailor fit to what you want to show on your PDF export slide.
            var myTDs = document.getElementById(tableId).getElementsByTagName('tbody')[0].children[thisRow].children,
                productName = myTDs[1].childNodes[0].data,
                $upc = myTDs[4].childNodes[0].data,
                suggOrderQty = ((myTDs[5].childNodes[0].value == "") ? "N/A" : stripNonRealDigits(myTDs[5].childNodes[0].value));

            selectedProducts.push({
                "product": productName,
                "UPC": $upc,
                "qty": suggOrderQty
            });
        }
        thisRow++;
    }
    //console.log(selectedProducts);
    return selectedProducts;
}

function makePDfBarCode(index, upc) {
    // UPCA.ShowBarcode($('.cellBarcode' + index), upc);
    $('.cellBarcode' + index).JsBarcode(upc, {
        //format: "upc",
        //width: 1,
        //height: 55,
        //background: "transparent"
        format: "EAN13",
        width: 1.2,
        height: 80,
        fontSize: 13
    });
}

function buildPDFImage(upc, index) {
    var tmpImg = new Image();
    tmpImg.src = "./images/Products/" + upc + ".png";

    $(tmpImg).one('load', function () {

        orgWidth = tmpImg.width;
        orgHeight = tmpImg.height;
        //landscape
        //if (orgWidth >= 219) {
        if (orgHeight < orgWidth) {
            tmpImg.height = 50;
            tmpImg.style.marginTop = "43px";
            tmpImg.style.width = "107px";
        }
        else {
            //if (orgHeight >= 219) {
            //Portrait
            if (orgHeight > orgWidth) {
                // tmpImg.width = 80;
                tmpImg.height = 110;
            }
            else {
                console.log("no image found");
            }
        }
        $('#cellProductImage' + index).html(tmpImg);
    });
}

//function BuildHTMLPDFTable2(selectedProducts) {

//    //Data Object
//    var data = selectedProducts;

//    //Get the inner divs of the html template 
//    var HTMLTemplate = $("#containerKGB").children();

//    //Clear the table
//    //Using text('') or html('') will cause some string parsing to take place, which generally is a bad idea when working with the DOM. Try and use DOM manipulation methods that do not involve string representations of DOM objects wherever possible.
//    $('#containerKGB div').empty();


//    //loop over data and build each template sqaure on the front end
//    if (data.length <= 12) {
//        for (var i = 0; i < data.length; i++) {
//            for (var x = 0; x < data.length; x++) {
//                HTMLTemplate[x].innerHTML =
//                   "<div class='imageWrapper'>" +
//                            "<div id='cellProductImage" + x + "'>" +
//                            "</div>" +
//                   "</div>" +
//                   "<div class='cellProductName'>" + data[x].product + "</div>" +
//                   "<div class='cellProductQTY'>" + "Qty: " + data[x].qty + "</div>" +
//                   "<img class='cellBarcode" + x + "'>"
//                makeBarCode(x, data[x].UPC);
//                buildPDFImage(data[x].UPC, x);
//            }
//            //console.log(HTMLTemplate[0]);
//        }
//    }
//}


////Helper to put images on the pdf summary
//function buildPDFImage(upc, index) {

//    var tmpImg = new Image();
//    tmpImg.src = "./images/Products/" + upc + ".png";


//    $(tmpImg).one('load', function () {

//        orgWidth = tmpImg.width;
//        orgHeight = tmpImg.height;

//        //landscape
//        //if (orgWidth >= 219) {
//        if (orgHeight < orgWidth) {
//            tmpImg.height = 50;
//            tmpImg.style.marginTop = "43px";
//            tmpImg.style.width = "107px";
//        }
//        else {
//            //if (orgHeight >= 219) {
//            //Portrait
//            if (orgHeight > orgWidth) {
//                // tmpImg.width = 80;
//                tmpImg.height = 110;

//            }
//            else {

//                console.log("no image found");
//            }
//        }

//        $('#cellProductImage' + index).html(tmpImg);
//    });
//}

function loadCustomPhotoImages(slideId) {
    if (slideId == "#displayprofit_CustomPhoto1") {
        loadSlidePhotos(slideId);
    }
}


function stripNonRealDigits(numberSource) {
    var m_strOut = new String(numberSource);
    m_strOut = m_strOut.replace(/[^\d.]/g, '');
    return m_strOut;
}

function ClearAll(container_name, recalculate_table) {
    // console.log("clearALl", container_name);

    if (container_name == null)
        container_name = $(event.srcElement).parent().attr('id');

    var $container;
    $container = $('#' + container_name);

    //Gets all the highlighted rows in the table.
    var tableCompleteRows = $container.children().children().children().children();

    var $multiSelects = $container.find('select[multiselect=true][storagekeyprefix]');
    $multiSelects.multiselect("uncheckAll");
    $multiSelects.multiselect("close");
    var $Selects = $container.find('select[multiselect=false][storagekeyprefix]');
    $Selects.val('');
    $Selects.change();

    var $elementsWithStoragesKeys = $container.find('*[storagekeyprefix]')
    $elementsWithStoragesKeys.each(function (index) {
        var $item = $(this);
        if ($item.is('[type=checkbox]')) {
            $item.attr('checked', false);
            //remove highlighted row
            tableCompleteRows.removeClass('complete');
            $item.change();
        } else if ($item.hasClass('checkboximage')) {
            if ($item.hasClass('checked'))
                $item.click();
        } else if ($item.is('[type=textbox]')) {
            $item.val('');
            $item.change();

        } else if ($item.is('[type=tel]')) {
            $item.val('');
            $item.change();
        }

        /*Custom Code for Suggested Order
      *
      *   add classes for input tags in HTML
      *   i.e. suggOrder1 & suggOrder
      *   
      */
        if ($item.hasClass('suggOrder')) {
            var $itemUPC = $item.parent().parent()[0].children[4].innerText; 

            if ($item.is('[type=checkbox]')) {
                var myIndex = stripNonRealDigits($item[0].className);
                clearSuggOrderJSONs(myIndex, recalculate_table, $itemUPC);
                $item.attr('checked', false);

            } else if ($item.is('[type=tel]')) {
                var myIndex = stripNonRealDigits($item[0].className);
                clearSuggOrderJSONs(myIndex, recalculate_table, $itemUPC);
                $item.attr('value', " ");
            }
        }
    });

    //change this by calculator
    if (recalculate_table.substring(0, 6) == 'DEMO_PC') {
        BigBetsRecalculateTable(recalculate_table);
    }
    //change this by calculator
    //alert(recalculate_table.substring(3,10));
    if (recalculate_table.substring(3, 10) == 'DEMO_DC') {
        //alert(recalculate_table);
        DEMO_DP_RecalculateTable(recalculate_table);
    }
    //change this by calculator
    //alert(recalculate_table.substring(0,7));
    if (recalculate_table.substring(0, 7) == 'SCJ_OSA') {
        CumulativeAddRecalculateTable(recalculate_table);
    }

    if (recalculate_table == 'tblSugOrd_All1') {
        selectedProducts = [];
    }
}

// Suggested Order FX
function clearSuggOrderJSONs(myIndex, tableId, $itemUPC) {
    var key = tableId + "_" + $itemUPC;
    var myData = JSON.parse(localStorage.getItem(key));

    if (myIndex == 1) {
        myData[0][myIndex] = false;
    } else {
        myData[0][myIndex] = " ";
    }
    save_storage(key, JSON.stringify(myData));
}

$(window).ready(function () {

    $("button.upload_picture").click(function (event) {
        uploadPhoto(event);
    });

    $('body select[filterTarget]').bind('custom_multi_select', function () {
        //when any of those select values are changed, it will run this function
        //the selected values are returned within the arrSelected array.
        //to convert to string,  use the $.map function.
        var arrSelected = $(this).multiselect("getChecked");
        var arrString = $.map(arrSelected, function (item) {
            return item.value;
        });
        var filterDiv = $(this).closest("div").attr("id");
        if (filterDiv.substring(0, 6) == 'DEMO_PC') {
            var tblDiv = 'tbl' + filterDiv.replace("_filters", "");
            BigBetsRecalculateTable(tblDiv);
        }
        if (filterDiv.substring(0, 7) == 'SCJ_OSA') {
            var tblDiv = 'tbl' + filterDiv.replace("_filters", "");
            CumulativeAddRecalculateTable(tblDiv);
        }
    })

})

function CumulativeAdd(element) {
    console.log('CumulativeAdd');
    var $element = $(element);
    //use the follow items to changet check state,
    //also save the updated check states to storage.
    $element.toggleClass('checked');
    save_storage($element.attr('storagekeyprefix'), $element.hasClass('checked'));

    var TableName = element.parentNode.parentNode.parentNode.parentNode.id
    CumulativeAddRecalculateTable(TableName);
}

function BigBetsfindit(element, ColumnToShow, ColumnToChange) {
    console.log('BigBetsfindit');
    var $element = $(element);
    //use the follow items to changet check state,
    //also save the updated check states to storage.
    $element.toggleClass('checked');
    save_storage($element.attr('storagekeyprefix'), $element.hasClass('checked'));

    var CBChecked = $element.hasClass('checked');

    if (CBChecked == 1) {
        var $otherCheckbox = $(element.parentNode.parentNode.cells[ColumnToChange].childNodes[0]);
        if ($otherCheckbox.hasClass('checked')) {
            $otherCheckbox.removeClass('checked');
            save_storage($otherCheckbox.attr('storagekeyprefix'), $otherCheckbox.hasClass('checked'));
        }
    }
    var TableName = element.parentNode.parentNode.parentNode.parentNode.id
    BigBetsRecalculateTable(TableName);
}

function BigBetsRecalculateTable(TableDivId) {
    console.log('BigBetsRecalculateTable');
    var AddTarget = 8; //Cell that holds add checkbox hardcoded
    var SubtractTarget = 7; //Cell that holds subtract checkbox hardcoded
    var ValueTarget = 6; //Cell that contains the value


    var AddValueTarget = TableDivId + '_AddValue';
    var SubtractValueTarget = TableDivId + '_SubtractValue';
    var ImpactValueTarget = TableDivId + '_ImpactValue';
    var NetValueTarget = TableDivId + '_NetValue';

    var AddValue = 0;
    var SubtractValue = 0;
    var ImpactValue = 0;
    var NetValue = 0;

    //alert(TableDivId);
    //alert (document.getElementById(TableDivId).innerHTML);
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;
    //alert(TableRows);

    var thisRow = 0;

    while (thisRow < TableRows) {
        //alert(thisRow);
        var CaseRowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].getElementsByTagName('tr')[thisRow];
        //alert(CaseRowElement.className)
        if (!$(CaseRowElement).hasClass('hidden')) {
            if (document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[ValueTarget] != null) {
                var rowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow]
                var targetValue = parseFloat(stripNonRealDigits(rowElement.children[ValueTarget].innerHTML))
                var targetValueRounded = targetValue.toFixed(2)
                //alert (stripNonRealDigits(rowElement.children[ValueTarget].innerHTML))
                if ($(rowElement.children[AddTarget].childNodes[0]).hasClass('checked')) {
                    //alert ('add it');
                    AddValue = AddValue + targetValue;
                    //alert(AddValue);
                }
                if ($(rowElement.children[SubtractTarget].childNodes[0]).hasClass('checked')) {
                    //alert ('subtract it');
                    SubtractValue = SubtractValue + targetValue;
                    //alert(SubtractValue);
                }
            }
        }
        thisRow++;
    }
    ImpactValue = AddValue - SubtractValue;
    //alert(ImpactValue);
    if (SubtractValue != 0) {
        NetValue = ImpactValue / SubtractValue * 100;
    }
    else {
        NetValue = 0
    }
    //alert(NetValue);

    //Now to format these
    var AddValueFormatted = '$' + Math.round(AddValue);
    //alert(AddValueFormatted);
    var SubtractValueFormatted = '$' + Math.round(SubtractValue);
    //alert(SubtractValueFormatted);
    var ImpactValueFormatted = '$' + Math.round(ImpactValue);
    //alert(ImpactValueFormatted);
    var NetValueFormatted = Math.round(NetValue) + '%';
    //alert(NetValueFormatted);

    var IndicatorArrow = '';
    if (NetValue != 0) {
        if (NetValue < 0) {
            IndicatorArrow = '<img src="images/red_down.png">';
        }
        else {
            IndicatorArrow = '<img src="images/green_up.png">';
        }
    }

    // Now output them to the Divs
    document.getElementById(AddValueTarget).innerHTML = AddValueFormatted;
    document.getElementById(SubtractValueTarget).innerHTML = SubtractValueFormatted;
    document.getElementById(ImpactValueTarget).innerHTML = ImpactValueFormatted;
    document.getElementById(NetValueTarget).innerHTML = NetValueFormatted + IndicatorArrow;
}


function stripNonRealDigits(numberSource) {
    var m_strOut = new String(numberSource);
    m_strOut = m_strOut.replace(/[^\d.]/g, '');
    return m_strOut;
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


//$(window).ready(function () {

//    $('body select[filterTarget]').bind('custom_multi_select', function () {
//        //when any of those select values are changed, it will run this function
//        //the selected values are returned within the arrSelected array.
//        //to convert to string,  use the $.map function.
//        var arrSelected = $(this).multiselect("getChecked");
//        var arrString = $.map(arrSelected, function (item) {
//            return item.value;
//        });
//        var filterDiv = $(this).closest("div").attr("id");
//        if (filterDiv.substring(0, 6) == 'DEMO_DC') {
//            var tblDiv = 'tbl' + filterDiv.replace("_filters", "");
//            DEMO_DP_RecalculateTable(tblDiv);
//        }
//    })

//})


function DEMO_DP_CaseMultiply(element, ColumnForPack, ColumnForCost, ColumnForRetail, ColumnForDolSales, ColumnForDolProfits) {
    //alert(element.value);
    var CaseQty = stripNonRealDigits(element.value);
    //alert(CaseQty);
    var DolRetail = stripNonRealDigits(element.parentNode.parentNode.cells[ColumnForRetail].childNodes[0].data);
    var DolCost = stripNonRealDigits(element.parentNode.parentNode.cells[ColumnForCost].childNodes[0].data);
    var PackSize = stripNonRealDigits(element.parentNode.parentNode.cells[ColumnForPack].childNodes[0].data);
    //alert(DolRetail);
    //alert(DolCost);
    if (CaseQty == '') //|| (PackQty == '')
    { // wipe out the columns
        element.parentNode.parentNode.cells[ColumnForDolSales].innerHTML = '';
        element.parentNode.parentNode.cells[ColumnForDolProfits].innerHTML = '';
    }
    else {
        if (CaseQty == 0) {
            element.parentNode.parentNode.cells[ColumnForDolSales].innerHTML = '';
            element.parentNode.parentNode.cells[ColumnForDolProfits].innerHTML = '';
        }
        else {
            DolSales = (CaseQty * PackSize * DolRetail)
            DolProfits = (CaseQty * PackSize * (DolRetail - DolCost))
            var DolSalesFormatted = (Math.round(DolSales * 100) / 100);
            DolSalesFormatted = DolSalesFormatted.toFixed(2);
            //alert(DolSalesFormatted);
            var DolProfitsFormatted = (Math.round(DolProfits * 100) / 100);
            DolProfitsFormatted = DolProfitsFormatted.toFixed(2);
            //alert(DolProfitsFormatted);
            element.parentNode.parentNode.cells[ColumnForDolSales].innerHTML = DolSalesFormatted;
            element.parentNode.parentNode.cells[ColumnForDolProfits].innerHTML = DolProfitsFormatted;
        }
    }
    //alert(element.parentNode.parentNode.parentNode.parentNode.id);
    var TableName = element.parentNode.parentNode.parentNode.parentNode.id
    DEMO_DP_RecalculateTable(TableName);
    //alert(TableName);
}


function DEMO_DP_RecalculateTable(TableDivId) {

    var CasesTarget = 6 //cell that holds the case qty textbox
    var SRPTarget = 5 //Cell that holds the SRP textbox
    var CostTarget = 11 //Cell that contains the value
    var DollarSalesTarget = 7 //cell with items dollar sales value
    var PackSizeTarget = 4 //cell with pack size
    var GrossProfitTarget = 8 //cell with Gross Profit Percent in the cell line


    var TotalTotalUnitsTarget = TableDivId + '_tdItems'
    var TotalCasesTarget = TableDivId + '_tdCases'
    var AverageProfitTarget = TableDivId + '_wm_netValue'
    var TotalSalesTarget = TableDivId + '_tdSales'

    var TotalTotalUnits = 0
    var TotalCases = 0
    var TotalCost = 0
    var TotalDollarSales = parseFloat('0.00')
    var AverageProfit = 0

    //alert(TableDivId);
    //alert (document.getElementById(TableDivId).innerHTML);
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;
    //alert(TableRows);
    //TableRows = 5 //hardcode a stop

    var thisRow = 0

    while (thisRow < TableRows) {
        //alert(thisRow);
        var CaseRowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].getElementsByTagName('tr')[thisRow];
        //	alert(CaseRowElement.className)
        if (!$(CaseRowElement).hasClass('hidden')) {
            if (stripNonRealDigits(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[CasesTarget].childNodes[0].value) != '') {
                //Because there is a Cases value, we can calculate TotalSales & TotalProfits
                thisCases = (Math.round(stripNonRealDigits(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[CasesTarget].childNodes[0].value)));
                thisPackSize = Number(stripNonRealDigits(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[PackSizeTarget].childNodes[0].data))
                thisTotalUnits = (Math.round(thisPackSize * thisCases));
                thisCost = Number(stripNonRealDigits(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[CostTarget].childNodes[0].data))
                thisSRP = Number(stripNonRealDigits((document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[SRPTarget].childNodes[0].data)))

                thisDolSales = (thisTotalUnits * thisSRP)
                thisDolProfits = (thisTotalUnits * (thisSRP - thisCost))
                var DolSalesFormatted = (Math.round(thisDolSales * 100) / 100);
                DolSalesFormatted = DolSalesFormatted.toFixed(2);
                var DolProfitsFormatted = (Math.round(thisDolProfits * 100) / 100);
                DolProfitsFormatted = DolProfitsFormatted.toFixed(2);
                document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[DollarSalesTarget].innerHTML = '$' + DolSalesFormatted;
                document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[GrossProfitTarget].innerHTML ='$' + DolProfitsFormatted;

                //These are for the totals at the bottom of the page!
                TotalCases = TotalCases + thisCases;
                TotalCost = TotalCost + (thisCost * thisTotalUnits);
                TotalTotalUnits = TotalTotalUnits + thisTotalUnits;
                TotalDollarSales = TotalDollarSales + (thisSRP * thisTotalUnits);
            }
            else {
                //Because there is NOT a Cases value, wipe out TotalUnitsTarget on this row.
                document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[DollarSalesTarget].innerHTML = '';
                document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[GrossProfitTarget].innerHTML = '';
            }
        }
        thisRow++;
    }


    //Now to format these
    TotalTotalUnitsFormatted = Math.round(TotalTotalUnits);
    var TotalCasesFormatted = Math.round(TotalCases);

    if (TotalCost != 0) {
        AverageProfit = ((TotalDollarSales - TotalCost) / TotalCost * 100);
        AverageProfitFormatted = Math.round(AverageProfit) + '%';
    }
    else {
        AverageProfitFormatted = '0%';
    }

    var TotalDollarSalesFormatted = addCommas(Math.round(TotalDollarSales));

    // Now output them to the Divs
    document.getElementById(TotalTotalUnitsTarget).innerHTML = TotalTotalUnitsFormatted;
    document.getElementById(TotalCasesTarget).innerHTML = TotalCasesFormatted;
    document.getElementById(AverageProfitTarget).innerHTML = AverageProfitFormatted;
    document.getElementById(TotalSalesTarget).innerHTML = '$' + TotalDollarSalesFormatted;

}

function PopUpWithFadeIn(image, MarginLeft, MarginTop, DivHeight, DivWidth) {
    if (image == null) {
        image = '<p>No image found</p>'
    } 		//in case there is no image source code entered in the index.html

    // Creates the div
    var msg = "<div id='pop' class='parentDisable' style='display: block;'>" +
        "<div id='popup' style='margin-left:" + MarginLeft + "px; margin-top:" + MarginTop + "px; height: " + DivHeight + "px; width: " + DivWidth + "px; display: none;'>" +
        "<a href='#' id='close' class='end'>" +
        "<img src='images/close.png'>" +
        "</a>" +
        image +
        "</div>" +
        "</div>" + "";

    var elNote = document.createElement('div');         // Creates a new element
    elNote.setAttribute('id', 'newDiv');                // Adds an id of newDiv
    elNote.innerHTML = msg;                             // Adds the message
    document.body.appendChild(elNote);				    // Adds it to the page

    var div = document.getElementById('pop');			//Creates variable div for calling 'pop' for the newly created div (above)
    var step = 0;
    steps = 10;
    speed = 60;
    fade = 0.0;
    color = 0.7; //variables for adjusting amount of steps and the darkness/transparency level of the background

    //alert('FadeInPOP(step = '+step+',steps='+steps+',speed='+speed+',fade='+fade+',color = '+color+',div='+div);

    FadeInPOP(div, step, steps, speed, fade, color);  //function call

    function FadeInPOP(div, step, steps, speed, fade, color) { //begin function
        fade += color / steps; 							 //fade will adjust by dividing color with steps
        div.style.background = 'rgba(0,0,0,' + fade + ')';//adjusts the 'style' for the fade in
        //alert(steps);
        if (step < steps) { //evaluates the steps
            step += 1;
            setTimeout(function () {
                FadeInPOP(div, step, steps, speed, fade, color)
            }, 1000 / speed);
        }  //Timer to slow down or speed up via speed variable
        else {   //execute last in order to make the div with image appear
            document.getElementById('popup').style.display = 'block';
        }  //makes the div with image appear
    }

    function dismissNote() {                          	  // Declare function
        document.body.removeChild(elNote);              // Remove the note
    }

    var elClose = document.getElementById('close');   		   // Get the close button
    elClose.addEventListener('click', dismissNote, false); // Click close-clear note
}


function Calculate() {
    var UnitsPerCase = document.getElementById('UnitsPerCase').value;
    var ProjectedCasesSold = document.getElementById('ProjectedCasesSold').value;
    var NewProjectedCasesSold = document.getElementById('NewProjectedCasesSold').value;

    var CaseCost = document.getElementById('CaseCost').value;
    //var CaseCostLength = CaseCost.length;
    //CaseCost = CaseCost.substring(1, CaseCostLength);

    var NewCaseCost = document.getElementById('NewCaseCost').value;
    //var NewCaseCostLength = NewCaseCost.length;
    //NewCaseCost = NewCaseCost.substring(1, NewCaseCostLength);

    var UnitSellPrice = document.getElementById('UnitSellPrice').value;
    //var UnitSellPriceLength = UnitSellPrice.length;
    // UnitSellPrice = UnitSellPrice.substring(1, UnitSellPriceLength);

    var NewUnitSellPrice = document.getElementById('NewUnitSellPrice').value;
    //var NewUnitSellPriceLength = NewUnitSellPrice.length;
    //NewUnitSellPrice = NewUnitSellPrice.substring(1, NewUnitSellPriceLength);

    // BEGIN PROFIT CALCULATIONS
    var UnitCost = (CaseCost / UnitsPerCase).toFixed(2);
    if ((UnitCost != "NaN.00") && (UnitCost != "0.00")) {
        document.getElementById('UnitCost').textContent = ("$" + UnitCost);
    }

    var CaseProfit = ((UnitSellPrice * UnitsPerCase) - CaseCost).toFixed(2);
    if ((CaseProfit != "NaN.00") && (CaseProfit != "0.00")) {
        document.getElementById('CaseProfit').textContent = ("$" + CaseProfit);
    }

    var UnitProfit = (UnitSellPrice - UnitCost).toFixed(2);
    if ((UnitProfit != "NaN.00") && (UnitProfit != "0.00")) {
        document.getElementById('UnitProfit').textContent = ("$" + UnitProfit);
    }

    var revenue = (UnitSellPrice * UnitsPerCase);
    var ProfitMargin = (((revenue - CaseCost) / revenue) * 100).toFixed(2);
    if ((ProfitMargin != "NaN.00") && (ProfitMargin != "-Infinity.00") && (ProfitMargin != "Infinity.00") && (ProfitMargin != "0.00")) {
        document.getElementById('ProfitMargin').textContent = (ProfitMargin + "%");
    }

    var Markup = (revenue - CaseCost);
    Markup = ((Markup / CaseCost) * 100).toFixed(2);
    if ((Markup != "NaN.00") && (Markup != "0.00")) {
        document.getElementById('Markup').textContent = (Markup + "%");
    }
    // END PROFIT CALCULATIONS

    // BEGIN NEW PROFIT CALCULATIONS
    var newUnitCost = (NewCaseCost / UnitsPerCase).toFixed(2);
    if ((newUnitCost != "NaN.00") && (newUnitCost != "0.00")) {
        document.getElementById('newUnitCost').textContent = ("$" + newUnitCost);
    }

    var newCaseProfit = ((NewUnitSellPrice * UnitsPerCase) - NewCaseCost).toFixed(2);
    if ((newCaseProfit != "NaN.00") && (newCaseProfit != "0.00")) {
        document.getElementById('newCaseProfit').textContent = ("$" + newCaseProfit);
    }

    var newUnitProfit = (NewUnitSellPrice - newUnitCost).toFixed(2);
    if ((newUnitProfit != "NaN.00") && (newUnitProfit != "0.00")) {
        document.getElementById('newUnitProfit').textContent = ("$" + newUnitProfit);
    }

    var newRevenue = (NewUnitSellPrice * UnitsPerCase);
    var newProfitMargin = (((newRevenue - NewCaseCost) / newRevenue) * 100).toFixed(2);
    if ((newProfitMargin != "NaN.00") && (newProfitMargin != "-Infinity.00") && (newProfitMargin != "Infinity.00") && (newProfitMargin != "0.00")) {
        document.getElementById('newProfitMargin').textContent = (newProfitMargin + "%");
    }

    var newMarkup = (newRevenue - NewCaseCost);
    newMarkup = ((newMarkup / NewCaseCost) * 100).toFixed(2);
    if ((newMarkup != "NaN.00") && (newMarkup != "0.00")) {
        document.getElementById('newMarkup').textContent = (newMarkup + "%");
    }

    var BreakEvenLift = ((CaseProfit * ProjectedCasesSold) / newCaseProfit).toFixed(1);
    if ((BreakEvenLift != "NaN.0") && (BreakEvenLift != "-Infinity.0") && (BreakEvenLift != "Infinity.0") && (BreakEvenLift != "0.0")) {
        document.getElementById('BreakEvenLift').textContent = (BreakEvenLift + "");
    }
    // END NEW PROFIT CALCULATIONS

    // BEGIN SALES INCREASE
    var ProjectedCases = (UnitsPerCase * ProjectedCasesSold) * UnitSellPrice;
    var NewProjectedCases = (UnitsPerCase * NewProjectedCasesSold) * NewUnitSellPrice;
    var SalesIncrease = (((NewProjectedCases - ProjectedCases) / ProjectedCases) * 100).toFixed(2);
    if ((SalesIncrease != "Infinity.00") && (SalesIncrease != "-Infinity.00") && (SalesIncrease != "NaN.00") && (SalesIncrease != "0.00")) {
        document.getElementById('NewSalesIncreaseTd').textContent = (SalesIncrease) + "%";
    }
    // END SALES INCREASE
}

//document.getElementById('calculate').addEventListener('click', Calculate, false);

//TODO:Need to make the this color function be generic, be able to not color all tables, have specific way to color only one
//TODO:Just quick fix to get the pitch released.
//EvenListener for Gaptracker that grey out the row that was clicked on
document.addEventListener("change", function (event) {
    //get the element that was clicked on
    var el = $(event.target);
    //holds the table id
    var tableID = el.parent().parent().parent().parent();
    //get the TR that contains the checkbox
    var tableRow = el.closest("tr");
    if (el.attr("checked") == "checked" &&
        (el.attr("storagekeyprefix") == "tblMWM_SST_All10" || el.attr("storagekeyprefix") == "tblMWM_SST_All11" ||
         el.attr("storagekeyprefix") == "tblMWM_SST_All12" || el.attr("storagekeyprefix") == "tblMWM_SST_All13" ||
         el.attr("storagekeyprefix") == "tblMWM_SST_All14")) {
        tableRow.addClass("complete");
    }
    else {
        tableRow.removeClass("complete");
    }

});

function CumulativeAddRecalculateTable(TableDivId) {
    console.log(TableDivId);
    //alert(TableDivId);
    var AddTarget = 5 //Cell that holds add checkbox hardcoded
    var ValueTarget1 = 3 //Cell that contains the value
    var ValueTarget2 = 4 //Cell that contains the second value


    var AddValueTarget = TableDivId + '_AddValue'
    var NetValueTarget = TableDivId + '_NetValue'

    var AddValue = 0
    var SubtractValue = 0
    var ImpactValue = 0
    var NetValue = 0

    //alert(TableDivId);
    //alert (document.getElementById(TableDivId).innerHTML);
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;
    //alert(TableRows);

    var thisRow = 0

    while (thisRow < TableRows) {
        var CaseRowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].getElementsByTagName('tr')[thisRow];
        //alert(CaseRowElement.className)
        if (!$(CaseRowElement).hasClass('hidden')) {
            //if (document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[ValueTarget1] != null)
            var rowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow]
            if ($(rowElement.children[AddTarget].childNodes[1]).hasClass('checked')) {
                //var rowElement=document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow]
                var targetValue1 = parseFloat(stripNonRealDigits(rowElement.children[ValueTarget1].innerHTML));
                console.log(targetValue1);

                var targetValue2 = parseFloat(stripNonRealDigits(rowElement.children[ValueTarget2].innerHTML));
                console.log(targetValue2);

                var targetValue = (targetValue1 + targetValue2);
                var targetValueRounded = Math.round(targetValue * Math.pow(10, 2)) / Math.pow(10, 2);
                var targetValueRounded = targetValueRounded.toFixed(2);

                if ($(rowElement.children[AddTarget].childNodes[1]).hasClass('checked')) {
                    //alert ('add it');
                    AddValue = AddValue + targetValue;
                    //alert(AddValue);
                }
                //if ($(rowElement.children[SubtractTarget].childNodes[0]).hasClass('checked'))
                //{
                //alert ('subtract it');
                //SubtractValue=SubtractValue+targetValue;
                //alert(SubtractValue);
                //}
            }
        }
        thisRow++;
    }
    NetValue = (AddValue * 0.31);
    console.log(NetValue);
    //Now to format these
    var AddValueFormatted = '$' + Math.round(AddValue);
    console.log(AddValueFormatted);
    var NetValueFormatted = '$' + Math.round(NetValue);
    console.log(NetValueFormatted);


    // Now output them to the Divs
    document.getElementById(AddValueTarget).innerHTML = AddValueFormatted;
    document.getElementById(NetValueTarget).innerHTML = NetValueFormatted;
}



/*Par Calc Fills in the zero at the total on page load*/
$(document).ready(function () {
    document.getElementById('divPCSummaryPacCalcTotal').innerHTML = 0;
});
/*INPUT: Your store input, current value, point value and max point value
 *
 * Description: The function use the input values to do calculations and based on the calculation,
 * we output to the current value column.
 *
 * OUTPUT: outputs the calculated value into current and updates the div with total
 * */
function FindMax(max) {

    var myMax = stripNonRealDigits(max.substring(max.indexOf("Max")));

    return myMax;
}

function ParCalc(element, ColumnForCurrentValue, ColumnForPointValue, ColumnForMaxPointValue) {

    /*Init and populate the variables with data based on the row it was clicked on.*/
    var YourStore = stripNonRealDigits(element.value);
    var curentValueCell = null;


    var PointValue = stripNonRealDigits(element.parentNode.parentNode.cells[ColumnForPointValue].childNodes[0].data);


    var MaxPointValue = FindMax(element.parentNode.parentNode.cells[ColumnForMaxPointValue].childNodes[0].data);

    //temp value for current value
    var temp = YourStore * PointValue;


    if (temp <= MaxPointValue && temp > 0) {
        curentValueCell = temp;
    }
    else if (YourStore == 0) {
        curentValueCell = null;
    }
    else {
        curentValueCell = MaxPointValue;
    }

    /*Ouputs the calc value into the cell*/
    element.parentNode.parentNode.cells[ColumnForCurrentValue].innerHTML = curentValueCell;


    //clear the variable after each run
    temp = null;
    curentValueCell = null;

    /*Get the table ID so i can work with it in PacCalcTotal Function*/
    var TableDivId = element.parentNode.parentNode.parentNode.parentNode.id;
    ParCalcTotal(TableDivId);

}

/*Function calculates the total for Par Calc*/
function ParCalcTotal(TableDivId) {

    /*Get how many table rows we have so i can use this to loop through each one in the table */
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;

    /*Init values that will use for calc*/
    var cellValue = 0;
    var total = 0;

    var thisRow = 0;
    /*Loop through the whole table, if there is value in Current Value column, add it to total else skip that cell*/
    while (thisRow < TableRows) {
        if (document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[5].innerHTML != "") {

            /*Gets the value of Current Value from each row of the table*/
            cellValue = parseInt(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[5].innerHTML);

            /*Holds the total for the table*/
            total = total + cellValue;
        }

        thisRow++;
    }
    /*Output the total of the table after it has done running*/
    document.getElementById('divPCSummaryPacCalcTotal').innerHTML = total;
}


function populate_table_overload(TableDivId) {
    //alert(TableDivId.substring(0,10));
    if (TableDivId.substring(0, 19) == 'tblENR_POS_Overview') {
        POSArrows(TableDivId);
    };
};


function POSArrows(TableDivId) {
    var StrValueTarget = 2 //Cell that contains the market percentage
    var MktValueTarget = 3 //Cell that contains the market percentage
    var ImageTarget = 4 //Cell that contains the image
    var upArrow = '<img height="30" src="images/arrow_green.png">';
    var downArrow = '<img height="30" src="images/arrows_red.png">';
    var equalSign = '<img height="30" src="images/equal-sign.jpg">';

    //alert(TableDivId);
    //alert (document.getElementById(TableDivId));
    //alert (document.getElementById(TableDivId).innerHTML);
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;
    //alert(TableRows); 
    var thisRow = 0

    while (thisRow < TableRows) {
        //alert(thisRow);
        if ((document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[StrValueTarget] != null) && (document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[MktValueTarget] != null)) {
            var rowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow]
            var StrTargetValue = rowElement.children[StrValueTarget].innerHTML;
            var MktTargetValue = rowElement.children[MktValueTarget].innerHTML;
            //alert(StrTargetValue);
            //alert(MktTargetValue);
            var StrValue = parseFloat(StrTargetValue.replace("%", ""));
            var MktValue = parseFloat(MktTargetValue.replace("%", ""));
            if ((typeof (MktValue) == 'number') && (typeof (StrValue) == 'number')) {
                if (StrValue < MktValue) {
                    rowElement.children[ImageTarget].innerHTML = downArrow;
                } else if (StrValue > MktValue) {
                    rowElement.children[ImageTarget].innerHTML = upArrow;
                } else {
                    rowElement.children[ImageTarget].innerHTML = equalSign;
                };
            };
        };
        thisRow++;
    }
};





//████████████████████████████████████████████████████████████████████████
// Multiple Custom Photo SLide and other related Functions MCASTRO(8/16)
//████████████████████████████████████████████████████████████████████████

function uploadPhoto(event) {
    var photoUrl = null;
    if (location.search == '') {
        photoUrl = "images/defaultUploadImage.jpg";
        displayAndStoreImage(photoUrl, event)
    }
    else {
        var options = { sourceType: SFPresentationAPI.Camera.PictureSourceType.PHOTOLIBRARY };
        SFPresentationAPI.acquire_photo(options, function (photoUrl) {
            displayAndStoreImage(photoUrl, event);
        }, function (e) {
            alert("error getting photo: " + JSON.stringify(e));
        });
    }
}

//████████████████████████████████████████████████████████████████████████
// related CPS Functions MCASTRO(8/16)
//████████████████████████████████████████████████████████████████████████

function displayAndStoreImage(photoUrl, event) {

    var name = event.currentTarget.name;
    var currentSlideId = event.currentTarget.accessKey;
    var targetImgSrc = event.currentTarget.value;
    var cstmPhtCount = event.currentTarget.attributes[4].value;


    document.getElementById(targetImgSrc).src = photoUrl;

    saveToLocalStorage(currentSlideId, targetImgSrc, photoUrl, name, cstmPhtCount);
}

function saveToLocalStorage(currentSlideId, targetImgSrc, photoUrl, name, cstmPhtCount) {

    var myKey = currentSlideId + '_' + cstmPhtCount;

    //console.log(myKey);

    var nLength = name.length;
    var pL = nLength - 1;
    var identifier = name.substring(pL, nLength);
    var x = localStorage.getItem(myKey)

    if (x == null) {
        var myProps = new Object();
        localStorage.setItem(myKey, JSON.stringify(myProps));
    }

    saveThisKey(myKey);

    var myImageDetails = JSON.parse(localStorage.getItem(myKey));

    myImageDetails.targetImgSrc = targetImgSrc;
    myImageDetails.photoUrl = photoUrl;

    localStorage.setItem(myKey, JSON.stringify(myImageDetails));
}

function saveThisKey(theKey) {
    var saveMe = true;
    var x = localStorage.getItem("myImageKeys")
    if (x == null) {
        var myKeys = [];
        localStorage.setItem("myImageKeys", JSON.stringify(myKeys));
    }

    var keysA = JSON.parse(localStorage.getItem("myImageKeys"));
    for (z = 0; z < keysA.length; z++) {

        if (keysA[z] == theKey) {
            saveMe = false;
        }
    }

    if (saveMe) {
        keysA.push(theKey);
    }

    localStorage.setItem("myImageKeys", JSON.stringify(keysA));
}

function loadSlidePhotos(slideId) {

    slideId = slideId.substring(1, slideId.length)
    var myIds = JSON.parse(localStorage.getItem("myImageKeys"));

    if (!myIds) return;
    for (x = 0; x < myIds.length; x++) {
        var thisId = myIds[x];
        if (thisId.includes(slideId)) {

            var details = JSON.parse(localStorage.getItem(thisId));
            var targetId = details.targetImgSrc;
            var photoUrl = details.photoUrl;
            document.getElementById(targetId).src = photoUrl;
        }
    }
}

//████████████████████████████████████████████████████████████
//             Custom Photo SLide Functions END
//████████████████████████████████████████████████████████████


function CustomPhotoPopup(slideId, id) {

    var myKey = slideId + '_' + id;
    console.log(myKey)

    var details = JSON.parse(localStorage.getItem(myKey));

    if (!details) return;

    var myUrl = details.photoUrl;
    image = '<img src="' + myUrl + '" height="450" >';
    showPopUpImage(image, 181, 42, 482, 800);

}

function showPopUpImage(image, MarginLeft, MarginTop, DivHeight, DivWidth) {
    // Creates the div
    var msg = "<div id='pop' class='parentDisable' style='display: block;'>" +
    "<div id='popup' style='margin-left:" + MarginLeft + "px; margin-top:" + MarginTop + "px; height: " + DivHeight + "px; width: " + DivWidth + "px; display: none;'>" +
    "<a href='#' id='close' class='end'>" +
    "<img class='redButton' src='images/red_x.png'>" +
    "</a>" +
    "<div class='container'>" +
    "<div id=''>" +

    image +

    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" + "";

    var elNote = document.createElement('div');         // Creates a new element
    elNote.setAttribute('id', 'newDiv');                // Adds an id of newDiv
    elNote.innerHTML = msg;                             // Adds the message
    document.body.appendChild(elNote);				    // Adds it to the page


    var div = document.getElementById('pop');			//Creates variable div for calling 'pop' for the newly created div (above)
    var step = 0;
    steps = 10;
    speed = 60;
    fade = 0.0;
    color = 0.7; //variables for adjusting amount of steps and the darkness/transparency level of the background


    FadeInPOP(div, step, steps, speed, fade, color);  //function call

    function FadeInPOP(div, step, steps, speed, fade, color) { //begin function
        fade += color / steps; 							 //fade will adjust by dividing color with steps
        div.style.background = 'rgba(0,0,0,' + fade + ')';//adjusts the 'style' for the fade in
        //alert(steps);
        if (step < steps) { //evaluates the steps
            step += 1;
            setTimeout(function () {
                FadeInPOP(div, step, steps, speed, fade, color)
            }, 1000 / speed);
        }  //Timer to slow down or speed up via speed variable
        else {   //execute last in order to make the div with image appear
            document.getElementById('popup').style.display = 'block';
        }  //makes the div with image appear
    }


    function dismissNote() {                          	  // Declare function
        document.body.removeChild(elNote);              // Remove the note
    }

    var elClose = document.getElementById('close');   		   // Get the close button
    elClose.addEventListener('click', dismissNote, false); // Click close-clear note

    //$(function () {
    //    $("#slides").slidesjs({
    //        width: 940,
    //        height: 528
    //    });
    //});
}



////////////////////////////////////////////////////////////////////
//          Suggested Order PDF start
////////////////////////////////////////////////////////////////////




var selectedProducts = [];
function checkboxcheck(element, productName, upc, qty) {
    console.log('checkboxcheck');
    var count = $("#tblSugOrd_All1 input[type='checkbox']:checked").length;
    var $element = $(element);
    var checkbox = $(element.parentNode.parentNode.cells[0].childNodes[0]);
    var productName = $(element.parentNode.parentNode.cells[productName].childNodes[0]);
    var $upc = $(element.parentNode.parentNode.cells[upc].childNodes[0].data);
    var qty = ((element.parentNode.parentNode.cells[qty].childNodes[0].value == "") ? 0 : element.parentNode.parentNode.cells[qty].childNodes[0].value);

    //get product image
    //  var productImageURL = buildPDFImage($upc.selector);
    //var barcode = makeBarCode($upc.selector);


    console.log(checkbox.attr('checked') == 'checked');
    if (count <= 12) {
        if (checkbox.attr('checked') == 'checked') {
            //check if in the array, if it is remove it or push it if not in array
            if ($.inArray($upc.selector, selectedProducts == 0)) {
                for (i = 0; i < selectedProducts.length; i++) {
                    if (selectedProducts[i].UPC == $upc.selector) {
                        selectedProducts.splice([i], 1);
                        // console.log("deleting identical item");
                    }
                }
                //console.log("pushing another element in the array");
                selectedProducts.push({
                    "product": productName[0].data,
                    "UPC": $upc.selector,
                    //"barcode": barcode,
                    "qty": qty,
                    //"productImageURL":productImageURL
                    //"barcode": barcode
                });
            }
        }
        if (checkbox.attr('checked') != 'checked') {
            if (selectedProducts.length != 0) {
                for (i = 0; i < selectedProducts.length; i++) {
                    if (selectedProducts[i].UPC == $upc.selector) {
                        selectedProducts.splice([i], 1);
                    }
                }
            }
            //console.log("removed from array");
        }


        //mike castro code
        //  var sNumber = pitch_data.STORENUMBER;
        //  var key = sNumber + "MLF_CustomPDF";
        // console.log(key);
        //save_storage("demoPDF", JSON.stringify(selectedProducts));

        //build pdf table from selected products

        //console.log(selectedProducts);

        // BuildHTMLPDFTable(selectedProducts);
    }
    else {

        checkbox.prop('checked', false);

        confirm("You cannot select more then 12 products!");

    }
    console.log(selectedProducts)
}



function loadPitchDataFromLocalStorage() {


    if (localStorage.getItem("demoPDF") != null) {
        selectedProducts = JSON.parse(localStorage.getItem("demoPDF"));
        BuildHTMLPDFTable(selectedProducts);
    }

}





//////////////////////////////////////////
// Creates data for your sugg order table
//////////////////////////////////////////

function DynamicPitch_SetStringAndWrite(data_hash_table) {
    //alert('start');
    var Activities = data_hash_table['ARTS_Client_Activities'];
    // console.log(Activities);

    var ListString = '';

    //CORE Counter
    var coreFamilyOutOfStockCounter = 0;
    var coreFamilyDistributionVoidsCounter = 0;

    //FLEX Counter
    var flexFamilyOutOfStockCounter = 0;
    var flexFamilyDistributionVoidsCounter = 0;

    //Order Suggested
    var pegFamilyOutOfStockCounter = 0;
    var pegFamilyDistributionVoidsCounter = 0;


    for (var x = 0; x < Activities.length; x++) {


        for (var i in Activities[x].distribution_folder) { //Ok, now we are in each product.
            //check if OS exist in the object

            if (Activities[x].distribution_folder[i]['AC'] == null) {
                continue;
            }

            if (typeof Activities[x].distribution_folder[i]['AC'] != "undefined") {

                //In Aisle
                if (Activities[x].distribution_folder[i]['PF'] == "9113") {
                    //|| (Activities[x].distribution_folder[i]['OS'].pt == "Out-Of-Stock (OOS)" && Activities[x].distribution_folder[i]['AC'].pt == "Ordered (OR)")
                    //|| (Activities[x].distribution_folder[i]['OS'].pt == "Distribution Void (DV)" && Activities[x].distribution_folder[i]['AC'].pt == "Ordered (OR)")

                    if ((Activities[x].distribution_folder[i]['OS'].pt == "On Shelf (SH)" && Activities[x].distribution_folder[i]['AC'].pt == "Order Suggested (ORS)") || (Activities[x].distribution_folder[i]['OS'].pt == "Out-Of-Stock (OOS)" && Activities[x].distribution_folder[i]['AC'].pt == "Ordered (OR)") || (Activities[x].distribution_folder[i]['OS'].pt == "Distribution Void (DV)" && Activities[x].distribution_folder[i]['AC'].pt == "Ordered (OR)")) {
                        pegFamilyOutOfStockCounter++;
                        //ORDER SUGGESTER CIRCLE
                        //Building the string that will be later converted into Object to be used as a datasouce by a table on the front end
                        if (ListString == '' && (Activities[x].distribution_folder[i]['prompt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (ListString == '' && (Activities[x].distribution_folder[i]['pt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['prompt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['pt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }

                    }
                    else if (Activities[x].distribution_folder[i]['OS'].pt == "Distribution Void (DV)" && Activities[x].distribution_folder[i]['AC'].pt == "Tagged (TG)") {
                        coreFamilyOutOfStockCounter++;

                        //Building the string that will be later converted into Object to be used as a datasouce by a table on the front end
                        if (ListString == '' && (Activities[x].distribution_folder[i]['prompt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (ListString == '' && (Activities[x].distribution_folder[i]['pt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['prompt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['pt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                    }
                    else if (Activities[x].distribution_folder[i]['OS'].pt == "Out-Of-Stock (OOS)" && Activities[x].distribution_folder[i]['AC'].pt == "Filled (F)") {
                        flexFamilyOutOfStockCounter++;

                        //Building the string that will be later converted into Object to be used as a datasouce by a table on the front end
                        if (ListString == '' && (Activities[x].distribution_folder[i]['prompt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (ListString == '' && (Activities[x].distribution_folder[i]['pt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['prompt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['pt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                    }
                }

                //Front End
                if (Activities[x].distribution_folder[i]['PF'] == "9112") {

                    if (Activities[x].distribution_folder[i]['OS'].pt == "On Shelf (SH)" && Activities[x].distribution_folder[i]['AC'].pt == "Order Suggested (ORS)") {
                        pegFamilyOutOfStockCounter++;


                        //Building the string that will be later converted into Object to be used as a datasouce by a table on the front end
                        if (ListString == '' && (Activities[x].distribution_folder[i]['prompt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (ListString == '' && (Activities[x].distribution_folder[i]['pt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['prompt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['pt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                    }
                    else if (Activities[x].distribution_folder[i]['OS'].pt == "Distribution Void (DV)" && Activities[x].distribution_folder[i]['AC'].pt == "Tagged (TG)") {
                        coreFamilyOutOfStockCounter++;

                        //Building the string that will be later converted into Object to be used as a datasouce by a table on the front end
                        if (ListString == '' && (Activities[x].distribution_folder[i]['prompt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (ListString == '' && (Activities[x].distribution_folder[i]['pt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['prompt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['pt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                    }
                    else if (Activities[x].distribution_folder[i]['OS'].pt == "Out-Of-Stock (OOS)" && Activities[x].distribution_folder[i]['AC'].pt == "Filled (F)") {

                        flexFamilyOutOfStockCounter++;

                        //Building the string that will be later converted into Object to be used as a datasouce by a table on the front end
                        if (ListString == '' && (Activities[x].distribution_folder[i]['prompt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (ListString == '' && (Activities[x].distribution_folder[i]['pt'])) {
                            ListString += '{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['prompt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['prompt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                        else if (Activities[x].distribution_folder[i]['pt']) {
                            ListString += ',{"UPC":"' + Activities[x].distribution_folder[i]['codes'].substring(1, 13) + '","Product":"' + Activities[x].distribution_folder[i]['pt'] + '","Family":"' + Activities[x].distribution_folder[i]['PF'] + '","Status":"' + Activities[x].distribution_folder[i]['OS'].pt + '"}';
                        }
                    }

                }

            }
        }//end Distribution Folder
    }//end Activities


    //Massage data quick and easy since client wants PF from Json displayed as EX 10373 -> Core 40
    ListString = ListString.replace(/11012/g, 'Front End');
    ListString = ListString.replace(/11013/g, 'In Aisle');

    ListString = ListString.replace(/9113/g, 'Shamp/Cond');
    ListString = ListString.replace(/9112/g, 'Hair Care');

    ListString = ListString.replace(/Distribution Void \(DV\)/g, "DV")
    ListString = ListString.replace(/Out-Of-Stock \(OOS\)/g, 'OOS');
    ListString = ListString.replace(/On Shelf \(SH\)/g, 'SH');

    if (ListString == '') {
        ListString = 'No Distribution items able to be listed.';
    }
    else {
        ListString = '{DEMO_SugOrd_All1:[' + ListString + ']}';
        var OOSobj = eval("(" + ListString + ')');
    }


    //check if obj there or crashs the app if there is no data for this data source.
    if (OOSobj != undefined) {
        //sort
        OOSobj.DEMO_SugOrd_All1.sort(function (a, b) {
            // return (a.Product) - (b.Product);
            return (a.Product > b.Product) - (a.Product < b.Product)
        });
    }


    //Populate the summary overview on the page with couter values 040000498582
    //Core
    document.getElementById('coreFamilyOutOfStockCounterContent').innerHTML = coreFamilyOutOfStockCounter + coreFamilyDistributionVoidsCounter;
    document.getElementById('coreFamilyDistributionVoidsCounterContent').innerHTML = coreFamilyDistributionVoidsCounter;
    //Flex
    document.getElementById('flexFamilyOutOfStockCounterContent').innerHTML = flexFamilyOutOfStockCounter + flexFamilyDistributionVoidsCounter;
    document.getElementById('flexFamilyDistributionVoidsCounterContent').innerHTML = flexFamilyDistributionVoidsCounter;
    //Peg
    document.getElementById('pegFamilyOutOfStockCounterContent').innerHTML = pegFamilyOutOfStockCounter + pegFamilyDistributionVoidsCounter;
    document.getElementById('pegFamilyDistributionVoidsCounterConent').innerHTML = pegFamilyDistributionVoidsCounter;


    //merge dynamic object
    $.extend(true, data_hash_table, OOSobj);

    return data_hash_table;

}

//For PDF Button by THE KGB
//function create_pdf() {


//    //close the side bar
//    var $left = $('div#left');
//    if ($left.is(':visible')) {
//        $('div#splitter').fadeOut(50, function () {
//            $left.hide("slide", { direction: 'left' }, 300, function () {
//                $('div#splitter').fadeIn(50);
//                $('div#splitter').toggleClass('collapse');
//            });
//        });
//    }
//    else {
//        $('div#splitter').hide(50, function () {
//            $left.show("slide", { direction: 'left' }, 300, function () {
//                $('div#splitter').fadeIn(50)
//                $('div#splitter').toggleClass('collapse');
//            });
//        });
//    }

//    //remove the pitch border background from the pdf
//    $("html").css({ 'background-color': 'white' });

//    //hide the PDF button 
//    $('.creatPDF').css({ 'display': 'none' });

//    //generate the pdf
//    ASMPDFHelper.pitch_pdf($('div.current'));

//    //temp fix to put back the border around the pitch after pdf generates
//    window.setTimeout(function () {
//        $("html").css({ 'background-color': ' #F2F2F2' });
//        $('.creatPDF').css({ 'display': 'block' });

//    }, 1000);


//}

//function BuildHTMLPDFTable(selectedProducts) {

//    //Data Object
//    var data = selectedProducts;

//    //Get the inner divs of the html template 
//    var HTMLTemplate = $("#containerKGB").children();

//    //Clear the table
//    //Using text('') or html('') will cause some string parsing to take place, which generally is a bad idea when working with the DOM. Try and use DOM manipulation methods that do not involve string representations of DOM objects wherever possible.
//    $('#containerKGB div').empty();

//    if (data.length <= 12) {
//        for (var i = 0; i < data.length; i++) {
//            for (var x = 0; x < data.length; x++) {
//                HTMLTemplate[x].innerHTML =
//                   "<div class='imageWrapper'><img id='cellProductImage' height='50' onerror='imgError(this);' src='./images/Products/" + data[x].UPC + ".png'></div>" +
//                   "<div class='cellProductName'>" + data[x].product + "</div>" +
//                   "<div class='cellProductQTY'>" + "Qty: " + data[x].qty + "</div>" +
//                   "<div class='cellBarcode'>" + data[x].barcode + "</div>"
//            }
//        }
//    }
//}



////////////////////////////////////////////////////////////////////
//          Suggested Order PDF end
////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////
//       WM LAG  Clear Button Function START
////////////////////////////////////////////////////////////////////

function clearWMLAGInputs(targetId) {
    var $storageKeys = $('#' + targetId).find('*[storagekeyprefix]');
    $storageKeys.each(function () {
        var $item = $(this);
        save_storage($item.attr('storagekeyprefix'), '');
        if ($(this).prop('tagName') == 'INPUT' && $item.is('[type=checkbox]')) {
            $item.attr('checked', value ? (value.toLowerCase() == 'true' || value === true) : false);
        } else if ($(this).prop('tagName') == 'INPUT' && $item.is('[type=tel]')) {
            $item.attr('value', '');
        } else if ($(this).prop('tagName') == 'INPUT' && $item.is('[type=number]')) {
            console.log($(this).prop('tagName'), $(this));
            $item.prop('value', '');
        } else if ($(this).prop('tagName') == 'DIV') {
            $item.prop('innerHTML', '$0');
        }
    });
}

////////////////////////////////////////////////////////////////////
//         Clear Button Function END
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
//         WM LAG Load storageKeyPrefix Values START
////////////////////////////////////////////////////////////////////

function loadWMLAGValues(targetId) {
    var $storageKeys = $('#' + targetId).find('*[storagekeyprefix]');
    $storageKeys.each(function (index) {
        var $item = $(this);
        var value = load_storage($item.attr('storagekeyprefix'));
        console.log(value);
        if ($(this).prop('tagName') == 'INPUT' && $item.is('[type=checkbox]')) {
            $item.attr('checked', value ? (value.toLowerCase() == 'true' || value === true) : false);
        } else if ($(this).prop('tagName') == 'INPUT' && $item.is('[type=tel]')) {
            $item.attr('value', value);
        } else if ($(this).prop('tagName') == 'INPUT' && $item.is('[type=number]')) {
            $item.attr('value', value);
        } else if ($(this).prop('tagName') == 'DIV') {
            if (value != null || value != undefined) {
                value = Number(value);
                $item.prop('innerHTML', "$" + value.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
            }
        }
    });
}

////////////////////////////////////////////////////////////////////
//         WM LAG Load storageKeyPrefix Values END
////////////////////////////////////////////////////////////////////