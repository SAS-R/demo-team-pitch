--    Select SourcePath, Title,  TouchPresentationDefinition, Descrip from dbo.TouchPresentationDefinition (nolock) where SourcePath like 'Z:\Demo%' order by VisibilityFinish desc, SourcePath

declare @thisPitch varchar(40)
Set @thisPitch = 'D3371C5F-659D-475B-B071-52F0A3D6FF3E'


IF OBJECT_ID('TEMPDB..#TMP1') IS NOT NULL BEGIN DROP TABLE #TMP1 END

create table #TMP1  (input Varchar(100))

Insert into #TMP1 values('ENR_POSGraph_HolidayPrgm1_SOURCE')
Insert into #TMP1 values('ENR_POSGraph_HolidayPrgm1')
Insert into #TMP1 values('ENR_POSGraph_HolidayPrgm1_PRODUCT')
Insert into #TMP1 values('ENR_POSGraph_HolidayPrgm1_STORENUMBER')
Insert into #TMP1 values('ENR_POSGraph_HolidayPrgm1_OCTOBER')
Insert into #TMP1 values('ENR_POSGraph_HolidayPrgm1_NOVEMBER')
Insert into #TMP1 values('ENR_POSGraph_HolidayPrgm1_DECEMBER')
Insert into #TMP1 values('ENR_POSGraph_HolidayPrgm1_JANUARY')
Insert into #TMP1 values('Demo_PAR_HD_All1')
Insert into #TMP1 values('DEMO_LAG_WM1')

Insert into #TMP1 values('ARTS_Client_Activities')
Insert into #TMP1 values('STORENUMBER')


-- Insert this list in to dbo.TouchPresentationInputDataLink creating a GUID for the TouchPresentationInputDataLink field.
--TouchPresentationInputDataLink
insert into TouchPresentationInputDataLink
Select newid() TouchPresentationInputDataLink, TouchPresentationInputData, @thisPitch  as TouchPresentationDefinition, null
from dbo.TouchPresentationInputData (nolock)
where ID in (select input from #TMP1 (nolock))
and TouchPresentationInputData NOT in (Select TouchPresentationInputData from dbo.TouchPresentationInputDataLink (nolock) where TouchPresentationDefinition = @thisPitch)
