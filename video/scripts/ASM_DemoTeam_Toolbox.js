function stripNonRealDigits(numberSource) {
    var m_strOut = new String(numberSource);
    m_strOut = m_strOut.replace(/[^\d.]/g, '');
    return m_strOut;
}

function ClearAll(container_name, recalculate_table) {
    if (container_name == null)
        container_name = $(event.srcElement).parent().attr('id');

    var $container;
    $container = $('#' + container_name);

     //Gets all the highlighted rows in the table.
    var tableCompleteRows = $container.children().children().children().children();

    var $multiSelects = $container.find('select[multiselect=true][storagekeyprefix]');
    $multiSelects.multiselect("uncheckAll");
    $multiSelects.multiselect("close");
    var $Selects = $container.find('select[multiselect=false][storagekeyprefix]');
    $Selects.val('');
    $Selects.change();

    var $elementsWithStoragesKeys = $container.find('*[storagekeyprefix]')
    $elementsWithStoragesKeys.each(function (index) {
        var $item = $(this);
        if ($item.is('[type=checkbox]')) {
            $item.attr('checked', false);
            //remove highlighted row
            tableCompleteRows.removeClass('complete');
            $item.change();
        } else if ($item.hasClass('checkboximage')) {
            if ($item.hasClass('checked'))
                $item.click();
        } else if ($item.is('[type=textbox]')) {
            $item.val('');
            $item.change();
        }
    });
    //change this by calculator
    if (recalculate_table.substring(0, 6) == 'DEMO_PC') {
        BigBetsRecalculateTable(recalculate_table);
    }
    //change this by calculator
    //alert(recalculate_table.substring(3,10));
    if (recalculate_table.substring(3, 10) == 'DEMO_DC') {
        //alert(recalculate_table);
        DEMO_DP_RecalculateTable(recalculate_table);
    }
    //change this by calculator
    //alert(recalculate_table.substring(0,7));
    if (recalculate_table.substring(0, 7) == 'SCJ_OSA') {
        CumulativeAddRecalculateTable(recalculate_table);
    }

}

$(window).ready(function () {
    $('body select[filterTarget]').bind('custom_multi_select', function () {
        //when any of those select values are changed, it will run this function
        //the selected values are returned within the arrSelected array.
        //to convert to string,  use the $.map function.
        var arrSelected = $(this).multiselect("getChecked");
        var arrString = $.map(arrSelected, function (item) {
            return item.value;
        });
        var filterDiv = $(this).closest("div").attr("id");
        if (filterDiv.substring(0, 6) == 'DEMO_PC') {
            var tblDiv = 'tbl' + filterDiv.replace("_filters", "");
            BigBetsRecalculateTable(tblDiv);
        }
        if (filterDiv.substring(0, 7) == 'SCJ_OSA') {
            var tblDiv = 'tbl' + filterDiv.replace("_filters", "");
            CumulativeAddRecalculateTable(tblDiv);
        }
    })

})

function CumulativeAdd(element) {
	var $element = $(element);
	//use the follow items to changet check state,
	//also save the updated check states to storage.
	$element.toggleClass('checked');
	save_storage($element.attr('storagekeyprefix'), $element.hasClass('checked'));

var TableName=element.parentNode.parentNode.parentNode.parentNode.id
CumulativeAddRecalculateTable(TableName);
}

function BigBetsfindit(element, ColumnToShow, ColumnToChange) {
    var $element = $(element);
    //use the follow items to changet check state,
    //also save the updated check states to storage.
    $element.toggleClass('checked');
    save_storage($element.attr('storagekeyprefix'), $element.hasClass('checked'));

    var CBChecked = $element.hasClass('checked');

    if (CBChecked == 1) {
        var $otherCheckbox = $(element.parentNode.parentNode.cells[ColumnToChange].childNodes[0]);
        if ($otherCheckbox.hasClass('checked')) {
            $otherCheckbox.removeClass('checked');
            save_storage($otherCheckbox.attr('storagekeyprefix'), $otherCheckbox.hasClass('checked'));
        }
    }
    var TableName = element.parentNode.parentNode.parentNode.parentNode.id
    BigBetsRecalculateTable(TableName);
}

function BigBetsRecalculateTable(TableDivId) {

    var AddTarget = 8 //Cell that holds add checkbox hardcoded
    var SubtractTarget = 7 //Cell that holds subtract checkbox hardcoded
    var ValueTarget = 6 //Cell that contains the value


    var AddValueTarget = TableDivId + '_AddValue'
    var SubtractValueTarget = TableDivId + '_SubtractValue'
    var ImpactValueTarget = TableDivId + '_ImpactValue'
    var NetValueTarget = TableDivId + '_NetValue'

    var AddValue = 0
    var SubtractValue = 0
    var ImpactValue = 0
    var NetValue = 0

//alert(TableDivId);
//alert (document.getElementById(TableDivId).innerHTML);
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;
//alert(TableRows);

    var thisRow = 0

    while (thisRow < TableRows) {
        //alert(thisRow);
        var CaseRowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].getElementsByTagName('tr')[thisRow];
        //alert(CaseRowElement.className)
        if (!$(CaseRowElement).hasClass('hidden')) {
            if (document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[ValueTarget] != null) {
                var rowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow]
                var targetValue = parseFloat(stripNonRealDigits(rowElement.children[ValueTarget].innerHTML))
                var targetValueRounded = targetValue.toFixed(2)
                //alert (stripNonRealDigits(rowElement.children[ValueTarget].innerHTML))
                if ($(rowElement.children[AddTarget].childNodes[0]).hasClass('checked')) {
                    //alert ('add it');
                    AddValue = AddValue + targetValue;
                    //alert(AddValue);
                }
                if ($(rowElement.children[SubtractTarget].childNodes[0]).hasClass('checked')) {
                    //alert ('subtract it');
                    SubtractValue = SubtractValue + targetValue;
                    //alert(SubtractValue);
                }
            }
        }
        thisRow++;
    }
    ImpactValue = AddValue - SubtractValue;
//alert(ImpactValue);
    if (SubtractValue != 0) {
        NetValue = ImpactValue / SubtractValue * 100;
    }
    else {
        NetValue = 0
    }
//alert(NetValue);

//Now to format these
    var AddValueFormatted = '$' + Math.round(AddValue);
//alert(AddValueFormatted);
    var SubtractValueFormatted = '$' + Math.round(SubtractValue);
//alert(SubtractValueFormatted);
    var ImpactValueFormatted = '$' + Math.round(ImpactValue);
//alert(ImpactValueFormatted);
    var NetValueFormatted = Math.round(NetValue) + '%';
//alert(NetValueFormatted);

    var IndicatorArrow = '';
    if (NetValue != 0) {
        if (NetValue < 0) {
            IndicatorArrow = '<img src="images/red_down.png">';
        }
        else {
            IndicatorArrow = '<img src="images/green_up.png">';
        }
    }

// Now output them to the Divs
    document.getElementById(AddValueTarget).innerHTML = AddValueFormatted;
    document.getElementById(SubtractValueTarget).innerHTML = SubtractValueFormatted;
    document.getElementById(ImpactValueTarget).innerHTML = ImpactValueFormatted;
    document.getElementById(NetValueTarget).innerHTML = NetValueFormatted + IndicatorArrow;
}


function stripNonRealDigits(numberSource) {
    var m_strOut = new String(numberSource);
    m_strOut = m_strOut.replace(/[^\d.]/g, '');
    return m_strOut;
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


$(window).ready(function () {
    $('body select[filterTarget]').bind('custom_multi_select', function () {
        //when any of those select values are changed, it will run this function
        //the selected values are returned within the arrSelected array.
        //to convert to string,  use the $.map function.
        var arrSelected = $(this).multiselect("getChecked");
        var arrString = $.map(arrSelected, function (item) {
            return item.value;
        });
        var filterDiv = $(this).closest("div").attr("id");
        if (filterDiv.substring(0, 6) == 'DEMO_DC') {
            var tblDiv = 'tbl' + filterDiv.replace("_filters", "");
            DEMO_DP_RecalculateTable(tblDiv);
        }
    })

})


function DEMO_DP_CaseMultiply(element, ColumnForPack, ColumnForCost, ColumnForRetail, ColumnForDolSales, ColumnForDolProfits) {
    //alert(element.value);
    var CaseQty = stripNonRealDigits(element.value);
    //alert(CaseQty);
    var DolRetail = stripNonRealDigits(element.parentNode.parentNode.cells[ColumnForRetail].childNodes[0].data);
    var DolCost = stripNonRealDigits(element.parentNode.parentNode.cells[ColumnForCost].childNodes[0].data);
    var PackSize = stripNonRealDigits(element.parentNode.parentNode.cells[ColumnForPack].childNodes[0].data);
    //alert(DolRetail);
    //alert(DolCost);
    if (CaseQty == '') //|| (PackQty == '')
    { // wipe out the columns
        element.parentNode.parentNode.cells[ColumnForDolSales].innerHTML = '';
        element.parentNode.parentNode.cells[ColumnForDolProfits].innerHTML = '';
    }
    else {
        if (CaseQty == 0) {
            element.parentNode.parentNode.cells[ColumnForDolSales].innerHTML = '';
            element.parentNode.parentNode.cells[ColumnForDolProfits].innerHTML = '';
        }
        else {
            DolSales = (CaseQty * PackSize * DolRetail)
            DolProfits = (CaseQty * PackSize * (DolRetail - DolCost))
            var DolSalesFormatted = (Math.round(DolSales * 100) / 100);
            DolSalesFormatted = DolSalesFormatted.toFixed(2);
            //alert(DolSalesFormatted);
            var DolProfitsFormatted = (Math.round(DolProfits * 100) / 100);
            DolProfitsFormatted = DolProfitsFormatted.toFixed(2);
            //alert(DolProfitsFormatted);
            element.parentNode.parentNode.cells[ColumnForDolSales].innerHTML = DolSalesFormatted;
            element.parentNode.parentNode.cells[ColumnForDolProfits].innerHTML = DolProfitsFormatted;
        }
    }
//alert(element.parentNode.parentNode.parentNode.parentNode.id);
    var TableName = element.parentNode.parentNode.parentNode.parentNode.id
    DEMO_DP_RecalculateTable(TableName);
//alert(TableName);
}


function DEMO_DP_RecalculateTable(TableDivId) {

    var CasesTarget = 6 //cell that holds the case qty textbox
    var SRPTarget = 5 //Cell that holds the SRP textbox
    var CostTarget = 11 //Cell that contains the value
    var DollarSalesTarget = 7 //cell with items dollar sales value
    var PackSizeTarget = 4 //cell with pack size
    var GrossProfitTarget = 8 //cell with Gross Profit Percent in the cell line


    var TotalTotalUnitsTarget = TableDivId + '_tdItems'
    var TotalCasesTarget = TableDivId + '_tdCases'
    var AverageProfitTarget = TableDivId + '_wm_netValue'
    var TotalSalesTarget = TableDivId + '_tdSales'

    var TotalTotalUnits = 0
    var TotalCases = 0
    var TotalCost = 0
    var TotalDollarSales = parseFloat('0.00')
    var AverageProfit = 0

//alert(TableDivId);
//alert (document.getElementById(TableDivId).innerHTML);
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;
//alert(TableRows);
//TableRows = 5 //hardcode a stop

    var thisRow = 0

    while (thisRow < TableRows) {
        //alert(thisRow);
        var CaseRowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].getElementsByTagName('tr')[thisRow];
//	alert(CaseRowElement.className)
        if (!$(CaseRowElement).hasClass('hidden')) {
            if (stripNonRealDigits(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[CasesTarget].childNodes[0].value) != '') {
                //Because there is a Cases value, we can calculate TotalSales & TotalProfits
                thisCases = (Math.round(stripNonRealDigits(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[CasesTarget].childNodes[0].value)));
                thisPackSize = Number(stripNonRealDigits(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[PackSizeTarget].childNodes[0].data))
                thisTotalUnits = (Math.round(thisPackSize * thisCases));
                thisCost = Number(stripNonRealDigits(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[CostTarget].childNodes[0].data))
                thisSRP = Number(stripNonRealDigits((document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[SRPTarget].childNodes[0].data)))

                thisDolSales = (thisTotalUnits * thisSRP)
                thisDolProfits = (thisTotalUnits * (thisSRP - thisCost))
                var DolSalesFormatted = (Math.round(thisDolSales * 100) / 100);
                DolSalesFormatted = DolSalesFormatted.toFixed(2);
                var DolProfitsFormatted = (Math.round(thisDolProfits * 100) / 100);
                DolProfitsFormatted = DolProfitsFormatted.toFixed(2);
                document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[DollarSalesTarget].innerHTML = DolSalesFormatted;
                document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[GrossProfitTarget].innerHTML = DolProfitsFormatted;

                //These are for the totals at the bottom of the page!
                TotalCases = TotalCases + thisCases;
                TotalCost = TotalCost + (thisCost * thisTotalUnits);
                TotalTotalUnits = TotalTotalUnits + thisTotalUnits;
                TotalDollarSales = TotalDollarSales + (thisSRP * thisTotalUnits);
            }
            else {
                //Because there is NOT a Cases value, wipe out TotalUnitsTarget on this row.
                document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[DollarSalesTarget].innerHTML = '';
                document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[GrossProfitTarget].innerHTML = '';
            }
        }
        thisRow++;
    }


//Now to format these
    TotalTotalUnitsFormatted = Math.round(TotalTotalUnits);
    var TotalCasesFormatted = Math.round(TotalCases);

    if (TotalCost != 0) {
        AverageProfit = ((TotalDollarSales - TotalCost) / TotalCost * 100);
        AverageProfitFormatted = Math.round(AverageProfit) + '%';
    }
    else {
        AverageProfitFormatted = '0%';
    }

    var TotalDollarSalesFormatted = addCommas(Math.round(TotalDollarSales));

// Now output them to the Divs
    document.getElementById(TotalTotalUnitsTarget).innerHTML = TotalTotalUnitsFormatted;
    document.getElementById(TotalCasesTarget).innerHTML = TotalCasesFormatted;
    document.getElementById(AverageProfitTarget).innerHTML = AverageProfitFormatted;
    document.getElementById(TotalSalesTarget).innerHTML = TotalDollarSalesFormatted;

}

function PopUpWithFadeIn(image, MarginLeft, MarginTop, DivHeight, DivWidth) {
    if (image == null) {
        image = '<p>No image found</p>'
    } 		//in case there is no image source code entered in the index.html

    // Creates the div
    var msg = "<div id='pop' class='parentDisable' style='display: block;'>" +
        "<div id='popup' style='margin-left:" + MarginLeft + "px; margin-top:" + MarginTop + "px; height: " + DivHeight + "px; width: " + DivWidth + "px; display: none;'>" +
        "<a href='#' id='close' class='end'>" +
        "<img src='images/close.png'>" +
        "</a>" +
        image +
        "</div>" +
        "</div>" + "";

    var elNote = document.createElement('div');         // Creates a new element
    elNote.setAttribute('id', 'newDiv');                // Adds an id of newDiv
    elNote.innerHTML = msg;                             // Adds the message
    document.body.appendChild(elNote);				    // Adds it to the page

    var div = document.getElementById('pop');			//Creates variable div for calling 'pop' for the newly created div (above)
    var step = 0;
    steps = 10;
    speed = 60;
    fade = 0.0;
    color = 0.7; //variables for adjusting amount of steps and the darkness/transparency level of the background

    //alert('FadeInPOP(step = '+step+',steps='+steps+',speed='+speed+',fade='+fade+',color = '+color+',div='+div);

    FadeInPOP(div, step, steps, speed, fade, color);  //function call

    function FadeInPOP(div, step, steps, speed, fade, color) { //begin function
        fade += color / steps; 							 //fade will adjust by dividing color with steps
        div.style.background = 'rgba(0,0,0,' + fade + ')';//adjusts the 'style' for the fade in
        //alert(steps);
        if (step < steps) { //evaluates the steps
            step += 1;
            setTimeout(function () {
                FadeInPOP(div, step, steps, speed, fade, color)
            }, 1000 / speed);
        }  //Timer to slow down or speed up via speed variable
        else {   //execute last in order to make the div with image appear
            document.getElementById('popup').style.display = 'block';
        }  //makes the div with image appear
    }

    function dismissNote() {                          	  // Declare function
        document.body.removeChild(elNote);              // Remove the note
    }

    var elClose = document.getElementById('close');   		   // Get the close button
    elClose.addEventListener('click', dismissNote, false); // Click close-clear note
}


function Calculate() {
    var UnitsPerCase = document.getElementById('UnitsPerCase').value;
    var ProjectedCasesSold = document.getElementById('ProjectedCasesSold').value;
    var NewProjectedCasesSold = document.getElementById('NewProjectedCasesSold').value;

    var CaseCost = document.getElementById('CaseCost').value;
    //var CaseCostLength = CaseCost.length;
    //CaseCost = CaseCost.substring(1, CaseCostLength);

    var NewCaseCost = document.getElementById('NewCaseCost').value;
    //var NewCaseCostLength = NewCaseCost.length;
    //NewCaseCost = NewCaseCost.substring(1, NewCaseCostLength);

    var UnitSellPrice = document.getElementById('UnitSellPrice').value;
    //var UnitSellPriceLength = UnitSellPrice.length;
   // UnitSellPrice = UnitSellPrice.substring(1, UnitSellPriceLength);

    var NewUnitSellPrice = document.getElementById('NewUnitSellPrice').value;
    //var NewUnitSellPriceLength = NewUnitSellPrice.length;
    //NewUnitSellPrice = NewUnitSellPrice.substring(1, NewUnitSellPriceLength);

    // BEGIN PROFIT CALCULATIONS
    var UnitCost = (CaseCost / UnitsPerCase).toFixed(2);
    if ((UnitCost != "NaN.00") && (UnitCost != "0.00")) {
        document.getElementById('UnitCost').textContent = ("$" + UnitCost);
    }

    var CaseProfit = ((UnitSellPrice * UnitsPerCase) - CaseCost).toFixed(2);
    if ((CaseProfit != "NaN.00") && (CaseProfit != "0.00")) {
        document.getElementById('CaseProfit').textContent = ("$" + CaseProfit);
    }

    var UnitProfit = (UnitSellPrice - UnitCost).toFixed(2);
    if ((UnitProfit != "NaN.00") && (UnitProfit != "0.00")) {
        document.getElementById('UnitProfit').textContent = ("$" + UnitProfit);
    }

    var revenue = (UnitSellPrice * UnitsPerCase);
    var ProfitMargin = (((revenue - CaseCost) / revenue) * 100).toFixed(2);
    if ((ProfitMargin != "NaN.00") && (ProfitMargin != "-Infinity.00") && (ProfitMargin != "Infinity.00") && (ProfitMargin != "0.00")) {
        document.getElementById('ProfitMargin').textContent = (ProfitMargin + "%");
    }

    var Markup = (revenue - CaseCost);
    Markup = ((Markup / CaseCost) * 100).toFixed(2);
    if ((Markup != "NaN.00") && (Markup != "0.00")) {
        document.getElementById('Markup').textContent = (Markup + "%");
    }
    // END PROFIT CALCULATIONS

    // BEGIN NEW PROFIT CALCULATIONS
    var newUnitCost = (NewCaseCost / UnitsPerCase).toFixed(2);
    if ((newUnitCost != "NaN.00") && (newUnitCost != "0.00")) {
        document.getElementById('newUnitCost').textContent = ("$" + newUnitCost);
    }

    var newCaseProfit = ((NewUnitSellPrice * UnitsPerCase) - NewCaseCost).toFixed(2);
    if ((newCaseProfit != "NaN.00") && (newCaseProfit != "0.00")) {
        document.getElementById('newCaseProfit').textContent = ("$" + newCaseProfit);
    }

    var newUnitProfit = (NewUnitSellPrice - newUnitCost).toFixed(2);
    if ((newUnitProfit != "NaN.00") && (newUnitProfit != "0.00")) {
        document.getElementById('newUnitProfit').textContent = ("$" + newUnitProfit);
    }

    var newRevenue = (NewUnitSellPrice * UnitsPerCase);
    var newProfitMargin = (((newRevenue - NewCaseCost) / newRevenue) * 100).toFixed(2);
    if ((newProfitMargin != "NaN.00") && (newProfitMargin != "-Infinity.00") && (newProfitMargin != "Infinity.00") && (newProfitMargin != "0.00")) {
        document.getElementById('newProfitMargin').textContent = (newProfitMargin + "%");
    }

    var newMarkup = (newRevenue - NewCaseCost);
    newMarkup = ((newMarkup / NewCaseCost) * 100).toFixed(2);
    if ((newMarkup != "NaN.00") && (newMarkup != "0.00")) {
        document.getElementById('newMarkup').textContent = (newMarkup + "%");
    }

    var BreakEvenLift = ((CaseProfit * ProjectedCasesSold) / newCaseProfit).toFixed(1);
    if ((BreakEvenLift != "NaN.0") && (BreakEvenLift != "-Infinity.0") && (BreakEvenLift != "Infinity.0") && (BreakEvenLift != "0.0")) {
        document.getElementById('BreakEvenLift').textContent = (BreakEvenLift + "");
    }
    // END NEW PROFIT CALCULATIONS

    // BEGIN SALES INCREASE
    var ProjectedCases = (UnitsPerCase * ProjectedCasesSold) * UnitSellPrice;
    var NewProjectedCases = (UnitsPerCase * NewProjectedCasesSold) * NewUnitSellPrice;
    var SalesIncrease = (((NewProjectedCases - ProjectedCases) / ProjectedCases) * 100).toFixed(2);
    if ((SalesIncrease != "Infinity.00") && (SalesIncrease != "-Infinity.00") && (SalesIncrease != "NaN.00") && (SalesIncrease != "0.00")) {
        document.getElementById('NewSalesIncreaseTd').textContent = (SalesIncrease) + "%";
    }
    // END SALES INCREASE
}

document.getElementById('calculate').addEventListener('click', Calculate, false);

//TODO:Need to make the this color function be generic, be able to not color all tables, have specific way to color only one
//TODO:Just quick fix to get the pitch released.
//EvenListener for Gaptracker that grey out the row that was clicked on
document.addEventListener("change", function (event) {
    //get the element that was clicked on
    var el = $(event.target);
    //holds the table id
    var tableID = el.parent().parent().parent().parent();
    //get the TR that contains the checkbox
    var tableRow = el.closest("tr");
    if (el.attr("checked") == "checked" &&
        (el.attr("storagekeyprefix") == "tblMWM_SST_All10" || el.attr("storagekeyprefix") == "tblMWM_SST_All11" ||
         el.attr("storagekeyprefix") == "tblMWM_SST_All12" || el.attr("storagekeyprefix") == "tblMWM_SST_All13"  ||
         el.attr("storagekeyprefix") == "tblMWM_SST_All14")) {
        tableRow.addClass("complete");
    }
    else {
        tableRow.removeClass("complete");
    }

});

function CumulativeAddRecalculateTable(TableDivId){
//alert(TableDivId);
var AddTarget=5 //Cell that holds add checkbox hardcoded
var ValueTarget1=3 //Cell that contains the value
var ValueTarget2=4 //Cell that contains the second value


var AddValueTarget=TableDivId+'_AddValue'
var NetValueTarget=TableDivId+'_NetValue'

var AddValue=0
var SubtractValue=0
var ImpactValue=0
var NetValue=0

//alert(TableDivId);
//alert (document.getElementById(TableDivId).innerHTML);
var TableRows=document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;
//alert(TableRows);

var thisRow=0

while (thisRow<TableRows)
   {
   //alert(thisRow);
	var CaseRowElement=document.getElementById(TableDivId).getElementsByTagName('tbody')[0].getElementsByTagName('tr')[thisRow];
	//alert(CaseRowElement.className)
	if (!$(CaseRowElement).hasClass('hidden')) {
		//if (document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[ValueTarget1] != null)
		var rowElement=document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow]
		if ($(rowElement.children[AddTarget].childNodes[0]).hasClass('checked'))
		   {
		   //var rowElement=document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow]
		   var targetValue1=parseFloat(stripNonRealDigits(rowElement.children[ValueTarget1].innerHTML));
		  // alert(targetValue1);
		   var targetValue2=parseFloat(stripNonRealDigits(rowElement.children[ValueTarget2].innerHTML));
		   //alert(targetValue2);
		   var targetValue=(targetValue1+targetValue2);
		   var targetValueRounded=Math.round(targetValue*Math.pow(10,2))/Math.pow(10,2);
		   var targetValueRounded=targetValueRounded.toFixed(2);
		   //alert (stripNonRealDigits(rowElement.children[ValueTarget].innerHTML))
			if ($(rowElement.children[AddTarget].childNodes[0]).hasClass('checked'))
			{
			//alert ('add it');
			AddValue=AddValue+targetValue;
			//alert(AddValue);
			}
			//if ($(rowElement.children[SubtractTarget].childNodes[0]).hasClass('checked'))
			//{
			//alert ('subtract it');
			//SubtractValue=SubtractValue+targetValue;
			//alert(SubtractValue);
			//}
		   }
	}
   thisRow++;
   }
NetValue=(AddValue*0.31);
//alert(AddValue);
//alert(NetValue);



//Now to format these
var AddValueFormatted='$'+Math.round(AddValue);
//alert(AddValueFormatted);
var NetValueFormatted='$'+Math.round(NetValue);
//alert(NetValueFormatted);


// Now output them to the Divs
document.getElementById(AddValueTarget).innerHTML=AddValueFormatted;
document.getElementById(NetValueTarget).innerHTML=NetValueFormatted;
}



/*Par Calc Fills in the zero at the total on page load*/
$(document).ready(function () {
    document.getElementById('divPCSummaryPacCalcTotal').innerHTML = 0;
});
/*INPUT: Your store input, current value, point value and max point value
 *
 * Description: The function use the input values to do calculations and based on the calculation,
 * we output to the current value column.
 *
 * OUTPUT: outputs the calculated value into current and updates the div with total
 * */
function FindMax(max) {

    var myMax = stripNonRealDigits(max.substring(max.indexOf("Max")));

    return myMax;
}

function ParCalc(element, ColumnForCurrentValue, ColumnForPointValue, ColumnForMaxPointValue) {

    /*Init and populate the variables with data based on the row it was clicked on.*/
    var YourStore = stripNonRealDigits(element.value);
    var curentValueCell = null;


    var PointValue = stripNonRealDigits(element.parentNode.parentNode.cells[ColumnForPointValue].childNodes[0].data);


    var MaxPointValue = FindMax(element.parentNode.parentNode.cells[ColumnForMaxPointValue].childNodes[0].data);

    //temp value for current value
    var temp = YourStore * PointValue;


    if (temp <= MaxPointValue && temp > 0) {
        curentValueCell = temp;
    }
    else if (YourStore == 0) {
        curentValueCell = null;
    }
    else {
        curentValueCell = MaxPointValue;
    }

    /*Ouputs the calc value into the cell*/
    element.parentNode.parentNode.cells[ColumnForCurrentValue].innerHTML = curentValueCell;


    //clear the variable after each run
    temp = null;
    curentValueCell = null;

    /*Get the table ID so i can work with it in PacCalcTotal Function*/
    var TableDivId = element.parentNode.parentNode.parentNode.parentNode.id;
    ParCalcTotal(TableDivId);

}

/*Function calculates the total for Par Calc*/
function ParCalcTotal(TableDivId) {

    /*Get how many table rows we have so i can use this to loop through each one in the table */
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;

    /*Init values that will use for calc*/
    var cellValue = 0;
    var total = 0;

    var thisRow = 0;
    /*Loop through the whole table, if there is value in Current Value column, add it to total else skip that cell*/
    while (thisRow < TableRows) {
        if (document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[5].innerHTML != "") {

            /*Gets the value of Current Value from each row of the table*/
            cellValue = parseInt(document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows[thisRow].cells[5].innerHTML);

            /*Holds the total for the table*/
            total = total + cellValue;
        }

        thisRow++;
    }
    /*Output the total of the table after it has done running*/
    document.getElementById('divPCSummaryPacCalcTotal').innerHTML = total;
}


function populate_table_overload(TableDivId) {
    //alert(TableDivId.substring(0,10));
    if (TableDivId.substring(0, 19) == 'tblENR_POS_Overview') {
        POSArrows(TableDivId);
    };
};


function POSArrows(TableDivId) {
    var StrValueTarget = 2 //Cell that contains the market percentage
    var MktValueTarget = 3 //Cell that contains the market percentage
    var ImageTarget = 4 //Cell that contains the image
    var upArrow = '<img height="30" src="images/arrow_green.png">';
    var downArrow = '<img height="30" src="images/arrows_red.png">';
    var equalSign = '<img height="30" src="images/equal-sign.jpg">';

    //alert(TableDivId);
    //alert (document.getElementById(TableDivId));
    //alert (document.getElementById(TableDivId).innerHTML);
    var TableRows = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].rows.length;
    //alert(TableRows); 
    var thisRow = 0

    while (thisRow < TableRows) {
        //alert(thisRow);
        if ((document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[StrValueTarget] != null) && (document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow].children[MktValueTarget] != null)) {
            var rowElement = document.getElementById(TableDivId).getElementsByTagName('tbody')[0].children[thisRow]
            var StrTargetValue = rowElement.children[StrValueTarget].innerHTML;
            var MktTargetValue = rowElement.children[MktValueTarget].innerHTML;
            //alert(StrTargetValue);
            //alert(MktTargetValue);
            var StrValue = parseFloat(StrTargetValue.replace("%", ""));
            var MktValue = parseFloat(MktTargetValue.replace("%", ""));
            if ((typeof (MktValue) == 'number') && (typeof (StrValue) == 'number')) {
                if (StrValue < MktValue) {
                    rowElement.children[ImageTarget].innerHTML = downArrow;
                } else if (StrValue > MktValue) {
                    rowElement.children[ImageTarget].innerHTML = upArrow;
                } else {
                    rowElement.children[ImageTarget].innerHTML = equalSign;
                };
            };
        };
        thisRow++;
    }
};

