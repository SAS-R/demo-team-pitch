﻿function populate_table($table, json_objects, match_value) {
    var start = new Date();
    var rows, $col, $row, $cells, $cell, $columnsHeader, value, arrResults, match_color;
    $table.find('tbody > tr').remove();
    $columnsHeader = $table.find('tr.header > th');
    rows = [];

    /*match_color = $table.attr('style');*/
   match_color = $table.attr('matchRowColor');
    if (match_color == '')
        match_color == null;

    var tableWidth = 0;
    var columnArray = [];
    $columnsHeader.each(function (index) {
        $col = $(this);
        var colObject = {};

        colObject.field = $col.attr('field');
        colObject.filter_attribute = $col.attr('filterattribute');
        colObject.column_color = $col.attr('columncolor');
        colObject.format_mask = $col.attr('formatmask');
        colObject.row_content = $col.attr('rowcontent');
        colObject.LockedHeadersPaddingDifference = $col.attr('LockedHeadersPaddingDifference');
        colObject.width = $col.width();
        colObject.$column = $col;
        //added attr
        colObject.columnClass = $col.attr('class');

        columnArray.push(colObject);
    });
    var row;
    var widths = $table.attr('widths');
    if (widths != null)
        widths = $table.attr('widths').split(',');

    $.map(json_objects, function (item) {

            row = [];
            row.push('<tr>');
            $.map(columnArray, function (colObject, index) {
                try {
                    cellStyles = [];
                    cell = [];
                    cell.push('<td');
                    cellStyles.push('style="')

                    var field = colObject.field;
                    var filter_attribute = colObject.filter_attribute;
                    var column_color = colObject.column_color;
                    var format_mask = colObject.format_mask;
                    var row_content = colObject.row_content;
                    var LockedHeadersPaddingDifference = parseInt(colObject.LockedHeadersPaddingDifference);

                    if (widths != null) {
                        colObject.$column.width(widths[index]);
                    }

                    if (isNaN(LockedHeadersPaddingDifference)) {
                        cellStyles.push('width:' + (colObject.$column.width()) + 'px;');
                        cellStyles.push('min-width:' + (colObject.$column.width()) + 'px;');
                        cellStyles.push('max-width:' + (colObject.$column.width()) + 'px;');
                    }
                    else {
                        cellStyles.push('width:' + (colObject.$column.width() + LockedHeadersPaddingDifference) + 'px;');
                        cellStyles.push('min-width:' + (colObject.$column.width() + LockedHeadersPaddingDifference) + 'px;');
                        cellStyles.push('max-width:' + (colObject.$column.width() + LockedHeadersPaddingDifference) + 'px;');
                    }

                    if (rows.length == 0)
                        tableWidth += colObject.$column.width();

                    if (field != null) {
                        value = eval_with_this(field, item);
                        change_color(value, cellStyles, colObject, row);
                        if (format_mask != null)
                            value = $.formatNumber(value, {format: colObject.format_mask});

                        cell.push('field="' + field + '"');
                    }
                    else {
                        value = '';
                        field = '';
                    }
                    if (filter_attribute != null)
                        cell.push(filter_attribute + '=""'); //$cell.attr(filterattribute,'');

                    if (match_value != null && match_value == item.MatchCondition && match_color != null)
                        cellStyles.push('background-color:' + match_color + ';');//$cell.css('background-color', match_color);
                    else if (column_color != null)
                        cellStyles.push('background-color:' + column_color + ';'); //$cell.css('background-color', column_color);

                    //append the cell details if there are row_content from the header
                    if (row_content != null) {
                        value = value + row_content;
                    }

                    cellStyles.push('"');
                    cell.push(cellStyles.join(' '));
                    cell.push('>' + value + '</td>');
                    row.push(cell.join(' '));
                }
                catch
                    (ex) {
                }
            });
            //if (rows.length<20){
            row.push('</tr>');
            rows.push(row.join(' '));
            //}
        }
    )
    ;

    showTimeDiff(start, 'populated rows');
    var skipwidth = $table.attr('skipwidthcalculation')
    if (!skipwidth)
        $table.width(tableWidth);

    $table.find('tbody').append(rows);
//find all values with storagekeyprefix,
    var $elementsWithStoragesKeys = $table.find('*[storagekeyprefix]')
    $elementsWithStoragesKeys.each(function (index) {
        var $item = $(this);
        //append an unique index behind the key prefix make sure everything is unique.
        $item.attr('storagekeyprefix', $item.attr('storagekeyprefix') + '' + index);
        //load from the storage and see if we have any OLD values defined and restore them into the checkbox.
        var value = load_storage($item.attr('storagekeyprefix'));
        if ($item.is('[type=checkbox]')) {
            $item.attr('checked', value ? (value.toLowerCase() == 'true' || value === true) : false);
        } else if ($item.is('[type=textbox]')) {
            $item.attr('value', value);
        } else if ($item.hasClass('checkboximage')) {
            $item.toggleClass('checked', value ? (value.toLowerCase() == 'true' || value === true) : false);
        }
    })
    $table.on('change', 'input[storagekeyprefix]', function () {
        var $item = $(this);
        //add check change against the checkbox for now.
        if ($item.is('[type=checkbox]')) {
            $item.attr('checked', $item.is(':checked'));
            save_storage($item.attr('storagekeyprefix'), $item.is(':checked'))
        } else if ($item.is('[type=textbox]')) {//enable textbox.
            $item.attr('value', $item.val());
            save_storage($item.attr('storagekeyprefix'), $item.val())
        }
    });
    if (typeof populate_table_overload == 'function') {
        //alert($table[0].id);
        populate_table_overload($table[0].id);
    }
    ;


    //Colors the previous row of the table
    colorPreviousRow($table);
    //Get row count of all rows in the table
    rowCount(rows,$table);
}

//Color previous cells in the table.
//INPUT: $table
function colorPreviousRow($table) {

    //Find all the TD in the table
    var allTD = $table.find('tbody > tr > td');
    //Initialize color variable.
    var colors = "";
    //Loop through each TD, find its color,
    //color previous cell that color.
    for (var i = 0; i < allTD.length; i++) {
        //Holds one TD at a time.
        colors = allTD[i];
        if (colors.style.backgroundColor == "red") {
            allTD[i - 1].style.backgroundColor = "red";
            allTD[i - 1].style.color = "black";
        }
        else if (colors.style.backgroundColor == "yellow") {
            allTD[i - 1].style.backgroundColor = "yellow";
            allTD[i - 1].style.color = "black";
        }
        else if (colors.style.backgroundColor == "green") {
            allTD[i - 1].style.backgroundColor = "green";
            allTD[i - 1].style.color = "black";
        }
        //Reset color variable
        colors = "";
    }
}

//Gets the row count for the table
//INPUT: $table, rows
function rowCount(rows,$table) {
    //Get the row count.
    var count = rows.length;
    //Get the table ID
    var tableID = $table.attr("id");
    //Get the class of rowCountOn
    var rowCountOn = $table.attr("class");
    //Add the "Number of Gaps" to the table
    //Checks if the tableID is not null and if the rowCountOn
    //is added to the table.
    if(tableID != null && rowCountOn.indexOf("rowCountOn") != -1)
    {
        //Displays the row count of the table.
        $('#'+tableID).before('<div id="rowCountStyle">Number of Gaps: '+ count + '</div>' );
    }
}

//Colors the text and columns of the tables
function change_color(value, cellStyles, colObject) {

    //colors the values text that are less then zero red
    if (value == null || cellStyles == null) return false;
    if (value < 0) {
        cellStyles.push('color:#FF0000;');
    }
    /*
     *Colors the columns if they have the class "colorColumn".
     */
    //Count of how many class in the table have "colorColumn"
    var count = document.getElementsByClassName("colorColumn");
    //initialize the variable.
    var colorColumnName = "";

    for (var i = 0; i < count.length; i++) {

        //gets the field value that has the class "colorColumn"
        colorColumnName += document.getElementsByClassName("colorColumn")[i].getAttribute("field");

        //colors the rows based on the cell values and makes the text color black.
        if (colObject.field.toString() == colorColumnName && colObject.columnClass != "") {
            if (value <= 0) {
                cellStyles.push('background-color: red;');
                cellStyles.push('color:black');
            } else if (value >= 1 && value <= 7) {
                cellStyles.push('background-color: yellow;');
                cellStyles.push('color:black');
            }
            else if (value > 7) {
                cellStyles.push('background-color:green;');
                cellStyles.push('color:black');
            }
        }

        //reset the variable
        colorColumnName = "";
    }
}


function display_barcode($tr) {
    $('div#OrderCode').text('Shelf Code: ' + $tr.find('td[field="ShelfCode"]').text());
    UPCA.ShowBarcode($('div#UPCCode'), $tr.find('td[field="UPC"]').text());
    $('div#Product').text($tr.find('td[field="Product"]').text());
    $('.selected-info').width($('#tblSalesData').width());
}

function changeRadioGraphFilter($radioContainer, item, all_data) {
    var $graphContainer = $('#' + $radioContainer.attr('filtertarget'));
    var graphData = eval_with_this($graphContainer.attr('graphdata'), all_data);
    var max_date;
    for (var x in graphData) {
        var record = graphData[x];
        var daysDiff, weekDiff;
        if (record.WeekNum == 52) {
            max_date = record.WeekEnding;
            daysDiff = Date.daysBetween(Date.parse(max_date), Date.today().moveToDayOfWeek(0, -1));
            weekDiff = parseInt(daysDiff / 7);
            break;
        }
    }
    if (weekDiff == null || weekDiff == 0)
        item.Value = 'WeekNum<13';
    else
        item.Value = '(WeekNum>=' + (1 + weekDiff) + ' && WeekNum<' + (13 + weekDiff) + ')';

    return item;
}

Date.daysBetween = function (date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
}

function filterSelectedRow(arrString, $row) {
    var selectedOnly = arrString[0] == 'Selected';
    var hasChecked = $row.find('div.checkboximage.checked,input:checked').length > 0;
    $row.find('div.checkboximage,input:checkbox').parent().toggleClass('hidden', !hasChecked && selectedOnly);
}
