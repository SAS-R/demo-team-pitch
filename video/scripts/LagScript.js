﻿function OrderTotal(userValue, userKey) {
    var dataSource = pitch_data['DEMO_LAG_WM1'];
    userValue = stripNonRealDigits(userValue.value);
    setValue(dataSource, userValue, userKey);
    getTotal(dataSource);
}

function setValue(dataSource, userValue, userKey) {
    if (dataSource != null) {
        $(dataSource).each(function (index, element) {
            if (element.key == userKey) {
                element.value = userValue;
            }
        });
    }
}


function getTotal(dataSource) {
    var temp, retailTotal = 0, profitTotal = 0;
    $(dataSource).each(function (x) {
        temp = getInteger(stripNonRealDigits((dataSource[x].value)) * stripNonRealDigits((dataSource[x].retail)));
        retailTotal += temp;
        temp = getInteger(stripNonRealDigits((dataSource[x].value)) * stripNonRealDigits((dataSource[x].profit)));
        profitTotal += temp;
    });
    PrintTotal(retailTotal, profitTotal);
}

function getInteger(number) {
    if (number == "") {
        return 0;
    } else {
        return parseInt(number);
    }
}

function PrintTotal(totalRetailValue, totalProfit) {
    if (totalRetailValue != 0 && totalProfit != 0) {
        $('#TotalRetailValue').html(currencyFormat(totalRetailValue));
        $('#TotalProfit').html(currencyFormat(totalProfit));
    }
    else {
        $('#TotalRetailValue').html('');
        $('#TotalProfit').html('');
    }
}

function currencyFormat(number) {
    return "$" + number.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}