var current_presentation_id = null;
var crm_allow_save_data = false;
var crm_current_call_key = '';
var crm_current_presentation_key = '';
var pitch_data;
(function () {
    var testing = ( location.search == '');
    var include = function (file) {
        var tag = '<script src="' + file + '"></script>';
        console.log('writing script tag: ' + tag);
        document.write(tag);
    };

    function testingStartup() {

        var data = {

            Demo_DisplayDist: [{ "ASMID": 86, "Description": "PURE HAIRCARE CLASSIC SHAMPOO 15OZ DISPLAYABLE CASE", "ShipDate": "8/9-8/15", "Qty": 1 }, { "ASMID": 86, "Description": "PURE HAIRCARE CLASSIC CONDITIONER 15OZ DISPLAYABLE CASE", "ShipDate": "8/9-8/15", "Qty": 1 }, { "ASMID": 86, "Description": "PURE HAIRCARE SHINE SPRAY 7OZ PALLET 393 CT", "ShipDate": "9/6-9/12", "Qty": 1 }, { "ASMID": 86, "Description": "HAIRSPRAY 6.2OZ BULLET CLIP STRIP", "ShipDate": "9/6-9/12", "Qty": 1 }, { "ASMID": 86, "Description": "KERATIN SHAMPOO 6OZ DISPLAYABLE CASE", "ShipDate": "9/6-9/12", "Qty": 1 }, { "ASMID": 86, "Description": "KERATIN CONDITIONER 6OZ DISPLAY MODULE", "ShipDate": "9/6-9/12", "Qty": 1 }, { "ASMID": 86, "Description": "ORGANIC COCONUT SHAMPOO 12OZ 4 TIER RAC", "ShipDate": "10/8-10/14", "Qty": 1 }],

            Demo_PAR_HD_All1: [{ "Priority": 1, "MerchandisingOptions": "Zero Cases in the backroom", "PointValue": 5, "MaxPointValue": "5" }, { "Priority": 2, "MerchandisingOptions": "All Facings Stocked", "PointValue": 5, "MaxPointValue": "5" }, { "Priority": 3, "MerchandisingOptions": "In Aisle products set to POG & tagged", "PointValue": 3, "MaxPointValue": "3" }, { "Priority": 4, "MerchandisingOptions": "Hairbands or Headbands stocked in Speedy Checklanes", "PointValue": 3, "MaxPointValue": "3" }, { "Priority": 5, "MerchandisingOptions": "Sample product sizes stocked in Checklanes", "PointValue": 3, "MaxPointValue": "3" }, { "Priority": 6, "MerchandisingOptions": "Shared Sidecap on Action Alley tagged/stocked", "PointValue": 3, "MaxPointValue": "2pts ea/Max6" }, { "Priority": 7, "MerchandisingOptions": "Exclusive Sidekicks on Action Alley tagged/stocked", "PointValue": 2, "MaxPointValue": "2pts ea/Max6" }, { "Priority": 8, "MerchandisingOptions": "New Keratin products on shelf", "PointValue": 2, "MaxPointValue": "3" }, { "Priority": 8, "MerchandisingOptions": "Incremental display up & tagged/stocked in Beauty section", "PointValue": 3, "MaxPointValue": "Max 10" }, { "Priority": 10, "MerchandisingOptions": "Clipstrips up in target aisles", "PointValue": 2, "MaxPointValue": "Max 6" }],

            
            DEMO_LAG_WM1: [{ "key": "4 Tier Haircare Rack", "value": "", "retail": "$202.98", "profit": "$117.64" }, { "key": "Classic/Keratin Pallet", "value": "", "retail": "$1,557.88", "profit": "$863.60" }, { "key": "Pure Sidekick", "value": "", "retail": "$179.64", "profit": "$95.88" }],


            ENR_POSCC_Lowes1_Chart:[{"Col1":"Yr","Col2":"Sept","Col3":"Oct","Col4":"Nov","Col5":"Dec","Col6":"Jan","Col7":"Feb","Col8":"YTD","Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":"1"},{"Col1":"Yr","Col2":"Mar","Col3":"Apr","Col4":"May","Col5":"Jun","Col6":"Jul","Col7":"Aug","Col8":"YTD","Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":"1"},{"Col1":"Yr","Col2":"Sept","Col3":"Oct","Col4":"Nov","Col5":"Dec","Col6":"Jan","Col7":"Feb","Col8":"YTD","Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":"1"},{"Col1":"Yr","Col2":"Mar","Col3":"Apr","Col4":"May","Col5":"Jun","Col6":"Jul","Col7":"Aug","Col8":"YTD","Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":"1"},{"Col1":"Current Yr","Col2":"632","Col3":"616","Col4":"382","Col5":"766","Col6":"433","Col7":"0","Col8":"2829","Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":"2"},{"Col1":"Current Yr","Col2":"0","Col3":"0","Col4":"0","Col5":"0","Col6":"0","Col7":"0","Col8":"0","Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":"2"},{"Col1":"Current Yr","Col2":"4862","Col3":"3299","Col4":"3295","Col5":"3998","Col6":"2375","Col7":"0","Col8":"17829","Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":"2"},{"Col1":"Current Yr","Col2":"0","Col3":"0","Col4":"0","Col5":"0","Col6":"0","Col7":"0","Col8":"0","Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":"2"},{"Col1":"Prior Yr","Col2":"0","Col3":"4274","Col4":"6361","Col5":"7362","Col6":"3894","Col7":"566","Col8":"22457","Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":"3"},{"Col1":"Prior Yr","Col2":"3787","Col3":"3080","Col4":"3617","Col5":"4682","Col6":"3175","Col7":"3646","Col8":"21987","Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":"3"},{"Col1":"Prior Yr","Col2":"0","Col3":"1045","Col4":"860","Col5":"1328","Col6":"648","Col7":"86","Col8":"3967","Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":"3"},{"Col1":"Prior Yr","Col2":"573","Col3":"489","Col4":"620","Col5":"1004","Col6":"645","Col7":"664","Col8":"3995","Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":"3"},{"Col1":"%Chg","Col2":"0.1","Col3":"0.26","Col4":"-0.38","Col5":"-0.24","Col6":"-0.33","Col7":"-1","Col8":"-0.29","Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":"4"},{"Col1":"%Chg","Col2":"0","Col3":"-1","Col4":"-1","Col5":"-1","Col6":"-1","Col7":"-1","Col8":"-1","Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":"4"},{"Col1":"%Chg","Col2":"0.28","Col3":"0.07","Col4":"-0.09","Col5":"-0.15","Col6":"-0.25","Col7":"-1","Col8":"-0.19","Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":"4"},{"Col1":"%Chg","Col2":"0","Col3":"-1","Col4":"-1","Col5":"-1","Col6":"-1","Col7":"-1","Col8":"-1","Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":"4"}],

ENR_POSCC_Lowes1_TimeFrame_ugh:[{"Value":"(WeekNum == 'Mar-Aug')","Display":"Mar-Aug"},{"Value":"(WeekNum == 'Sept-Feb')","Display":"Sept-Feb"}],
ENR_POSCC_Lowes1_TimeFrame:[{"Value":"Mar-Aug","Display":"Mar-Aug"},{"Value":"Sept-Feb","Display":"Sept-Feb"}],
		//		data.PWS_Movement_Radio = [{"Value":"(WeekNum>40 && WeekNum<53)","Display":"Last 12 weeks"},{"Value":"(WeekNum>0 && WeekNum<13)","Display":"LY Perf 12 weeks out"}];

ENR_POSCC_Lowes1_Category:[{"Value":"Lighting Products","Display":"Lighting Products"},{"Value":"Premium Batteries","Display":"Premium Batteries"}],

ENR_POSCC_Lowes1:[{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":1,"WeekEnding":"07-Sep","Year1DollarValue":132.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"31"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":1,"WeekEnding":"09-Mar","Year1DollarValue":158.00,"Year1Label":"Prior Year","Year2DollarValue":99.00,"Year2Label":"Current Year","WeekNum":"5"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":1,"WeekEnding":"07-Sep","Year1DollarValue":1052.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"31"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":1,"WeekEnding":"09-Mar","Year1DollarValue":1367.00,"Year1Label":"Prior Year","Year2DollarValue":1083.00,"Year2Label":"Current Year","WeekNum":"5"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":2,"WeekEnding":"16-Mar","Year1DollarValue":61.00,"Year1Label":"Prior Year","Year2DollarValue":97.00,"Year2Label":"Current Year","WeekNum":"6"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":2,"WeekEnding":"14-Sep","Year1DollarValue":157.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"32"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":2,"WeekEnding":"16-Mar","Year1DollarValue":1041.00,"Year1Label":"Prior Year","Year2DollarValue":925.00,"Year2Label":"Current Year","WeekNum":"6"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":2,"WeekEnding":"14-Sep","Year1DollarValue":1099.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"32"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":3,"WeekEnding":"23-Mar","Year1DollarValue":199.00,"Year1Label":"Prior Year","Year2DollarValue":200.00,"Year2Label":"Current Year","WeekNum":"7"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":3,"WeekEnding":"21-Sep","Year1DollarValue":165.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"33"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":3,"WeekEnding":"23-Mar","Year1DollarValue":792.00,"Year1Label":"Prior Year","Year2DollarValue":1071.00,"Year2Label":"Current Year","WeekNum":"7"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":3,"WeekEnding":"21-Sep","Year1DollarValue":1300.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"33"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":4,"WeekEnding":"28-Sep","Year1DollarValue":159.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"34"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":4,"WeekEnding":"30-Mar","Year1DollarValue":62.00,"Year1Label":"Prior Year","Year2DollarValue":231.00,"Year2Label":"Current Year","WeekNum":"8"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":4,"WeekEnding":"28-Sep","Year1DollarValue":1078.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"34"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":4,"WeekEnding":"30-Mar","Year1DollarValue":1179.00,"Year1Label":"Prior Year","Year2DollarValue":889.00,"Year2Label":"Current Year","WeekNum":"8"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":5,"WeekEnding":"06-Apr","Year1DollarValue":154.00,"Year1Label":"Prior Year","Year2DollarValue":93.00,"Year2Label":"Current Year","WeekNum":"9"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":5,"WeekEnding":"05-Oct","Year1DollarValue":224.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"35"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":5,"WeekEnding":"06-Apr","Year1DollarValue":1051.00,"Year1Label":"Prior Year","Year2DollarValue":1430.00,"Year2Label":"Current Year","WeekNum":"9"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":5,"WeekEnding":"05-Oct","Year1DollarValue":1292.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"35"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":6,"WeekEnding":"12-Oct","Year1DollarValue":216.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"36"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":6,"WeekEnding":"13-Apr","Year1DollarValue":109.00,"Year1Label":"Prior Year","Year2DollarValue":226.00,"Year2Label":"Current Year","WeekNum":"10"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":6,"WeekEnding":"12-Oct","Year1DollarValue":1118.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"36"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":6,"WeekEnding":"13-Apr","Year1DollarValue":1190.00,"Year1Label":"Prior Year","Year2DollarValue":1047.00,"Year2Label":"Current Year","WeekNum":"10"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":7,"WeekEnding":"20-Apr","Year1DollarValue":85.00,"Year1Label":"Prior Year","Year2DollarValue":192.00,"Year2Label":"Current Year","WeekNum":"11"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":7,"WeekEnding":"19-Oct","Year1DollarValue":425.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"37"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":7,"WeekEnding":"20-Apr","Year1DollarValue":865.00,"Year1Label":"Prior Year","Year2DollarValue":972.00,"Year2Label":"Current Year","WeekNum":"11"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":7,"WeekEnding":"19-Oct","Year1DollarValue":1243.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"37"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":8,"WeekEnding":"26-Oct","Year1DollarValue":164.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"38"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":8,"WeekEnding":"27-Apr","Year1DollarValue":128.00,"Year1Label":"Prior Year","Year2DollarValue":148.00,"Year2Label":"Current Year","WeekNum":"12"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":8,"WeekEnding":"26-Oct","Year1DollarValue":1174.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"38"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":8,"WeekEnding":"27-Apr","Year1DollarValue":1190.00,"Year1Label":"Prior Year","Year2DollarValue":1136.00,"Year2Label":"Current Year","WeekNum":"12"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":9,"WeekEnding":"04-May","Year1DollarValue":81.00,"Year1Label":"Prior Year","Year2DollarValue":97.00,"Year2Label":"Current Year","WeekNum":"13"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":9,"WeekEnding":"02-Nov","Year1DollarValue":210.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"39"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":9,"WeekEnding":"04-May","Year1DollarValue":1293.00,"Year1Label":"Prior Year","Year2DollarValue":808.00,"Year2Label":"Current Year","WeekNum":"13"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":9,"WeekEnding":"02-Nov","Year1DollarValue":971.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"39"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":10,"WeekEnding":"11-May","Year1DollarValue":164.00,"Year1Label":"Prior Year","Year2DollarValue":158.00,"Year2Label":"Current Year","WeekNum":"14"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":10,"WeekEnding":"09-Nov","Year1DollarValue":212.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"40"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":10,"WeekEnding":"11-May","Year1DollarValue":1224.00,"Year1Label":"Prior Year","Year2DollarValue":925.00,"Year2Label":"Current Year","WeekNum":"14"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":10,"WeekEnding":"09-Nov","Year1DollarValue":969.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"40"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":11,"WeekEnding":"16-Nov","Year1DollarValue":263.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"41"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":11,"WeekEnding":"18-May","Year1DollarValue":194.00,"Year1Label":"Prior Year","Year2DollarValue":152.00,"Year2Label":"Current Year","WeekNum":"15"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":11,"WeekEnding":"16-Nov","Year1DollarValue":974.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"41"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":11,"WeekEnding":"18-May","Year1DollarValue":779.00,"Year1Label":"Prior Year","Year2DollarValue":1106.00,"Year2Label":"Current Year","WeekNum":"15"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":12,"WeekEnding":"25-May","Year1DollarValue":127.00,"Year1Label":"Prior Year","Year2DollarValue":175.00,"Year2Label":"Current Year","WeekNum":"16"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":12,"WeekEnding":"23-Nov","Year1DollarValue":187.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"42"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":12,"WeekEnding":"25-May","Year1DollarValue":1199.00,"Year1Label":"Prior Year","Year2DollarValue":992.00,"Year2Label":"Current Year","WeekNum":"16"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":12,"WeekEnding":"23-Nov","Year1DollarValue":1316.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"42"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":13,"WeekEnding":"30-Nov","Year1DollarValue":133.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"43"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":13,"WeekEnding":"01-Jun","Year1DollarValue":90.00,"Year1Label":"Prior Year","Year2DollarValue":125.00,"Year2Label":"Current Year","WeekNum":"17"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":13,"WeekEnding":"30-Nov","Year1DollarValue":1057.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"43"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":13,"WeekEnding":"01-Jun","Year1DollarValue":1191.00,"Year1Label":"Prior Year","Year2DollarValue":816.00,"Year2Label":"Current Year","WeekNum":"17"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":14,"WeekEnding":"08-Jun","Year1DollarValue":141.00,"Year1Label":"Prior Year","Year2DollarValue":134.00,"Year2Label":"Current Year","WeekNum":"18"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":14,"WeekEnding":"07-Dec","Year1DollarValue":219.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"44"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":14,"WeekEnding":"08-Jun","Year1DollarValue":1064.00,"Year1Label":"Prior Year","Year2DollarValue":909.00,"Year2Label":"Current Year","WeekNum":"18"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":14,"WeekEnding":"07-Dec","Year1DollarValue":1066.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"44"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":15,"WeekEnding":"14-Dec","Year1DollarValue":197.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"45"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":15,"WeekEnding":"15-Jun","Year1DollarValue":131.00,"Year1Label":"Prior Year","Year2DollarValue":119.00,"Year2Label":"Current Year","WeekNum":"19"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":15,"WeekEnding":"14-Dec","Year1DollarValue":1326.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"45"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":15,"WeekEnding":"15-Jun","Year1DollarValue":988.00,"Year1Label":"Prior Year","Year2DollarValue":977.00,"Year2Label":"Current Year","WeekNum":"19"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":16,"WeekEnding":"22-Jun","Year1DollarValue":135.00,"Year1Label":"Prior Year","Year2DollarValue":80.00,"Year2Label":"Current Year","WeekNum":"20"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":16,"WeekEnding":"21-Dec","Year1DollarValue":518.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"46"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":16,"WeekEnding":"22-Jun","Year1DollarValue":1356.00,"Year1Label":"Prior Year","Year2DollarValue":717.00,"Year2Label":"Current Year","WeekNum":"20"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":16,"WeekEnding":"21-Dec","Year1DollarValue":1638.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"46"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":17,"WeekEnding":"28-Dec","Year1DollarValue":279.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"47"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":17,"WeekEnding":"29-Jun","Year1DollarValue":188.00,"Year1Label":"Prior Year","Year2DollarValue":279.00,"Year2Label":"Current Year","WeekNum":"21"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":17,"WeekEnding":"28-Dec","Year1DollarValue":1571.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"47"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":17,"WeekEnding":"29-Jun","Year1DollarValue":1257.00,"Year1Label":"Prior Year","Year2DollarValue":1234.00,"Year2Label":"Current Year","WeekNum":"21"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":18,"WeekEnding":"06-Jul","Year1DollarValue":213.00,"Year1Label":"Prior Year","Year2DollarValue":276.00,"Year2Label":"Current Year","WeekNum":"22"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":18,"WeekEnding":"04-Jan","Year1DollarValue":255.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"48"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":18,"WeekEnding":"06-Jul","Year1DollarValue":1198.00,"Year1Label":"Prior Year","Year2DollarValue":936.00,"Year2Label":"Current Year","WeekNum":"22"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":18,"WeekEnding":"04-Jan","Year1DollarValue":1036.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"48"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":19,"WeekEnding":"11-Jan","Year1DollarValue":286.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"49"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":19,"WeekEnding":"13-Jul","Year1DollarValue":88.00,"Year1Label":"Prior Year","Year2DollarValue":251.00,"Year2Label":"Current Year","WeekNum":"23"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":19,"WeekEnding":"11-Jan","Year1DollarValue":1077.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"49"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":19,"WeekEnding":"13-Jul","Year1DollarValue":1327.00,"Year1Label":"Prior Year","Year2DollarValue":951.00,"Year2Label":"Current Year","WeekNum":"23"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":20,"WeekEnding":"18-Jan","Year1DollarValue":233.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"50"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":20,"WeekEnding":"20-Jul","Year1DollarValue":207.00,"Year1Label":"Prior Year","Year2DollarValue":191.00,"Year2Label":"Current Year","WeekNum":"24"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":20,"WeekEnding":"18-Jan","Year1DollarValue":927.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"50"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":20,"WeekEnding":"20-Jul","Year1DollarValue":1371.00,"Year1Label":"Prior Year","Year2DollarValue":971.00,"Year2Label":"Current Year","WeekNum":"24"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":21,"WeekEnding":"27-Jul","Year1DollarValue":212.00,"Year1Label":"Prior Year","Year2DollarValue":212.00,"Year2Label":"Current Year","WeekNum":"25"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":21,"WeekEnding":"25-Jan","Year1DollarValue":164.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"51"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":21,"WeekEnding":"27-Jul","Year1DollarValue":1077.00,"Year1Label":"Prior Year","Year2DollarValue":1242.00,"Year2Label":"Current Year","WeekNum":"25"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":21,"WeekEnding":"25-Jan","Year1DollarValue":1094.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"51"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":22,"WeekEnding":"01-Feb","Year1DollarValue":179.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"52"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":22,"WeekEnding":"03-Aug","Year1DollarValue":168.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"26"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":22,"WeekEnding":"01-Feb","Year1DollarValue":957.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"52"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":22,"WeekEnding":"03-Aug","Year1DollarValue":1673.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"26"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":23,"WeekEnding":"10-Aug","Year1DollarValue":104.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"27"},{"Category":"Lighting Products","TimeFrame":"Sept-Feb","Sequence":23,"WeekEnding":"08-Feb","Year1DollarValue":67.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"1"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":23,"WeekEnding":"10-Aug","Year1DollarValue":1054.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"27"},{"Category":"Premium Batteries","TimeFrame":"Sept-Feb","Sequence":23,"WeekEnding":"08-Feb","Year1DollarValue":1008.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"1"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":24,"WeekEnding":"17-Aug","Year1DollarValue":312.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"28"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":24,"WeekEnding":"17-Aug","Year1DollarValue":1664.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"28"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":25,"WeekEnding":"24-Aug","Year1DollarValue":222.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"29"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":25,"WeekEnding":"24-Aug","Year1DollarValue":958.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"29"},{"Category":"Lighting Products","TimeFrame":"Mar-Aug","Sequence":26,"WeekEnding":"31-Aug","Year1DollarValue":183.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"30"},{"Category":"Premium Batteries","TimeFrame":"Mar-Aug","Sequence":26,"WeekEnding":"31-Aug","Year1DollarValue":1048.00,"Year1Label":"Prior Year","Year2DollarValue":0.00,"Year2Label":"Current Year","WeekNum":"30"}],

            ENR_POS_OverviewLowes1_INFO:[{"Team":"ENR","Source":"Last Updated: 11/25/2013","LastUpdated":"Last Updated: 11/25/2013"}],
ENR_POS_OverviewLowes1_StoreNum:[{"StoreNumber":"Store 1051"}],
ENR_POS_OverviewLowes1:[
{"Category":"Total Energizer","SalesLast4Wks":14219,"StorePct":14.9,"MktPct":4.0,"Sequence":1},
{"Category":"Batteries","SalesLast4Wks":5289.3,"StorePct":-8.4,"MktPct":10.7,"Sequence":2},
{"Category":"Shave","SalesLast4Wks":7426.8,"StorePct":-16,"MktPct":-13.1,"Sequence":3},
{"Category":"Suncare","SalesLast4Wks":1503,"StorePct":10.0,"MktPct":10.0,"Sequence":4}
],



            /*Gap Tracker Data*/
            CHW_GapT_All1: [{
                "Client": "Big Heart Pet Brands",
                "Gap": "Place Shelf Space Holder and take picture",
                "DueDate": "4/16/2015",
                "DaysPast": "17",
                "WorkDaysLeft": "0",
                "CallMode": "GM"
            },
                {
                    "Client": "Reynolds Consumer Products",
                    "Gap": "Hefty Kitchen Indoor trash bag IRC's (Goal is 140/store) Total Placed:  35",
                    "DueDate": "5/1/2015",
                    "DaysPast": "6",
                    "WorkDaysLeft": "14",
                    "CallMode": "GM"
                },
                {
                    "Client": "Big Heart Pet Brands",
                    "Gap": "Place Shelf Space Holder and take picture",
                    "DueDate": "4/16/2015",
                    "DaysPast": "17",
                    "WorkDaysLeft": "0",
                    "CallMode": "GM"
                },
                {
                    "Client": "Marzetti",
                    "Gap": "Flatout Display Placement",
                    "DueDate": "6/2/2015",
                    "DaysPast": "0",
                    "WorkDaysLeft": "16",
                    "CallMode": "Every"
                },
                {
                    "Client": "Paramount Farms",
                    "Gap": "8oz or 10oz Roasted No Salt Pistachios",
                    "DueDate": "4/2/2015",
                    "DaysPast": "27",
                    "WorkDaysLeft": "0",
                    "CallMode": "Grocery"
                },
                {
                    "Client": "47 Hour",
                    "Gap": "Checkout Display Feature Set Up",
                    "DueDate": "5/1/2015",
                    "DaysPast": "6",
                    "WorkDaysLeft": "0",
                    "CallMode": "Every"
                }],

            CHW_GapT_All1_Client: [{"Value": "47 Hour", "Display": "47 Hour"},
                {"Value": "Big Heart Pet Brands", "Display": "Big Heart Pet Brands"},
                {"Value": "Keurig", "Display": "Keurig"},
                {"Value": "Marzetti", "Display": "Marzetti"},
                {"Value": "Paramount Farms", "Display": "Paramount Farms"},
                {"Value": "Reynolds Consumer Products", "Display": "Reynolds Consumer Products"}],

            CHW_GapT_All1_WorkDaysLeft: [{"Value": "0", "Display": "0"},
                {"Value": "14", "Display": "14"},
                {"Value": "16", "Display": "16"}],

            CHW_GapT_All1_CallMode: [{"Value": "Every", "Display": "Every"},
                {"Value": "GM", "Display": "GM"},
                {"Value": "Grocery", "Display": "Grocery"}],

            /*END Gap Tracker Data*/



            /*Lost Sales Data*/
            SCJ_OSA_SelectedFilter: [{"Value": "", "Display": "All"}, {
                "Value": "Selected",
                "Display": "Selected Only"
            }],
            SCJ_OSA_LostSalesCalculator1_INFO: [{
                "Team": "SCJ",
                "Source": "Source: Retail Link, 2/20/2014",
                "LastUpdated": "2/20/2014"
            }],


            SCJ_OSA_LostSalesCalculator1: [{
                "WINDescription": "550460643 - GLD AEROSOL REDHONEY",
                "Category": "Air Care",
                "OHLW": 157,
                "MissedSls3Wks": 94.08,
                "FutureMissedSls": 282.24
            }, {
                "WINDescription": "1336229 - GLD SOLID HAWAIIAN",
                "Category": "Air Care",
                "OHLW": 27,
                "MissedSls3Wks": 82.32,
                "FutureMissedSls": 246.96
            }, {
                "WINDescription": "550415176 - GLD AEROSOL APPLCINN",
                "Category": "Air Care",
                "OHLW": 183,
                "MissedSls3Wks": 76.44,
                "FutureMissedSls": 229.32
            }, {
                "WINDescription": "1311707 - GLD OIL RF2PK SWTPEA",
                "Category": "Air Care",
                "OHLW": 7,
                "MissedSls3Wks": 58.56,
                "FutureMissedSls": 175.68
            }, {
                "WINDescription": "1319611 - GLD LI OIL RF2PK LIN",
                "Category": "Air Care",
                "OHLW": 93,
                "MissedSls3Wks": 58.56,
                "FutureMissedSls": 175.68
            }, {
                "WINDescription": "551605991 - ZPLC TNTED SLDR GAL",
                "Category": "Home Storage",
                "OHLW": 9,
                "MissedSls3Wks": 52.56,
                "FutureMissedSls": 157.68
            }, {
                "WINDescription": "1317913 - PLEDGE MS RAINSHOWER",
                "Category": "Home Cleaning",
                "OHLW": 6,
                "MissedSls3Wks": 48.60,
                "FutureMissedSls": 145.80
            }, {
                "WINDescription": "551945013 - GLD CDL HB/VPF 3.8OZ",
                "Category": "Air Care",
                "OHLW": 6,
                "MissedSls3Wks": 44.70,
                "FutureMissedSls": 134.10
            }, {
                "WINDescription": "550081517 - GLD AUTOSPR RF LAVND",
                "Category": "Air Care",
                "OHLW": 8,
                "MissedSls3Wks": 43.92,
                "FutureMissedSls": 131.76
            }, {
                "WINDescription": "550487668 - GLD S&S RFL CASHMERE",
                "Category": "Air Care",
                "OHLW": 10,
                "MissedSls3Wks": 26.82,
                "FutureMissedSls": 80.46
            }],

            SCJ_OSA_LostSalesCalculator1old: [

                {
                    "WINDescription": "551945013 - GLD CDL HB/VPF 3.8OZ",
                    "Category": "Air Care",
                    "OHLW": 23,
                    "MissedSls3Wks": 23.00,
                    "FutureMissedSls": 69.00
                },
                {
                    "WINDescription": "1110625 - DRANO MAX 42OZ GEL",
                    "Category": "Home Cleaning",
                    "OHLW": 13,
                    "MissedSls3Wks": 13.00,
                    "FutureMissedSls": 39.00
                },
                {
                    "WINDescription": "551945539 - GLD CND MW/WS 3.8OZ",
                    "Category": "Air Care",
                    "OHLW": 9,
                    "MissedSls3Wks": 9.00,
                    "FutureMissedSls": 27.00
                },
                {
                    "WINDescription": "551945013 - GLD CDL HB/VPF 3.8OZ",
                    "Category": "Air Care",
                    "OHLW": 23,
                    "MissedSls3Wks": 23.00,
                    "FutureMissedSls": 69.00
                },
                {
                    "WINDescription": "1110625 - DRANO MAX 42OZ GEL",
                    "Category": "Home Cleaning",
                    "OHLW": 13,
                    "MissedSls3Wks": 13.00,
                    "FutureMissedSls": 39.00
                },
                {
                    "WINDescription": "551945539 - GLD CND MW/WS 3.8OZ",
                    "Category": "Air Care",
                    "OHLW": 9,
                    "MissedSls3Wks": 9.00,
                    "FutureMissedSls": 27.00
                },
                {
                    "WINDescription": "551945013 - GLD CDL HB/VPF 3.8OZ",
                    "Category": "Air Care",
                    "OHLW": 23,
                    "MissedSls3Wks": 23.00,
                    "FutureMissedSls": 69.00
                },
                {
                    "WINDescription": "1110625 - DRANO MAX 42OZ GEL",
                    "Category": "Home Cleaning",
                    "OHLW": 13,
                    "MissedSls3Wks": 13.00,
                    "FutureMissedSls": 39.00
                },
                {
                    "WINDescription": "551945539 - GLD CND MW/WS 3.8OZ",
                    "Category": "Air Care",
                    "OHLW": 9,
                    "MissedSls3Wks": 9.00,
                    "FutureMissedSls": 27.00
                },

                {
                    "WINDescription": "551945013 - GLD CDL HB/VPF 3.8OZ",
                    "Category": "Air Care",
                    "OHLW": 23,
                    "MissedSls3Wks": 23.00,
                    "FutureMissedSls": 69.00
                },
                {
                    "WINDescription": "1110625 - DRANO MAX 42OZ GEL",
                    "Category": "Home Cleaning",
                    "OHLW": 13,
                    "MissedSls3Wks": 13.00,
                    "FutureMissedSls": 39.00
                },
                {
                    "WINDescription": "551945539 - GLD CND MW/WS 3.8OZ",
                    "Category": "Air Care",
                    "OHLW": 9,
                    "MissedSls3Wks": 9.00,
                    "FutureMissedSls": 27.00
                },
                {
                    "WINDescription": "551598487 - OVAL NEON PINK 45",
                    "Category": "Shoe Care",
                    "OHLW": 6,
                    "MissedSls3Wks": 6.00,
                    "FutureMissedSls": 18.00
                }
            ]

            /*END Lost Sales Data*/


        }
        data.STORENUMBER = "2081";//set the key to storenumber 2081 which matches the PWS_Opp_BusinessReview2 match value.
        data.PWS_Movement_Radio = [{
            "Value": "(WeekNum>40 && WeekNum<53)",
            "Display": "Last 12 weeks"
        }, {"Value": "(WeekNum>0 && WeekNum<13)", "Display": "LY Perf 12 weeks out"}];
        append_pitch_static_data(data); //call to/from the ASMCustomData.js
        display_presentation_data(data);
        setup_presentation();
    }

    if (!testing)
        include('corescripts/SFPresentations.js');


    function display_presentation_data(data_hash_table) {
        data_hash_table['UBF_MMCalc_Selected'] = [{Value: '', Display: 'All'}, {
            Value: 'Selected',
            Display: 'Selected Only'
        }];
        crm_allow_save_data = data_hash_table['crm_allow_save_data'];
        crm_current_call_key = data_hash_table['crm_current_call_key'];
        crm_current_presentation_key = data_hash_table['crm_current_presentation_key'];
        check_storage_clean_up(crm_current_call_key, crm_current_presentation_key);
        pitch_data = data_hash_table;
        core_setup_presentation();
        populate_ui_data($('body'));
    }

    function get_filter_key($filter_element) {
        if ($filter_element.data('filter_key') == null)
            $filter_element.data('filter_key', new_guid());
        return $filter_element.data('filter_key');
    }

    function display_presentation_data_error(message) {
        alert(message);
    }

    function setup_presentation(presentationID) {
        current_presentation_id = presentationID;
        var $end = $('.EndPresentation');
        $end.click(function () {
            var $video = $('video');
            if ($video.length > 0) {
                $video.get(0).pause();
            }


            core.record_breadcrumb('Presentation Ended', function () {
                SFPresentationAPI.end_presentation(presentationID, function () {
                    console.log('presentation ended');
                }, function (message) {
                    alert('Presentation end error: ' + message);
                });
            });
        });
        var $video = $('video');
        $('video').bind('play', function () {
            core.record_breadcrumb('Play Video: ' + $(this).get(0).currentSrc);
        });
        $('video').bind('pause', function () {
            core.record_breadcrumb('Pause Video: ' + $(this).get(0).currentSrc);
        });
        $('video').bind('ended', function () {
            core.record_breadcrumb('End Video: ' + $(this).get(0).currentSrc);
        });
    }

    function wait_for_presentations() {
        if ('SFPresentationAPI' in window) {
            console.log('setting up sfpresentation api');
            SFPresentationAPI.ready(function (presentationID) {
                SFTouch.log('presentation API ready');
                var data_success = function (data) {
                    append_pitch_static_data(data); //call to/from the ASMCustomData.js
                    current_presentation_id = presentationID;
                    display_presentation_data(data);
                    setup_presentation(presentationID);
                }
                SFPresentationAPI.get_presentation_data(presentationID, data_success, display_presentation_data_error);
            });
        }
        else {
            console.log('waiting for SFPresentationAPI');
            setTimeout(wait_for_presentations, 1000);
        }
    }

    if (testing)
        $(testingStartup);
    else
        wait_for_presentations();
})();